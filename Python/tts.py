#Script to read and "read" (text to speach) the content of a file written by kOS from inside KSP -- by maehschaf
import pyttsx3
import json, time, shutil, os, threading
from os import path

engine = None
messages = list()
currentmessage = None
alreadyread = list()
running = True

dirname = os.path.dirname(__file__)

transferFilePython = os.path.join(dirname, '../Data/tts_python.json')
transferFileKOS = os.path.join(dirname, '../Data/tts_kOS.json')

def readMessageFile(f):
   messages = list()

   try:
      kOSjson = json.load(f)
   except json.decoder.JSONDecodeError:
      return messages

   items = kOSjson["items"]
   for i in items:
      message = dict()
      for e in range(len(i["entries"])):
         if e % 2 == 0:
            message[i["entries"][e]] = None
         else:
            message[i["entries"][e - 1]] = i["entries"][e]
      messages.append(message)
   messages = remove_read_messages(messages)
   return messages

def remove_read_messages(ms):
   global alreadyread
   newms = list()
   for m in ms:
      if not m["id"] + m["timecreated"] in alreadyread:
         newms.append(m)
   return newms #new list without unread messages

def message_sort(message):
   return message["priority"] * 1000 + (message["id"] % 1000)

def tts_loop():
   global currentmessage
   global engine

   engine = pyttsx3.init()
   engine.setProperty("voice", engine.getProperty("voices")[2].id) # Set voice

   while running:
      #print("TTS")
      if currentmessage:
         engine.setProperty("voice", engine.getProperty("voices")[2].id) # Set voice
         engine.setProperty("volume", currentmessage["volume"])
         engine.setProperty("rate", currentmessage["rate"])
         engine.say(currentmessage["text"])
         print("Reading message:", currentmessage["text"])
         engine.runAndWait()
         print("Done")
         currentmessage = None

def read_file_loop():
   global messages
   global currentmessage

   while running:
      #print("Fread")
      #Update the read messages
      messages = remove_read_messages(messages)
      #Remove messages that timed out
      for m in messages:
         #Message timed out
         if (m["timecreated"] + m["timeout"]) < time.time() and m["timeout"] != -1:
            alreadyread.append(m["id"] + m["timecreated"])
            continue

      #Add new messages in case they exist
      if path.exists(transferFilePython):
         try:
            with open(transferFilePython, "r") as f:
               messages = readMessageFile(f)
               messages.sort(key=message_sort)
               messages = remove_read_messages(messages) #Remove timed out messages
               f.close()
               os.rename(transferFilePython, transferFileKOS) #Rename the file so kOS knows it can write again
         except PermissionError as e:
            print("TTS file is currently being accessed by kOS:", e)

      #There are messages to read
      if len(messages) > 0:
         #print("Unread messages:", len(messages))
         m0 = messages[0]
         if m0["priority"] == 0 and not (currentmessage and currentmessage["priority"] == 0): #Error, stop current message if its not an error too
            print("Found Message with priority 0")
            engine.stop()
         elif currentmessage: #Otherwise return if we are reading a message
            continue
         #Seems like there is no message or this is an error so lets read it!
         currentmessage = m0
         alreadyread.append(m0["id"] + m0["timecreated"])
         
   
ttsThread = threading.Thread(target=tts_loop, daemon=True)
ttsThread.start()

while engine == None:
   time.sleep(0.1)

#List voices
print("Available voices:")
for v in engine.getProperty("voices"):
   print(v.name)

#The current voice
print("Selected voice:", engine.getProperty("voices")[2].name)
engine.setProperty("voice", engine.getProperty("voices")[2].id) # Set voice

print("\nPress Crtl + C to stop")
try:
   read_file_loop()
except (KeyboardInterrupt, SystemExit):
   running = False

engine.stop()
print("Waiting for TTS to finish...")
ttsThread.join()

print("Stopped")


