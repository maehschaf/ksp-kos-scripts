#By /u/SciVibes on /r/kOS
#TODO: Change it to my liking! (And find out how it actually works lol)
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

fig = plt.figure()
ax1 = fig.add_subplot(1,1,1)

def animate(i):
    (laterr, lngerr) = np.loadtxt('out.csv', delimiter=',', unpack=True)
    ax1.clear()
    ax1.plot(laterr)
    ax1.plot(lngerr)

ani = animation.FuncAnimation(fig, animate, interval=1)
plt.show()