//A collection of useful Guis and Gui related functions -- by Maehschaf

local loadTime to time:seconds.

global settingGuiElements to Lexicon().

//Useful Gui Elements

global function addLabeledTextField {
    //Adds a textfield with a label with the given text infront
    parameter parent, label, defaultText is "", onconfirm is {Parameter text.}, onchange is {parameter text.}, width is 200. //TODO.. got to change that with the width somehow for Lexicon generation

    local root is Lexicon().
    set root:parent to parent.

    set root:hlayout to root:parent:addHlayout().
    set root:label to root:hlayout:addLabel(label).
    set root:textfield to root:hlayout:addTextField(defaultText).
    set root:textfield:tooltip to "  " + defaultText.
    set root:textfield:style:width to width.
    set root:label:style:textcolor to white.
    set root:textfield:onchange to onchange@.
    set root:textfield:onconfirm to onconfirm@.

    return root.
}

global function addHeader {
    //Adds a nice looking header Gui Element
    Parameter parent, text, color is yellow, fontsize is 18.
    local lheader to parent:addLabel("<b>" + text + "</b>").
    set lheader:style:textcolor to color.
    set lheader:style:fontsize to fontsize.
    set lheader:style:align to "center".
    return lheader.
}

//Popups

global function PopupGui {
    Parameter parent, width.
    local root to Lexicon().
    set root:gui to GUI(width).
    set root:parent to parent.

    set root:parent:enabled to False.

    set root:close to {
        set root:parent:enabled to True.
        root:gui:dispose().
    }.

    root:gui:show().
    return root.
}

global function InfoPopupGui {
    Parameter parent, title, text is "", doneText is "Okay", width is 400.

    local root to PopupGui(parent, width).
    set root:lTitle to addHeader(root:gui, title).
    root:gui:addSpacing(10).

    set root:lInfo to root:gui:addLabel(text).
    set root:lInfo:style:textcolor to white.
    set root:hlButtonBar to root:gui:addHLayout().
    
    set root:bDone to  root:hlButtonBar:addButton(doneText).
    set root:bDone:onclick to root:close@.

    return root.
}

global function BooleanPopupGui {
    Parameter parent, title, text is "", yesCallback is {}, noCallback is {}, yesText is "Yes", noText is "No", width is 400.

    local root to InfoPopupGui(parent, title, text, yesText, width).
    set root:bDone:onclick to {
        root:close().
        yesCallback().
    }.
    
    set root:bNo to  root:hlButtonBar:addButton(noText).
        set root:bNo:onclick to {
        root:close().
        noCallback().
    }.
}

global function TextPromptPopupGui {
    Parameter parent, title is "Enter a text:", defaultText is "Text", doneCallback is {Parameter text.}, cancelCallback is {}, doneText is "Done", cancelText is "Cancel", width is 400.

    local root to PopupGui(parent, width). //Maybe use InfoPopupGui?

    set root:lTitle to addHeader(root:gui, title).
    root:gui:addSpacing(10).
    set root:tftext to root:gui:addTextfield(defaultText).
    set root:hlButtonBar to root:gui:addHLayout().
    
    set root:hlButtonBar:addButton(cancelText):onclick to {
        root:close().
        cancelCallback().
    }.
    root:hlButtonBar:addSpacing().
    set root:hlButtonBar:addButton(doneText):onclick to {
        root:close().
        doneCallback(root:tftext:text).
    }.

    return root.
}

//FileBrowser

global Function FileBrowserGui {
    //Opens a gui that lets the user select a file or directory
    //Possible modes: directory, file or both
    //If invertShowExtensions is False only files that end with an extension that is included in showExtensions are shown
    //If invertShowExtensions is True all files exept those with an extension that is included in showExtensions are shown
    Parameter startPath, mode is "both", cancelCallback is {}, openCallBack is {parameter path.}, invertShowExtensions is True, showExtensions is list().

    local root to Lexicon().
    set root:selectedPath to startPath.
    set root:done to False.
    set root:gui to Gui(400, 400).

    set root:lheader to root:gui:addLabel("<b>Select a " + (choose "file or directory" if mode = "both" else mode) + ":</b>").
    set root:lheader:style:textcolor to white.
    set root:lheader:style:fontsize to 18.
    set root:lheader:style:align to "center".

    set root:sbFiles to root:gui:addScrollbox().
    set root:updateFiles to {
        //for f in vesselClasses:values {
        //    local button to root:sbFiles:addButton("<b>" + c:name + "</b>").
        //    set button:onclick to {
        //        set root:selected to c:name.
        //        root:updateInfo().
        //    }.
        //}
    }.

    root:updateFiles().

    set root:hlButtons to root:gui:addhlayout().
    set root:bCancel to root:hlButtons:addButton("Cancel").
    set root:bCancel:onclick to {
        set root:done to True.
        root:gui:dispose().
        cancelCallBack:call().
    }.

    set root:bSelect to root:hlButtons:addButton("Select").
    set root:bSelect:enabled to False.
    set root:bSelect:onclick to {
        if getVesselClassByName(root:selected) <> "None" {
            set root:done to True.
            root:gui:dispose().
            openCallBack:call(root:selected).
        }
    }.

    root:gui:show().

    return root.
}

//Helper functions

global function confirmTextfieldsInElement {
    //Recursive function to run onconfirm on the given widget and all child widgets that have it
    Parameter widget.
    if widget:hasSuffix("onconfirm") { //Is a textfield (I hope! We have a problem if other widgets use "onconfirm" too)
        if widget:onconfirm:istype("UserDelegate") {
            widget:onconfirm(widget:text).
        }
    }
    if widget:hasSuffix("widgets") {
        for w in widget:widgets {
            confirmTextfieldsInElement(w).
        }
    }
}

//Type specific Gui Elements

global function addSettingGuiElementForVariable {
    parameter parent, name, var.

    local tname to choose var:getType() if var:hassuffix("gettype") else var:typename.

    if tname = "UserDelegate" or name = "serializedtype" { //Hacky way to handle exceptions
        return tname.
    } else if settingGuiElements:hasKey(tname) { //The actually important part
        return settingGuiElements[tname]:call(parent, name, var).
    } else {
        return settingGuiElements["String"]:call(parent, name, var:tostring()).
    }
}

global function addSettingGuiElementForLexicon { //Copy signature from settingGuiElements["Lexicon"]
    parameter parent, name, var, allowEditing to True, excludeKeys is list(), containerType is "scrollbox", displayNameFunction is defaultLexiconDispayNameFunction@.
    return settingGuiElements["Lexicon"]:call(parent, name, var, allowEditing, excludeKeys, containerType, displayNameFunction).
}

global function addPopupMenuSettingGuiElement {
    Parameter parent, name, var is "None", options is list(), displaysuffix is "tostring".

    local root to Lexicon().
    set root:parent to parent.
    set root:hlayout to root:parent:addHlayout().
    set root:label to root:hlayout:addLabel(name).
    set root:popup to root:hlayout:addPopupMenu().
    set root:popup:optionsuffix to displaysuffix.
    set root:popup:options to options.

    set root:value to var.

    print(var).
    set root:popup:value to var.

    set root:getValue to {
        return root:value.
    }.
    set root:setValue to {
        Parameter val.
        set root:popup:value to val.
        set root:value to val.
    }.
    set root:popup:onchange to {
        parameter val.
        set root:value to val.
    }.
    return root.
}

//Default dataType settings Gui Elements
set settingGuiElements["String"] to {
    Parameter parent, name, var is "".

    local root to addLabeledTextField(parent, name, var, {
        parameter text.
        set root:value to text.
    }).
    set root:value to var.
    set root:getValue to {
        return root:value.
    }.
    set root:setValue to {
        Parameter val.
        set root:value to val.
        set root:textfield:text to val.
    }.


    return root.
}.

set settingGuiElements["Scalar"] to {
    Parameter parent, name, var is 0.

    local root to addLabeledTextField(parent, name, "" + var, {
        parameter text.
        if root:validate(text) {
            set root:value to text.
        }
    }, {
        Parameter text.
        root:validate(text).
    }, 100).
    set root:textfield:style:align to "right".
    set root:value to var.
    set root:validate to {
        parameter text is root:textfield:text.
        if (not text:istype("Scalar")) and text:tonumber(-99999) = -99999 {
            //Invalid
            set root:textfield:style:textcolor to red.
            return false.
        } else {
            //Valid
            set root:textfield:style:textcolor to root:parent:gui:skin:textfield:textcolor.
            return true.
        }
    }.
    set root:getValue to {
        return root:value.
    }.
    set root:setValue to {
        Parameter val.
        set root:value to val.
        set root:textfield:text to val.
        root:validate(val).
    }.
    root:validate(var).

    return root.
}.

set settingGuiElements["Boolean"] to {
    Parameter parent, name, var is False.

    local root to Lexicon().
    set root:parent to parent.
    set root:checkbox to root:parent:addCheckbox(name, var).
    set root:value to var.
    set root:checkbox:ontoggle to {
        parameter val.
        set root:value to val.
    }.
    set root:getValue to {
        return root:value.
    }.
    set root:setValue to {
        Parameter val.
        set root:value to val.
        set root:checkbox:pressed to val.
    }.

    return root.
}.

global function defaultLexiconDispayNameFunction {
    parameter key, value.
    set key to key[0]:toupper() + key:substring(1, key:length() - 1).
    if not value:istype("Boolean") {
        set key to key + ":".
    }
    return key.
}

set settingGuiElements["Lexicon"] to { // If you change the signature here, change it in addSettingGuiElementForLexicon() too... //possible containerTypes: scrollbox, layout, box
    Parameter parent, name, var is Lexicon(), allowEditing to True, excludeKeys is list(), containerType is "scrollbox", dispayNameFunction is defaultLexiconDispayNameFunction@.

    local root to Lexicon().
    set root:parent to parent.
    set root:value to var.
    set root:settingsGuis to Lexicon().
    set root:label to root:parent:addLabel(name).
    set root:label:style:textcolor to white.

    excludeKeys:add("serializedtype").

    set root:container to root:parent:addVLayout().
    if containerType = "layout" {
        set root:scrollbox to root:container:addVLayout().
    } else if containerType = "box" {
        set root:scrollbox to root:container:addVBox().
    } else {
        set root:scrollbox to root:container:addScrollbox().
    }
    set root:settingGuis to Lexicon().
    set root:buttons to root:container:addHLayout().


    set root:updateValue to {
        for k in root:settingGuis:keys() {
            set (root:value)[k] to (root:settingGuis)[k]:getValue().
        }
    }.

    set root:addSetting to {
        parameter key, val.
        root:updateValue().
        root:value:add(key, val).
        root:updateChildSettings().
    }.

    set root:removeSetting to {
        Parameter key.
        root:updateValue().
        root:value:remove(key).
        root:settingGuis:remove(key).
        root:updateChildSettings().
    }.

    set root:getSetting to {
        Parameter key.
        return root:settingGuis[key].
    }.

    set root:updateChildSettings to {
        root:scrollbox:clear().
        root:settingGuis:clear().
        if root:values:length() <= 0 {
            root:scrollbox:addLabel("No Settings found.").
        } else {

            local function renameSetting {
                parameter kk.

                TextPromptPopupGui(root:parent:gui, "Rename this setting:", kk, {
                    Parameter text.
                    if root:settingGuis:hasKey(text) {
                            InfoPopupGui(root:parent:gui, "Setting already exists!", "A Setting with this name already exists, please choose a different name.").
                    } else {
                        local v to (root:settingGuis)[kk]:getValue().
                        root:removeSetting(kk).
                        root:addSetting(text, v).
                    }
                }, {}, "Rename").
            }

            for k in root:value:keys() {
                if not excludeKeys:contains(k) {
                    local guiElement to addSettingGuiElementForVariable(root:scrollbox, dispayNameFunction(k, root:value[k]), root:value[k]).
                    if guiElement:istype("Lexicon") { //Does that work?
                        root:settingGuis:add(k, guiElement).

                        if allowEditing { //Add Remove and rename buttons
                            local container to root:scrollbox:addHLayout().
                            set container:addButton("Rename"):onclick to renameSetting@:bind(k).
                            container:addSpacing().
                            set container:addButton("Remove"):onclick to root:removeSetting@:bind(k).
                        }
                    }
                }
            }
        }
    }.

    set root:getValue to {
        //Not returning a copy of the original Lexicon. If you want the original Lexicon unchanged pass a copy of it to this Gui Element
        root:updateValue().
        return root:value.
    }.
    set root:setValue to {
        //Replacing the original Lexicon!
        Parameter val. //Clear things leftover from the original Lexicon
        root:updateValue().
        set root:value to val.
        root:updateChildSettings().
    }.

    //Call that stuff! :D
    root:updateChildSettings().

    //Add Add new Setting Button
    if allowEditing {
        set root:bNewSetting to root:buttons:addButton("Add new Setting").
        set root:bNewSetting:onclick to {
            local newSettingGui to PopupGui(root:parent:gui, 400).
            addHeader(newSettingGui:gui, "Add a new Setting:").
            set newSettingGui:tfname to addLabeledTextField(newSettingGui:gui, "Setting name:").
            set newSettingGui:tType to settingGuiElements:type(newSettingGui:gui, "Setting type:").
            set newSettingGui:hlButtonBar to newSettingGui:gui:addHLayout().
    
            set newSettingGui:hlButtonBar:addButton("Cancel"):onclick to {
                newSettingGui:close().
            }.

            set newSettingGui:hlButtonBar:addButton("Done"):onclick to {
                local key to newSettingGui:tfname:textfield:text.
                if key:length() > 0 and newSettingGui:tType:getValue() and not root:value:hasKey(key) {
                    newSettingGui:close().
                    local startValue to settingGuiElements[newSettingGui:tType:getValue()](root:scrollbox, "temp"):getValue(). //Create a new GUI Element just to get its default value...
                    printLog(startValue).
                    root:addSetting(key, startValue).
                } else {
                    //TODO Info popup that things are missing...
                }
            }.
        }.
    }

    return root.
}.

set settingGuiElements["Path"] to {
    Parameter parent, name, var is path(), browserMode is "both".

    local root to addLabeledTextField(parent, name, var:tostring(), {
        parameter text.
        if root:validate(text) {
            set root:value to text.
        }
    }, {
        Parameter text.
        root:validate(text).
    }, 171). //Reorder params

    set root:parent to parent.
    set root:button to root:hlayout:addButton("..."). //TODO on change, open file browser
    set root:button:style:width to 25.
    set root:value to var.
    set root:getValue to {
        return root:value.
    }.
    set root:setValue to {
        Parameter val.
        set root:value to val.
        if root:haskey("selectgui") {
            set root:selectgui:selected to val.
        }
    }.
    set root:button:onclick to {
        set root:parent:gui:enabled to False.
        set browserMode to choose "file" if var:hasextension else "directory".
        set root:selectgui to FileBrowserGui(var, browserMode, {
            set root:parent:gui:enabled to True.
        }, {
            Parameter selected.

            set root:value to selected.
            set root:parent:gui:enabled to True.
            root:remove("selectgui").
        }).
    }.
    set root:validate to {
        parameter text is root:textfield:text.
        if not exists(text) {
            //Invalid
            set root:textfield:style:textcolor to red.
            return false.
        } else {
            //Valid
            set root:textfield:style:textcolor to root:parent:gui:skin:textfield:textcolor.
            return true.
        }
    }.

    root:validate().

    return root.
}.

set settingGuiElements["Type"] to {
    Parameter parent, name, var is list().
    return addPopupMenuSettingGuiElement(parent, name, var, settingGuiElements:keys()).
}.

set settingGuiElements["Body"] to {
    Parameter parent, name, var is Kerbin. //Cause afterall it's our Home <3
    list bodies in bList.
    return addPopupMenuSettingGuiElement(parent, name, var, bList, "name").
}.

set settingGuiElements["Vessel"] to {
    Parameter parent, name, var is choose target if HasTarget else ship.
    list targets in vList.
    //print(vList).
    return addPopupMenuSettingGuiElement(parent, name, var, vList, "name").
}.

//Custom "type" SettingGuiElements...
//set settingGuiElements["VesselInformation"] to. //TODO

if exists("0:steering_lib.ks") { //Temporary (I HOPE)
    RunOncePath("0:steering_lib.ks").

    set settingGuiElements["SteeringFactors"] to {
        parameter parent, name, var is SteeringFactors().

        local root to settingGuiElements["Lexicon"](parent, name, var, False, list(), "box").

        //TODO display list as simple scalar inputs - Maybe not even use Lexicon?
        //TODO add "Init Steering" Button to use these factors as steering input

        return root.
    }.

    set settingGuiElements["SteeringControlFactors"] to {
        parameter parent, name, var is SteeringControlFactors().
        return settingGuiElements["SteeringFactors"](parent, name, var).
    }.

    set settingGuiElements["SteeringRateFactors"] to {
        parameter parent, name, var is SteeringRateFactors().
        return settingGuiElements["SteeringFactors"](parent, name, var).
    }.
}

set settingGuiElements["VesselClass"] to {
    Parameter parent, name, var is "None".

    RunOncePath("0:/vessel_class.ks").
    if var:istype("String") {
        set var to getVesselClassByName(var).
    }

    local root to Lexicon().
    set root:parent to parent.
    set root:hlayout to parent:addHlayout().
    set root:label to root:hlayout:addLabel(name).
    set root:label:style:textcolor to white.
    set root:button to root:hlayout:addButton(choose "Select" if var = "None" else var:name).
    set root:value to var.
    set root:getValue to {
        return root:value.
    }.
    set root:setValue to {
        Parameter val.
        set root:value to val.
        set root:button:text to val:name.
        if root:haskey("selectgui") {
            set root:selectgui:selected to val.
        }
    }.
    set root:button:onclick to {
        set root:parent:gui:enabled to False.
        set root:selectgui to SelectVesselClassGui(root:value, {
            Parameter selected.

            set root:value to selected.
            set root:button:text to selected:name.
            set root:parent:gui:enabled to True.
            root:remove("selectgui").
        }).
    }.
    return root.
}.

printLog("Loaded " + ScriptPath():name + " in " + round(time:seconds - loadTime, 2) + "s").