//All vessel info or mission guis -- by maehschaf

RunOncePath("0:/vessel_info.ks").
RunOncePath("0:/Gui/utils_gui.ks").

local loadTime to time:seconds.

global function VesselInfoSettingsGui {
    parameter vInfo, cancelCallback to {}, doneCallback to { Parameter vInfo. }, allowCancel to True.
    local root to Lexicon().
    set root:vesselInfo to vInfo.
    set root:done to False.
    set root:gui to Gui(400, 200).

    set root:lheader to root:gui:addLabel("<b>Edit Vessel Information</b>").
    set root:lheader:style:textcolor to yellow.
    set root:lheader:style:fontsize to 18.
    set root:lheader:style:align to "center".

    set root:settingsbox to root:gui:addScrollbox().

    root:settingsbox:addSpacing(10).

    //Vessel Information Settings
    set root:sName to addSettingGuiElementForVariable(root:settingsbox, "Name:", root:vesselInfo:name).
    set root:sClass to addSettingGuiElementForVariable(root:settingsbox, "Vessel Class:", root:vesselInfo:class).
    

    //Buttons
    set root:hlButtons to root:gui:addhlayout().
    if allowCancel {
        set root:bCancel to root:hlButtons:addButton("Cancel").
        set root:bCancel:onclick to {
            root:gui:dispose().
            set root:done to True.
            cancelCallback:call().
        }.
    }

    set root:bSave to root:hlButtons:addButton("Save").
    set root:bSave:style:textcolor to green.
    set root:bSave:onclick to {
        confirmTextfieldsInElement(root:settingsBox).

        //Write to vesselInfo and save

        set root:vesselInfo:name to root:sName:getValue().
        set root:vesselInfo:class to root:sClass:getValue():name.

        //set root:vesselInfo:bootfile to path(root:sBootFile:getValue()). //TODO add option to just use the classes bootfile!



        root:vesselInfo:save().

        root:gui:dispose().
        set root:done to True.
        doneCallback:call(root:vesselInfo).
    }.

    root:gui:show().

    return root.
}

printLog("Loaded " + ScriptPath():name + " in " + round(time:seconds - loadTime, 2) + "s").