//All vessel class guis -- by maehschaf

RunOncePath("0:/Gui/utils_gui.ks").

local loadTime to time:seconds.

global function SelectVesselClassGui {
    //Gui to select a vessel class from a list of the existing ones or create a new one
    Parameter preselected is "None", selectedCallback is {parameter selected.}.

    local root to Lexicon().
    set root:selected to preselected.
    if root:selected:istype("String") {
        set root:selected to getVesselClassByName(root:selected).
    }
    set root:done to False.
    set root:gui to Gui(400, 400).

    set root:lheader to addHeader(root:gui, "Select a VesselClass").

    set root:sbclasses to root:gui:addScrollbox().
    set root:updateSBClasses to {
        root:sbclasses:clear().
        for c in vesselClasses:values {
            local button to root:sbclasses:addButton("<b>" + c:name + "</b>").
            set button:toggle to true.
            set button:exclusive to true.
            set button:tooltip to c:name.
            if c = root:selected {
                set button:pressed to true.
            }
            set button:ontoggle to {
                parameter selected.
                if selected {
                    set root:selected to getVesselClassByName(button:tooltip).
                    set root:bSelect:enabled to True.
                    set root:bEdit:enabled to True.
                    root:updateInfo().
                }
            }.
        }
    }.

    root:updateSBClasses().

    set root:vlinfo to root:gui:addvlayout().
    set root:lclassName to root:vlinfo:addLabel("").
    set root:lclassName:style:textcolor to yellow.
    set root:lclassName:style:fontsize to 15.
    set root:lclassDesc to root:vlinfo:addLabel("").
    set root:lclassDesc:style:textcolor to white.
    set root:updateInfo to {
        if root:selected = "None" {
            set root:lclassName:text to "".
            set root:lclassDesc:text to "".
        } else {
            set root:lclassName:text to "<b>" + root:selected:name + "</b>".
            set root:lclassDesc:text to root:selected:description.
        }
    }.

    root:updateInfo().

    set root:hlButtons to root:gui:addhlayout().
    set root:bNew to root:hlButtons:addButton("New VesselClass").
    set root:bNew:onclick to {
        root:gui:hide().
        VesselClassSettingsGui("None", {
            root:gui:show().
        }, {parameter class.
            set root:selected to class.
            root:updateSBClasses().
            root:updateInfo().
            root:gui:show().
        }, True).
    }.

    set root:bEdit to root:hlButtons:addButton("Edit").
    set root:bEdit:enabled to root:selected <> "None".
    set root:bEdit:onclick to {
        if root:selected <> "None" {
            root:gui:hide().
            VesselClassSettingsGui(root:selected, {
                root:gui:show().
            }, {parameter class.
                set root:selected to class.
                root:updateSBClasses().
                root:updateInfo().
                root:gui:show().
            }, False).
        }
    }.

    set root:bSelect to root:hlButtons:addButton("Select").
    set root:bSelect:enabled to root:selected <> "None".
    set root:bSelect:onclick to {
        if root:selected <> "None" {
            set root:done to True.
            root:gui:dispose().
            selectedCallback:call(root:selected).
        }
    }.

    root:gui:show().

    return root.
}

global function VesselClassSettingsGui {
    parameter class is "None", cancelCallback to {}, doneCallback is {parameter class.}, askForOverwrite is False.

    local root to Lexicon().

    if class:istype("String") {
        set class to getVesselClassByName(class).
    }
    if class = "None" {
        set class to VesselClass(ship:name, "", path("0:/boot/")).
    }
    set root:class to class.
    set root:done to False.
    set root:gui to Gui(400, 600).

    set root:lheader to root:gui:addLabel("<b>VesselClass Settings</b>").
    set root:lheader:style:textcolor to yellow.
    set root:lheader:style:fontsize to 18.
    set root:lheader:style:align to "center".

    set root:settingsbox to root:gui:addScrollbox().

    root:settingsBox:addSpacing(10).

    //Actual Class Settings
    set root:sName to addSettingGuiElementForVariable(root:settingsBox, "Name:", root:class:name).
    set root:sDescription to addSettingGuiElementForVariable(root:settingsBox, "Description:", root:class:description).
    
    set root:sAutoSelectClass to addSettingGuiElementForVariable(root:settingsBox, "Automatically select this class if the ship name matches", root:class:get("autoSelectClass", False)).
    set root:sSkipVesselInfoGui to addSettingGuiElementForVariable(root:settingsBox, "Skip editing the Vessel Information", root:class:get("skipVesselInfoGui", False)).
    
    
    root:settingsBox:addSpacing(25).

    set root:settingsBox:addLabel("<b>Settings for vessels that use this class:</b>"):style:textcolor to yellow.

    //User defined settings
    set root:sUserDefined to settingGuiElements:Lexicon(root:settingsBox, "Custom Settings:", root:class:copy(), True, list("serializedtype", "name", "description", "autoSelectClass", "skipVesselInfoGui", "steeringfactors", "filePath")).
    root:sUserDefined:label:dispose().

    root:settingsBox:addSpacing(25).
    
    set root:sFilePath to settingGuiElements:path(root:settingsBox, "Folder to save in:", root:class:filePath, "directory").


    //Buttons
    set root:hlButtons to root:gui:addhlayout().
    set root:bCancel to root:hlButtons:addButton("Cancel").
    set root:bCancel:onclick to {
        root:gui:dispose().
        set root:done to True.
        cancelCallback:call().
    }.

    set root:bSave to root:hlButtons:addButton("Save").
    set root:bSave:style:textcolor to green.
    set root:bSave:onclick to {
        confirmTextfieldsInElement(root:settingsBox).

        local function save {
            confirmTextfieldsInElement(root:settingsBox).
            //Seems like we are allowed to overwrite or the class is new ->
            //Write to class and save

            root:class:clear(). //To get rid of deleted user defined settings

            //User defined settings
            local newLex to root:sUserDefined:getValue().
            
            local cl to root:class.//The syntax set root:class[key] to... Does NOT work for some reason
            for key in newLex:keys() {
                set cl[key] to newLex[key].
            }
            print(newLex).

            set root:class:name to root:sName:getValue().
            set root:class:description to root:sDescription:getValue().

            set root:class:skipVesselInfoGui to root:sSkipVesselInfoGui:getValue().
            set root:class:autoSelectClass to root:sAutoSelectClass:getValue().

            set root:class:filePath to path(root:sFilePath:getValue()).


            root:class:save().

            root:gui:dispose().
            set root:done to True.
            doneCallback:call(root:class).
        }

        //Does the class already exist?
        if getVesselClassByName(root:sName:getValue()) = "None" {
            //Add Vessel Class
            addVesselClass(root:class).
        } else if askForOverwrite { //Ask if existing class should be overwritten...
            BooleanPopupGui(root:gui, "Overwrite?", "A vessel class named " + root:sName:getValue() + " already exists. Should it be overwritten?", {
                set vesselClasses[root:class:name] to root:class.
                save().//Replace existing vesselClass
            }).
            return. //Kinda not elegant I know...
        }
        save().
    }.

    root:gui:show().

    return root.
}


global function getDefaultShipName {
    parameter name is ship:name.

    local shipNumber to 1.
    list targets in vessels.
    for v in vessels {
        if v <> ship and v:name:startswith(name) {
            set shipNumber to v:name:substring(v:name:findLast("#") + 1, v:name:length - (v:name:findLast("#") + 1)):toNumber(shipNumber) + 1.
        }
    }
    return name + " #" + shipNumber.
}

global function getVesselClassFromShipName {
    local match to "None".
    for c in vesselClasses:values() {
        if ship:name:startswith(c:name) and c:autoSelectClass {
            if match = "None" {
                set match to c.
            } else if ship:name = c:name { //If its a perfect match -> Just use it
                return c.
            } else {
                return "None". //It could be multiple classes -> Dont return any!
            }
        }
    }
    return match.
}

printLog("Loaded " + ScriptPath():name + " in " + round(time:seconds - loadTime, 2) + "s").