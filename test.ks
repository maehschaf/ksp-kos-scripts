

runoncepath("0:/Starship/Starship.ks").
runoncepath("0:/steering_lib.ks").

shutdown.

ClearScreen.

local nextState to False.

on ag10 {
    set nextState to True.
    return True.
}


//GUI
local function onControlSliderChange {
    parameter axis, value.
    if axis = "pitch" {
        set ship:control:pitch to value.
    } else if axis = "yaw" {
        set ship:control:yaw to value.
    } else if axis = "roll" {
        set ship:control:roll to value.
    }
}

CLEARGUIS().
Local fineControlGui is GUI(400).

local pitchSlider to fineControlGui:addhslider(0, -1, 1).
set pitchSlider:onchange to onControlSliderChange@:bind("pitch").

local yawSlider to fineControlGui:addhslider(0, -1, 1).
set yawSlider:onchange to onControlSliderChange@:bind("yaw").

local rollSlider to fineControlGui:addhslider(0, -1, 1).
set rollSlider:onchange to onControlSliderChange@:bind("roll").

//fineControlGui:show().

RunPath("0:/Util/PIDTuner.ks", pitchRatePID, "PitchRate").
RunPath("0:/Util/PIDTuner.ks", yawRatePid, "YawRate").
RunPath("0:/Util/PIDTuner.ks", rollRatePID, "RollRate").

RunPath("0:/Util/PIDTuner.ks", pitchPID, "Pitch").
RunPath("0:/Util/PIDTuner.ks", yawPid, "Yaw").
RunPath("0:/Util/PIDTuner.ks", rollPID, "Roll").

//end GUI

updateFlaps("deployed").

initSteeringControlFactors(lexicon(
    "pitch", list(0.1, 0, 0),
    "yaw", list(0.1, 0, 0),
    "roll", list(0.05, 0, 0)
)).
setSteeringTargetHold().
//setSteeringTargetNavball(0, 0, 0).

until nextState {
    local angVel to getAngularVelocity().
    updateSteering().
    println("pitch rate: " + round(angVel:x, 3), 2).
    println("yaw rate: " + round(angVel:y, 3), 3).
    println("roll rate: " + round(angVel:z, 3), 4).
    wait 0.
}
set nextState to False.

until nextState {
    updateSteering().
    updateFlaps("control").
    
    println("pitch control: " + round(ship:control:pitch, 3) * 100, 2).
    println("yaw control: " + round(ship:control:yaw, 3) * 100, 3).
    println("roll control: " + round(ship:control:roll, 3) * 100, 4).

    local angVel to getAngularVelocity().
    println("pitch rate: " + round(angVel:x, 3), 9).
    println("yaw rate: " + round(angVel:y, 3), 10).
    println("roll rate: " + round(angVel:z, 3), 11).
    
    wait 0.
}
set nextState to False.
lock steering to starshipSteering(getPitch(SrfRetrograde)).
Gear on.
