//Spline stuff I experimented with some time ago...
RunOncePath("0:/basic_lib.ks").

local loadTime to time:seconds.

//Bezier curves from https://stackoverflow.com/questions/31167663/how-to-code-an-nth-order-bezier-curve

// from: http://rosettacode.org/wiki/Evaluate_binomial_coefficients#JavaScript
local function binom {
	parameter n, k.
	local coeff to 1.
	from {local i is n - k + 1.} until not (i <= n)  step { set i to i + 1.} do {
		set coeff to coeff * i.
	}
	from {local i is 1.} until not (i <= k)  step { set i to i + 1.} do {
		set coeff to coeff / i.
	}
	return coeff.
}

// based on: https://stackoverflow.com/questions/16227300
function bezier {
	parameter t, pointList.
	local order to pointlist:length - 1.

	local point to V(0,0,0).

	from {local i is 0.} until not (i <= order) step { set i to i + 1. } do {
		set point:x to point:x + (binom(order, i) * (1 - t) ^ (order - i) * t ^ i * (pointlist[i]:x)).
		set point:y to point:y + (binom(order, i) * (1 - t) ^ (order - i) * t ^ i * (pointlist[i]:y)).
		set point:z to point:z + (binom(order, i) * (1 - t) ^ (order - i) * t ^ i * (pointlist[i]:z)).
	}

	return point.
}

//Returns a controlpoint which will make the bezier curve with points start and end go through point P. Simple add the returned Point between start and end.
function getControlPointForP {
	parameter p, start, end, t is 0.5.
	//P(t) = P0*t^2 + P1*2*t*(1-t) + P2*(1-t)^2 nach P1 umgeformt -> P1 = (P0*t^2 + P2*(1-t)^2) / 2*t*(1-t)
	return (p - start * t^2 - end*(1-t)^2) / (2*t*(1-t)).
}


function drawBezier {
	parameter pointList, accuracy is 0.01, col is red, width is 2.

	local prevPoint is bezier(0, pointlist).
	from {local i is accuracy.} until i > 1 + accuracy step { set i to i + accuracy. } do {
		local newPoint is bezier(i, pointlist).
		vecdraw(prevpoint, newPoint - prevpoint, col, "", 1, true, width).
		set prevpoint to newpoint.
	}
}

function drawGeoPosBezier {
	parameter pointList, accuracy is 0.01, col is red, width is 2, b is body.

	local posList is List().
	local altList is List().

	local bezierPoint to bezier(0, pointlist).
	posList:add(b:geopositionof(bezierPoint)).
	altList:add(b:altitudeof(bezierPoint)).
	from {local i is accuracy.} until i > 1 + accuracy step { set i to i + accuracy. } do {
		set bezierpoint to bezier(i, pointlist).
		posList:add(b:geopositionof(bezierPoint)).
		altList:add(b:altitudeof(bezierPoint)).
	}
	from {local i is 1.} until i >= posList:length step { set i to i + 1. } do {
		local c is i.
		local p is i - 1.
		vecdraw({ return posList[p]:altitudeposition(altList[p]). }, { return (posList[c]:altitudeposition(altList[c]) - posList[p]:altitudeposition(altList[p])). }, col, "", 1, true, width).
	}
}
//End Bezier curves

function waypointListToVecList {
	parameter waypointlist.
	local vecList to List().
	for wp in waypointlist {
		vecList:add(getwaypointpos(wp)).
	}
	return veclist.
}

function vecListToWaypointList {
	parameter vecList.
	local waypointlist to List().
	for vec in vecList {
		waypointlist:add(Lexicon("GeoPos", body:geopositionof(vec), "Altitude", body:altitudeof(vec))).
	}
	return waypointlist.
}


//Follow Curve
local curveWaypoints to List().
local t is 0.
local step to 0.0001.
local closestpoint to V(0,0,0).
local targetpoint to V(0,0,0).
local currentPointOnCurve to 0.
local curveheading to 0.

local lastSteeringTime to 0.

function startFollowCurve {
    parameter curvePositions, maxBankAngle is 60, maxVerticalSpeed is 20, maxHeadingError is 20, drawCurve is false, closestPointToTargetPointDistance to 400, updateTimeStep is 1, stepsOnCurve is 1 / (curvePositions:length - 1) * 0.05.
    
    set config:IPU to 2000.

    set curveWaypoints to veclisttowaypointlist(curvePositions).

    set t to 0.
    set step to stepsOnCurve.
    set closestPoint to bezier(t, waypointlisttoveclist(curveWaypoints)).
    set targetPoint to closestpoint.
    set currentPointOnCurve to Lexicon("GeoPos", body:geopositionof(targetPoint), "Altitude", body:altitudeof(targetPoint)).

    set lastSteeringTime to 0.

    if (drawCurve) {
        clearvecdraws().
        drawGeoPosBezier(curvePositions, 0.05, green).
        vecdraw({ return ship:position.}, { return (getWaypointPos(currentPointOnCurve) - ship:position).}, red, "", 1, true, 2).
        vecdraw({ return ship:position.}, { return (closestpoint - ship:position).}, yellow, "", 1, true, 2).
    }

    lock steering to followcurvesteering(updateTimeStep, maxBankAngle, maxVerticalSpeed, maxHeadingError, closestPointToTargetPointDistance).

    when t > 1.0 then {
        print "Follow curve has finished".
        lock steering to "kill".
        if drawCurve {
            clearvecdraws().
        }
        return false.
    }
}

function followCurveDone {
    return t > 1.
}

local function followCurveSteering {
    parameter updateTime is 1, maxBankAngle is 60, maxVerticalSpeed is 20, maxHeadingError is 20, closestPointToTargetPointDistance is 200.

    if time:seconds - laststeeringtime < updateTime {
        //print (time:seconds - laststeeringtime).
        return steertowardsalt(clamp(currentPointOnCurve["GeoPos"]:heading, curveheading + maxHeadingError, curveheading - maxHeadingError), currentpointoncurve["Altitude"], min(alt:radar / 2, maxbankangle), maxVerticalSpeed).
    }
    set laststeeringtime to time:seconds.

    local waypointveclist to waypointlisttoveclist(curveWaypoints).
    local dist is 10000000000000000000000000000000000000000000000.
    local closestpointstep to 0.
    until false {
        set closestpoint to bezier(t + closestpointstep, waypointveclist).
        if closestpoint:mag < dist {
            set closestpointstep to closestpointstep + (step / 2).
        } else {
            break.
        }
        set dist to closestpoint:mag.
        print "Distance closest point on curve: " + round(dist) at (0,24).
    }
    set t to clamp(t + closestpointstep - step, 0, 2).
    	
    local targetpointstep to step.
    set targetpoint to bezier(clamp(t + targetpointstep, 0, 1), waypointveclist).
    until (targetpoint - closestpoint):mag > closestPointToTargetPointDistance {
        set targetpoint to bezier(clamp(t + targetpointstep, 0, 1), waypointveclist).
        set targetpointstep to targetpointstep + step.
        print "Distance closest point to target point: " + round((targetpoint - closestpoint):mag) at(0,25).
    }

    if t + closestpointstep > 1 or t + targetpointstep > 1{
        set t to 2.
    }

    local targetPointDir to (targetPoint - closestpoint):normalized().

    set currentPointOnCurve to Lexicon("GeoPos", body:geopositionof(targetPoint), "Altitude", body:altitudeof(targetPoint)).

    set curveHeading to getHeading(lookdirup(targetpointdir, UP:forevector)).

    print "Steering to heading: " + curveheading at(0, 17).

    PRINT "T: " + t at (0, 19).
    PRINT "Target Altitude: " + round(currentPointOnCurve["Altitude"]) at (0, 20).
    //verticalspeedpid:update(time:seconds, verticalspeed - 1)
    return steertowardsalt(clamp(currentPointOnCurve["GeoPos"]:heading, curveheading + maxHeadingError, curveheading - maxHeadingError), currentpointoncurve["Altitude"], min(alt:radar / 2, maxbankangle), maxVerticalSpeed).
}

printLog("Loaded " + ScriptPath():name + " in " + round(time:seconds - loadTime, 2) + "s").