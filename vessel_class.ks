//A lib for vessel classes -- by maehschaf

local loadTime to time:seconds.

global vesselClasses to Lexicon().

//static
global vesselClassFileExtension to "vesselclass".

findVesselClasses().

//Class

//Constructor with all attributes
Global function VesselClass {
    //Dont forget to add to the list via addVesselClass(class) -- bootfile should be a path
    Parameter name.
    Parameter description.
    Parameter bootfile.
    Parameter filePath is path("0:/VesselClasses/").
    local self to Lexicon().

    set self:name to name.
    set self:description to description.
    set self:bootfile to bootfile.
    set self:filePath to filePath.
    set self:steeringFactors to Lexicon().
    set self:textToSpeech to False.

    set self:autoSelectClass to False. //Whether this class should be automatically asigned to vessels with its classname when they are first launched TODO -- actually implement this
    set self:skipVesselInfoGui to False. //Whether vessels of this class skip the VesselInfo Screen on launch and just use the default values
    //TODO probaply more stuff that has to do with boot files n stuff

    init(self).

    return self.
}

//initialize all functions
local function init {
    Parameter self.

    initTypeFunctionOverrides(self, "VesselClass").

    set self:runBootScript to {
        if IsFile(self:bootfile) {
            runpath(self:bootfile).
        } else {
            printLog("Could not find bootfile of VesselClass " + self:name + ".", MessagePriority:WARNING).
        }
    }.

    set self:initSteering to {
        Parameter steeringPhase is "default".
        if self:steeringfactors:hasKey(steeringPhase) {
            RunOncePath("0:steering_lib.ks").
            initSteering(self:steeringfactors[steeringPhase]).
        } else {
            printLog("No SteeringFactors found for steering phase " + steeringPhase + ", using default", MessagePriority:WARNING).
            if steeringPhase = "default" {
                printLog("Failed to initialize Steering: No default SteeringFactors found", MessagePriority:ERROR).
            } else {
                initSteering(self:steeringfactors["default"]).
            }
        }
    }.

    set self:get to {
        Parameter name, default is "None".
        if self:hasKey(name) {
            return self[name].
        } else {
            return default.
        }
    }.

    set self:set to {
        Parameter name, value.
        set self[name] to value.
    }.

    //Saves self class to the given path and filename
    set self:save to {
        Parameter path is self:filePath, filename is self:name + "." + vesselClassFileExtension.
        saveVesselClass(self, path, filename).
    }.
}

//Load and save
global function saveVesselClass {
    Parameter class, path is class:filePath, name is class:name + "." + vesselClassFileExtension.

    WriteJson(class, path:combine(name)).
}

global function loadVesselClass {
    parameter file.
    local self is ReadJson(file).

    init(self).

    for k in self:steeringfactors:keys {
        set self:steeringfactors[k] to loadSteeringFactors(self:steeringfactors[k]).
    }

    return self.
}

//End Class

//Basic
function addVesselClass {
    parameter class.
    set vesselClasses[class:name] to class.
}

//Gets this vessels VesselClass
function getVesselClass {
    RunOncePath("0:/vessel_info.ks").
    if vesselInfo = "None" {
        return "None".
    }
    return getVesselClassByName(vesselInfo:class).
}

//Gets the VesselClass with the given name
function getVesselClassByName {
    Parameter name.
    if vesselClasses:hasKey(name) {
        return vesselClasses[name].
    } else {
        return "None".
    }
}

//Sets this vessels VesselClass
function setVesselClass {
    parameter class.
    RunOncePath("0:/vessel_info.ks").
    if vesselInfo = "None" {
        return "None".
    }
    if class:isType("Lexicon") or class:isType("VesselClass") {
        set class to class:name.
    }
    set vesselInfo:class to class.
}

//Finds VesselClasses and adds them to the vessel Classes Lexicon
local function findVesselClasses {
    Parameter searchDirPath to "0:".
    local dir to choose Open(searchDirPath) if searchDirPath:isType("String") else searchDirPath.
    if dir:isFile() {
        if dir:extension = vesselClassFileExtension {
            addVesselClass(loadVesselClass(dir)).
        }
    } else {
        for f in dir:list:values {
            findVesselClasses(f).
        } 
    }
    return vesselClasses.
}

printLog("Loaded " + ScriptPath():name + " in " + round(time:seconds - loadTime, 2) + "s").