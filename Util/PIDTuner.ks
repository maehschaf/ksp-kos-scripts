@LazyGlobal Off.
//Helps to test and tune PIDloops
Parameter ploop is "None", loopName is "".

local loadTime to time:seconds.

//If a loop is given as argument -> Tune it!
if ploop <> "None" {
    tunePIDloop(ploop, loopName).
}

global function tunePIDloop {
    //Main function to call on a PID to tune and analyze it
    parameter pidloop, name is "".

    initTuner(pidloop, name).
}

local function initTuner {
    Parameter pidloop, name is "".
    Local tuneGui to Lexicon().
    set tuneGui:gui to GUI(200).
    set tuneGui:disableWatcher to False.

    //Header
    set tuneGui:lHeader to tuneGui:gui:addLabel("<b>" + name + " PID Tuner</b>").
    set tuneGui:lHeader:style:fontsize to 19.
    set tuneGui:lHeader:style:textcolor to yellow.
    set tuneGui:lHeader:style:align to "center".

    set tuneGui:main to tuneGui:gui:addVBox().

    set tuneGui:linput to tuneGui:main:addLabel("").
    set tuneGui:loutput to tuneGui:main:addLabel("").
    set tuneGui:loutputlimits to tuneGui:main:addLabel("").
    set tuneGui:lerror to tuneGui:main:addLabel("").
    set tuneGui:lpterm to tuneGui:main:addLabel("").
    set tuneGui:literm to tuneGui:main:addLabel("").
    set tuneGui:ldterm to tuneGui:main:addLabel("").
    set tuneGui:lerrorsum to tuneGui:main:addLabel("").
    set tuneGui:lchangerate to tuneGui:main:addLabel("").

    //TODO safe the setpoint in tunegui and implement that stuff so it cant be overidden by other programs
    set tuneGui:hlsetpoint to tuneGui:main:addHlayout().
    set tuneGui:cbsetpoint to tuneGui:hlsetpoint:addCheckbox("SetPoint: ", False).
    set tuneGui:tfsetpoint to tuneGui:hlsetpoint:addTextField("").
    set tuneGui:tfsetpoint:onconfirm to { parameter setpoint. if tuneGui:cbsetpoint:pressed { set pidloop:setpoint to setpoint:tonumber(pidloop:setpoint). set tuneGui:tfsetpoint:text to "" + pidloop:setpoint. } }.

    set tuneGui:hlpfactor to tuneGui:main:addHlayout().
    set tuneGui:cbpfactor to tuneGui:hlpfactor:addCheckbox("P-Factor: ", False).
    set tuneGui:tfpfactor to tuneGui:hlpfactor:addTextField("").
    set tuneGui:tfpfactor:onconfirm to { parameter pfactor. if tuneGui:cbpfactor:pressed { set pidloop:kp to pfactor:tonumber(pidloop:kp). set tuneGui:tfpfactor:text to "" + pidloop:kp. } }.

    set tuneGui:hlifactor to tuneGui:main:addHlayout().
    set tuneGui:cbifactor to tuneGui:hlifactor:addCheckbox("I-Factor: ", False).
    set tuneGui:tfifactor to tuneGui:hlifactor:addTextField("").
    set tuneGui:tfifactor:onconfirm to { parameter ifactor. if tuneGui:cbifactor:pressed { set pidloop:ki to ifactor:tonumber(pidloop:ki). set tuneGui:tfifactor:text to "" + pidloop:ki. } }.

    set tuneGui:hldfactor to tuneGui:main:addHlayout().
    set tuneGui:cbdfactor to tuneGui:hldfactor:addCheckbox("D-Factor: ", False).
    set tuneGui:tfdfactor to tuneGui:hldfactor:addTextField("").
    set tuneGui:tfdfactor:onconfirm to { parameter dfactor. if tuneGui:cbdfactor:pressed { set pidloop:kd to dfactor:tonumber(pidloop:kd). set tuneGui:tfdfactor:text to "" + pidloop:kd. } }.

    set tuneGui:bresetpid to tuneGui:main:addButton("Reset PID").
    set tuneGui:bresetpid:onclick to { pidloop:reset(). }. 

    //Todo Ziegler-Nichols methode + fertige werte in Datei schreiben buttons
    

    set tuneGui:dataFile to "0:/Data/PID" + name + ".txt".
    set tuneGui:hlwriteFile to tuneGui:main:addHLayout().
    set tuneGui:cbwritefile to tuneGui:hlwriteFile:addCheckbox("Write Data to file", True). //TEMP
    set tuneGui:hlwritefile:addButton("Clear File"):onclick to {writeDataToFile(tuneGui:dataFile, pidloop, 0).}.

    set tuneGui:gui:addButton("Close"):onclick to {tuneGui:gui:hide(). set tuneGui:disableWatcher to true.}.

    updatePIDTuneGui(pidloop, tuneGui).
    initPIDWatcher(pidloop, tuneGui).

    //TEMP
    writeDataToFile(tuneGui:dataFile, pidloop, 0).

    tuneGui:gui:show().

    set tuneGui:gui:x to -200.
}

local function initPIDWatcher {
    Parameter PidLoop, tuneGui.

    on pidloop:Lastsampletime {
        if tuneGui:disableWatcher {
            return False.
        }

        updatePIDTuneGui(PidLoop, tuneGui).
        return True.
    }
}

local function updatePIDTuneGui {
    Parameter loop, tuneGui.

    local decimal to 2. //Decimal places

    set tuneGui:linput:text to "Input: " + round(loop:input, decimal).
    set tuneGui:loutput:text to "Output: " + round(loop:output, decimal).
    set tuneGui:loutputlimits to "Min: " + loop:minoutput + " Max: " + loop:maxoutput.
    set tuneGui:lerror:text to "Error: " + round(loop:error, decimal).
    set tuneGui:lpterm:text to "PTerm: " + round(loop:pterm, decimal).
    set tuneGui:literm:text to "ITerm: " + round(loop:iterm, decimal).
    set tuneGui:ldterm:text to "DTerm: " + round(loop:dterm, decimal).
    set tuneGui:lerrorsum:text to "Errorsum: " + round(loop:errorsum, decimal).
    set tuneGui:lchangerate:text to "ChangeRate: " + round(loop:changeRate, decimal).

    if not tuneGui:cbsetpoint:pressed {
        set tuneGui:tfsetpoint:text to "" + loop:setpoint.
    }
    if not tuneGui:cbpfactor:pressed {
        set tuneGui:tfpfactor:text to "" + loop:kp.
    }
    if not tuneGui:cbifactor:pressed {
        set tuneGui:tfifactor:text to "" + loop:ki.
    }
    if not tuneGui:cbdfactor:pressed {
        set tuneGui:tfdfactor:text to "" + loop:kd.
    }

    if tuneGui:cbwritefile:pressed {
        writeDataToFile(tuneGui:dataFile, loop).
    }
}

local function writeDataToFile {
    parameter filePath, loop, setting is -1.
    //Writes the pid data to a file - set setting to 0 to clear the file, set a number other than -1 to keep the file at that line count

    local sep to ";".

    if not Exists(filePath) {
        Create(filePath).
    }
    local f to Open(filePath).
    if setting = 0 {
        f:clear().
    } else { //Clamps are temporary
        local graphClampMax to 2 * loop:maxoutput.//TEMP
        local graphClampMin to 2 * loop:minoutput.//TEMP
        f:writeln(time:seconds + sep + loop:setpoint + sep + loop:input + sep + clamp(loop:pterm, graphClampMax, graphClampMin) + sep + clamp(loop:iterm, graphClampMax, graphClampMin) + sep + clamp(loop:dterm, graphClampMax, graphClampMin) + sep + loop:output + sep + (ship:q * constant:atmtokpa) + sep + ship:sensors:acc:mag).
    }
}

//Gui functions

printLog("Loaded " + ScriptPath():name + " in " + round(time:seconds - loadTime, 2) + "s").