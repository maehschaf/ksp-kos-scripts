//A lib for vessel info -- by maehschaf


//Load libs
RunOncePath("0:basic_lib.ks").
RunOncePath("0:vessel_class.ks").

local loadTime to time:seconds.

Global vesselInfo to "None". //Needs to be explicitly set

Global vesselInfoDefaultPath to path("1:/VesselInfo.json").//path(core:volume):combine(":/VesselInfo" + ".json").

//Class
Global Function VesselInformation {
    Parameter name.
    Parameter class. //class is just the name of the class

    local self is Lexicon().

    set self:name to name.
    set self:class to class.
    set self:steeringfactors to Lexicon().

    init(self).

    return self.
}

//initialize all functions
local function init {
    Parameter self.

    initTypeFunctionOverrides(self, "VesselInformation").

    set self:getVesselClass to {
        return getVesselClassByName(self:class).
    }.
    
    set self:runBootScript to {
        if self:hasKey("bootfile") and IsFile(self:bootfile) {
            runpath(self:bootfile).
        } else if self:getVesselClass() <> "None" {
            self:getVesselClass():runBootScript().
        } else {
            printLog("Error: Could not find bootfile of VesselClass " + self:name + ".").
        }
        
    }.

    set self:initSteering to {
        Parameter steeringPhase is "default", forceVesselClassFactors is False.
        if forceVesselClassFactors or (not self:steeringfactors:hasKey(steeringPhase)) {
            self:getVesselClass():initSteering(steeringPhase).
        } else {
            RunOncePath("0:steering_lib.ks").
            initSteering(self:steeringfactors[steeringPhase]).
        }
    }.

    set self:get to {
        //Gets the variable with the given name from either this VesselInfo or the current VesselClass
        Parameter name, default is "None", getClassValue is False.
        if self:hasKey(name) and not getClassValue {
            return self[name].
        } else if self:getVesselClass() <> "None" {
            return self:getVesselClass():get(name, default).
        } else {
            return default.
        }
    }.

    set self:set to {
        //Sets the variable with the given name on this VesselInfo
        Parameter name, value, onlyIfLocal is True. //If onlyIfLocal is True it wont add a new name when the current value comes from the vesselClass
        if self:hasKey(name) or (not onlyIfLocal) {
            set self[name] to value.
        }
    }.

    //Saves self vesselInfo to the given path and filename
    set self:save to {
        Parameter path is vesselInfoDefaultPath.
        saveVesselInformation(self, path).
    }.
}

//Load and save
global function saveVesselInformation {
    Parameter vi, filepath is vesselInfoDefaultPath.

    WriteJson(vi, filepath).
}

global function loadVesselInformation {
    parameter filepath is vesselInfoDefaultPath.
    local self is ReadJson(filepath).

    init(self).
    
    if self:steeringfactors:length > 0 {
        RunOncePath("0:steering_lib.ks").
        for k in self:steeringfactors:keys {
            set self:steeringfactors[k] to loadSteeringFactors(self:steeringfactors[k]).
        }
    }

    return self.
}

global function startVesselNameWatcher {
    when (ship:name <> vesselInfo:name) then {
        set ship:name to vesselInfo:get("name", getDefaultShipName("Unnamed Vessel")).
        return True.
    }
    wait 0.
}

printLog("Loaded " + ScriptPath():name + " in " + round(time:seconds - loadTime, 2) + "s").