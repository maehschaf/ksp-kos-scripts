//Controlled entry into an atmosphere with a vessel that has some kind of lifting bodys / control surfaces
Parameter targetGeoPos, endAtVelocity is 900, minPitch is 20, maxPitch is 50, RetrogradeEntry is false. //This program will end as soon as the velocity is lower that the given value.

RunOncePath("0:basic_lib.ks").
RunOncePath("0:atmospheric_steering_lib.ks").

local loadTime to time:seconds.
sas off.

FOR n IN ALLNODES {
    REMOVE n.
}

if addons:tr:Retrograde and not RetrogradeEntry {
    set Addons:tr:Prograde to true.
}

if not addons:tr:hasimpact {
    runoncepath("0:orbital_maneuvers_lib.ks").
    local node is makeImpactAtGeoPositionManeuver(latlng(targetGeoPos:lat, targetGeoPos:lng)).
    executeManeuverNode(node).
    remove node.
}

addons:tr:settarget(targetGeoPos).

//local lock impactRelativeToTarget to vectorRelativeGeoCoords(targetGeoPos:position - Addons:tr:impactpos:position).
local lock trTargetPitch to clamp(getPitch(addons:tr:plannedvector), minPitch, maxPitch).

local bankingDir to 1.

lock lngLoc to (addons:tr:impactpos:lng - targetGeoPos:lng).

local lngPid to PidLoop(-100, 0, 0, 0, 90).
set lngPid:setpoint to 0.

//Steering
lock steering To entrySteering(ship:Velocity:surface, trTargetPitch, lngPid:update(time:seconds, lngLoc) * bankingDir).


//Rcs corrections high up
when Altitude < body:atm:height then {
    rcs on.
    when ship:q > 0.01 then {
        rcs off.
    }
}

ClearScreen.
print "Controlled Entry".

until Airspeed < endAtVelocity {
    print ship:q at (0, 2).
    print "Time till impact: " + time(addons:tr:timetillimpact):clock at (0, 3).

    print "Longitudinal distance in degrees: " + round(lngLoc, 4) at (0, 5).
    print "Distance (impact to target) in km: " + round((geoDistance(Addons:tr:impactpos, targetGeoPos) / 1000), 2) at (0, 6).

    print "Bank angle trgt: " + round(lngPid:output, 2) * bankingDir at (0, 8).
    
    //Bank left and right to keep heading.
    local relativeHeading to getRelativeHeading(targetGeoPos:position, ship:Velocity:surface).
    if abs(relativeHeading) > 90 {
        set relativeHeading to abs(relativeHeading) - 180.
    }

    print "RelativeHeading: " + relativeHeading at (0,9).
    if ship:q > 0.01 { //Roll reversals to fly towards the correct heading

        if relativeHeading > 0.5 or (lngPid:output < 5 and relativeHeading > 0.1) {
            set bankingDir to 1.
        } else if relativeHeading < -0.5 or (lngPid:output < 5 and relativeHeading < -0.1){
            set bankingDir to -1.
        }
    }

    //RCS
    set ship:control:fore to -lngLoc * 2.
}

set ship:control:fore to 0.


printLog("Loaded " + ScriptPath():name + " in " + round(time:seconds - loadTime, 2) + "s").