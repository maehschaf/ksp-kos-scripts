runoncepath("0:atmospheric_steering_lib.ks").

wait 0.1.

set config:IPU to max(1200, config:IPU).

on Ag10 {//Manual Override
    unlock steering.
    SET SHIP:CONTROL:NEUTRALIZE to True.
    unlock throttle.
    unlock WheelSteering.
    Preserve.
}

setupWarningSounds().

//Changing stuff
//local destinationRunwayStart to waypoints["IslandRunwayWestEnd"].
//local destinationRunwayEnd to waypoints["IslandRunwayEastEnd"].

//local destinationRunwayStart to waypoints["DesertRunwayNorthEnd"].
//local destinationRunwayEnd to waypoints["DesertRunwaySouthEnd"].

local destinationRunwayStart to waypoints["KSCRunwayEastEnd"].
local destinationRunwayEnd to waypoints["KSCRunwayWestEnd"].

local departureRunwayStart to waypoints["KSCRunwayWestEnd"].
local departureRunwayEnd to waypoints["KSCRunwayEastEnd"].

if status = "landed" or status = "prelaunch" or status = "splashed" {
    if departureRunwayStart["GeoPos"]:position:mag < destinationRunwayStart["GeoPos"]:position:mag {
        travelFromRunwayToRunway(departureRunwayStart, departureRunwayEnd, destinationRunwayStart, destinationRunwayEnd, 15000, 1050, 5000). // at 15000 speed of sound is 295.16 m/s -> mach 3.5 = 1033 m/ s
    } else {
        travelFromRunwayToRunway(destinationRunwayEnd, destinationRunwayStart, departureRunwayStart, departureRunwayEnd, 15000, 1050, 5000).
    }
} else {
    if departureRunwayStart["GeoPos"]:position:mag < destinationRunwayStart["GeoPos"]:position:mag {
        travelFromRunwayToRunway(departureRunwayStart, departureRunwayEnd, destinationRunwayStart, destinationRunwayEnd, 15000, 1050, 5000).
    } else {
        travelFromRunwayToRunway(destinationRunwayEnd, destinationRunwayStart, departureRunwayStart, departureRunwayEnd, 15000, 1050, 5000).
    }
}

//End changing stuff
wait 3.
reboot.
print "Done".