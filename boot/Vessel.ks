//General Bootscript that starts the Gui and Vessel class based stuff TODO better description -- by maehschaf
wait until ship:unpacked.

local loadTime to time:seconds.

//Load libs
RunOncePath("0:/basic_lib.ks").
playNote(500, 0.2). //Hello there!
RunOncePath("0:/vessel_info.ks").
RunOncePath("0:/vessel_class.ks").

RunOncePath("0:/tts_lib.ks").

//Load Guis
RunOncePath("0:/Gui/utils_gui.ks").
RunOncePath("0:/Gui/vessel_class_gui.ks").
RunOncePath("0:/Gui/vessel_info_gui.ks").

ClearGuis().

printLog("Done! (" + round(time:seconds - loadTime, 2) + "s)").

//Try to load vessel Info or create new if it can not be found
if Exists(vesselInfoDefaultPath) {
    set vesselinfo to loadVesselInformation().
} else {
    local class to getVesselClassFromShipName(). //Try to automatically select the class from the Name
    if class = "None" {
        wait until KUniverse:activevessel = ship. //Make sure we dont get random popups from other craft
        
        local selectClassGui to SelectVesselClassGui(ship:name).

        wait until selectClassGui:done.
        set class to selectClassGui:selected.
    }
    printLog("Selected VesselClass: " + class:name).

    set vesselInfo to VesselInformation(getDefaultShipName(class:name), class).
    if not class:skipVesselInfoGui { 
        wait until KUniverse:activevessel = ship.

        local infoSettingsGui to VesselInfoSettingsGui(vesselInfo, {}, {Parameter info.}, False).

        wait until infoSettingsGui:done.
    }
}

startVesselNameWatcher().
setTTSEnabled(vesselInfo:get("tts", True)).

//Lets say hello!
printLog("Welcome aboard " + ship:name + "!").
ttsLog("Welcome aboard " + ship:name + "!", 5, MessagePriority:INFO).

//Setup inflight GUI


//Run the bootscript
vesselInfo:runBootScript().
wait until False. //TODO: Start waiting for Tasks