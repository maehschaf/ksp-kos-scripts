ON AG1 {
	SET runMode TO runMode - 1.
	SET updateSettings TO true.
	//return true.
}
ON AG2 {
	SET runMode TO runMode + 1.
	SET updateSettings TO true.
	//return true.
}

CLEARSCREEN.
if addons:tr:available() = false {
	print "Trajectories mod is not installed or is the wrong version." at(0,8).
	print "Script will fail, but you may press 1 to launch anyway." at(0,9).
} else {
	print "Press 1 to launch." at(0,9).
}
print "Press 1 to launch." at(0,9).
RUNONCEPATH("0:land_lib.ks"). //Includes the function library
RunOncePath("0:basic_lib.ks").
RUNONCEPATH("0:Starship/Starship.ks").
runoncepath("0:vessel_info_lib.ks", "Starship Booster", BODY:NAME + " Starship Booster").

SET steeringDir TO 90. //0-360, 0=north, 90=east
SET steeringPitch TO 90. // 90 is up
LOCK STEERING TO HEADING(steeringDir,steeringPitch).

//see next comment
SET steeringArrow TO VECDRAW().
SET steeringArrow:VEC TO HEADING(steeringDir,steeringPitch):VECTOR.
SET steeringArrow:SCALE TO 7.
SET steeringArrow:COLOR TO RGB(1.0,0,0).
SET retroArrow TO VECDRAW().
SET retroArrow:VEC TO RETROGRADE:VECTOR.
SET retroArrow:SCALE TO 7.
SET retroArrow:COLOR TO RGB(0,1.0,0).
SET lpArrow TO VECDRAW().
SET lpArrow:VEC TO RETROGRADE:VECTOR.
SET lpArrow:SCALE TO 7.
SET lpArrow:COLOR TO RGB(0,0,1.0).

//uncomment these and lines 224 & 225 for some debug arrows
//SET steeringArrow:SHOW TO true.
//SET retroArrow:SHOW TO true.
//SET lpArrow:SHOW TO true.

for b in boosterEngines {
	b:ACTIVATE().
}
set ship:control:pilotmainthrottle to 0.
core:part:controlfrom().
SET thrott TO 0.
LOCK THROTTLE TO thrott.
SAS OFF.
RCS OFF.

LOCK radar TO choose terrainDist(addons:tr:impactpos) if addons:tr:HASIMPACT else terrainDist().
SET radarOffset TO 45.//ship:altitude - radar. //rocket should be on ground at this point
SET launchPad TO waypoints["KSCLandingPad1"]["GeoPos"].//SHIP:GEOPOSITION.//LATLNG(-0.0972077635067718, -74.5576726244574).//LATLNG(-0.529363813786219, -72.5739690220555).//LATLNG(-0.0969914507773016, -74.6168152630095).//VAB//
SET gravityTurnMidwayAlt TO 13000.
LOCK targetDist TO geoDistance(launchPad, ADDONS:TR:IMPACTPOS).
LOCK targetDir TO geoDir(ADDONS:TR:IMPACTPOS, launchPad).
SET cardVelCached TO cardVel().
SET targetDistOld TO 0.
//g in m/s^2 at sea level.
SET g TO constant:G * BODY:Mass / (BODY:RADIUS + Altitude / 3)^2. //Average gravity between surface and current
LOCK maxVertAcc TO SHIP:AVAILABLETHRUSTAT(1) / SHIP:MASS. //max acceleration in up direction the engines can create
LOCK dragAcc TO scalarProj(SHIP:SENSORS:ACC, UP:VECTOR) - g - maxVertAcc * Throttle. //vertical acceleration due to drag. Same as g at terminal velocity

lock acc to abs(maxVertAcc) + abs(dragAcc) - g.
// Burn time to reach 0 vertical velocity
LOCK sBurnTime TO abs(SHIP:verticalspeed / acc).
//Distance in vacuum = Vi*t + 1/2*a*t^2
LOCK sBurnDist to abs((ship:verticalspeed * sBurnTime) + 0.5 * ( acc * sBurnTime^2)).
//LOCK sBurnDist2 TO (SHIP:verticalspeed)^2 / (2 * (maxVertAcc + dragAcc/2)).//-SHIP:VERTICALSPEED * sBurnTime + 0.5 * -maxVertAcc * sBurnTime^2.//SHIP:VERTICALSPEED^2 / (2 * maxVertAcc). 


SET stopLoop TO false.
//0 = landed, 1 = final decent, 2 = hover/manouver, 3 = suicide burn, 4 = falling, 5 = boostback, 6 = launching, 7 = nothing
SET runMode TO 7.
SET updateSettings TO true.

SET climbPID TO PIDLOOP(0.4, 0.3, 0.005, 0, 1). //Controls vertical speed
SET hoverPID TO PIDLOOP(1, 0.01, 0.0, -15, 15). //Controls altitude by changing climbPID setpoint
SET hoverPID:SETPOINT TO 114. //87 is the altitude about 7 meters above launch pad
SET eastVelPID TO PIDLOOP(3, 0.01, 0.0, -15, 15). //Controls horizontal speed by tilting rocket
SET northVelPID TO PIDLOOP(3, 0.01, 0.0, -15, 15).
SET eastPosPID TO PIDLOOP(1700, 0, 100, -10, 10). //controls horizontal position by changing velPID setpoints
SET northPosPID TO PIDLOOP(1700, 0, 100, -10, 10).
SET eastPosPID:SETPOINT TO launchPad:LNG.
SET northPosPID:SETPOINT TO launchPad:LAT.

WHEN runMode = 6 THEN {
	SET thrott TO 1.
	GEAR OFF.
	SET updateSettings TO true.
	WHEN APOAPSIS > 60000 THEN {//STAGE:LIQUIDFUEL < landingFuel AND STAGE:LIQUIDFUEL > 0 THEN { //Does not seem to work on some vessels
		PRINT STAGE:LIQUIDFUEL AT(0,17).
		SET thrott TO 0.
		SET runMode TO 5.
		SET updateSettings TO true.
		WHEN runMode = 4 THEN { //When falling
			SET updateSettings TO true.
			SET thrott TO 0.
			SET NAVMODE TO "surface".
			when body:atm:altitudepressure(altitude) > 0.05 then {
				RCS Off.
			}
			WHEN sBurnDist > radar - radarOffset AND SHIP:VERTICALSPEED < -5 THEN {//When there is barely enough time to stop before reaching altitude 90.
				LOG "burn start alt: " + (radar - radarOffset) TO burn.txt.
				LOG "burn est: " + sBurnDist TO burn.txt.
				SET runMode TO 3.
				SET updateSettings TO true.
				SteeringManager:resettodefault().
				BRAKES ON.
				SET thrott to 1.
				SET NAVMODE TO "surface".
				WHEN SHIP:VERTICALSPEED > -5 THEN { //When it has stopped falling
					LOG "burn end alt: " + radar TO burn.txt.
					//SET runMode TO 2. Ignore that for now
					GEAR ON.
					SET updateSettings TO true.
					//WHEN geoDistance(SHIP:GEOPOSITION, launchPad) < 5 THEN { //When it is over the launch pad - we dont care for now
						SET runMode TO 1.
						WHEN SHIP:STATUS = "LANDED" THEN {
							SET runMode TO 0.
							SET updateSettings TO true.
							SET thrott TO 0.
						}
					//}
				}
			}
		}
	}
}

UNTIL stopLoop = true { //Main loop
	if runMode = 7 {
		if updateSettings = true {
			UNLOCK THROTTLE.
			UNLOCK STEERING.
			SET updateSettings TO false.
		}
	}	
	//Gravity turn
	if runMode = 6 {
		if updateSettings = true {
			IF vesselInfo["FirstLaunch"] = -1 {
				SET vesselInfo["FirstLaunch"] TO TIME:SECONDS.
			}
			vesselInfo["PayloadLog"]:ADD(starshipControl:getModule("kOSProcessor"):VOLUME:NAME).
			updateVesselInfo().

			LOCK STEERING TO HEADING(steeringDir,steeringPitch).
			LOCK THROTTLE TO thrott.

			SET updateSettings TO false.
			CLEARSCREEN.
		}
		if ALTITUDE <= 100 {
			SET steeringPitch TO 88.
		} else if ALTITUDE < gravityTurnMidwayAlt {
			SET steeringPitch TO 45 + MAX(0, (45 * (gravityTurnMidwayAlt - SHIP:ALTITUDE) / gravityTurnMidwayAlt)).
			SET midwayApoapsis TO APOAPSIS.
		} else {
			SET steeringPitch TO MIN(45, MAX(45 * ((70000 - SHIP:APOAPSIS) / (70000 - midwayApoapsis)), (90 - VECTORANGLE(UP:VECTOR, SRFPROGRADE:FOREVECTOR)) - 5)).
		}
	}
	if runMode = 5 { //boostback
		if updateSettings = true {
			RCS ON.
			SAS OFF.
			BRAKES ON.
			SET thrott TO 0.

			//temp
			set ship:control:mainthrottle to 0.5.
			for e in starshipVacEngines {
				e:activate().
			}
			wait 0.

			decoupleBooster().
			RCS ON.
			SAS OFF.
			LOCK STEERING TO HEADING(steeringDir,steeringPitch, 0).
			SteeringManager:resettodefault().
			set SteeringManager:maxstoppingtime to 20.
			wait 0.1.
			KUniverse:FORCESETACTIVEVESSEL(SHIP).
			RCS ON.
			SET SHIP:SHIPNAME TO CORE:VOLUME:NAME.
			WAIT UNTIL Kuniverse:activevessel = ship.
			boosterControl:controlfrom().
			SET updateSettings TO false.
		}
		if ADDONS:TR:HASIMPACT = true { //If ship will hit ground
			SET steeringDir TO targetDir - 180. //point towards launch pad
			SET steeringPitch TO 0.
			if VANG(HEADING(steeringDir,steeringPitch):VECTOR, SHIP:FACING:VECTOR) < 20 {  //wait until pointing in right direction
				SET thrott TO targetDist / 5000 + 0.1.
			} else {
				SET thrott TO 0.
			}
			if targetDist > targetDistOld AND targetDist < 300 {
				SET thrott TO 0.
				set SteeringManager:maxstoppingtime to 2.
				SET runMode TO 4.
			}
			SET targetDistOld TO targetDist.
		}
	}
	if runMode = 4 { //Glide rocket back to launch pad.
		SET shipProVec TO (SHIP:VELOCITY:SURFACE * -1):NORMALIZED.
		if SHIP:VERTICALSPEED < -10 {
			SET launchPadVect TO (launchPad:POSITION - ADDONS:TR:IMPACTPOS:POSITION):NORMALIZED. //vector with magnitude 1 from impact to launchpad
			SET rotateBy TO MIN(targetDist*2, 15). //how many degrees to rotate the steeringVect
			PRINT "rotateBy: " + rotateBy at(0,7).
			SET steeringVect TO shipProVec * 40. //velocity vector lengthened
			SET loopCount TO 0.
			UNTIL (rotateBy - VANG(steeringVect, shipProVec)) < 3 { //until steeringVect gets close to desired angle
				PRINT "entered loop" at(0,9).
				if VANG(steeringVect, shipProVec) > rotateBy { //stop from overshooting
					PRINT "broke loop" at(0,9).
					BREAK.
				}
				SET loopCount TO loopCount + 1.
				if loopCount > 100 {
					PRINT "broke infinite loop" at(0,10).
					BREAK.
				}
				SET steeringVect TO steeringVect - launchPadVect. //essentially rotate steeringVect in small increments by subtracting the small vector.
			}
			PRINT "steeringAngle: " + VANG(steeringVect, shipProVec) at(0,8).
			SET steeringArrow:VEC TO steeringVect:NORMALIZED. //RED
			SET retroArrow:VEC TO shipProVec. //GREEN
			SET lpArrow:VEC TO launchPadVect:NORMALIZED. //BLUE
			LOCK STEERING TO steeringVect:DIRECTION.
		} else {
			LOCK STEERING TO (shipProVec):DIRECTION.
		}
	}
	if runMode = 3 {//Suicide burn. Mainly handled by WHEN on line ~38
		if updateSettings = true {
			SET eastVelPID:MINOUTPUT TO -7.//-5
			SET eastVelPID:MAXOUTPUT TO 7.
			SET northVelPID:MINOUTPUT TO -7.
			SET northVelPID:MAXOUTPUT TO 7.
			SET steeringDir TO 0.
			SET steeringPitch TO 90.
			LOCK STEERING TO HEADING(steeringDir,steeringPitch).
			SET updateSettings TO false.
		}
		SET cardVelCached TO cardVel().
		steeringPIDs().
	}
	if runMode = 2 { //Powered flight to launch pad
		if updateSettings = true {
			SAS OFF.
			SET eastVelPID:MINOUTPUT TO -5. //35.
			SET eastVelPID:MAXOUTPUT TO 5.
			SET northVelPID:MINOUTPUT TO -5.
			SET northVelPID:MAXOUTPUT TO 5.
			SET updateSettings TO false.
		}
		SET cardVelCached TO cardVel().
		SET climbPID:SETPOINT TO hoverPID:UPDATE(TIME:SECONDS, SHIP:ALTITUDE). //lower ship down while flying to launch pad
		SET thrott TO climbPID:UPDATE(TIME:SECONDS, SHIP:VERTICALSPEED).
		steeringPIDs().
	}
	if runMode = 1 { //Final landing
		set eastPosPID:setpoint to ship:geoposition:lng.
		set northPosPID:setpoint to ship:geoposition:lat.
		SET cardVelCached TO cardVel().
		steeringPIDs().
		SET climbPID:SETPOINT TO MAX(radar - radarOffset, 3) * -1.
		PRINT "climbPID:SETPOINT: " + climbPID:SETPOINT at(0,8).
		SET thrott TO climbPID:UPDATE(TIME:SECONDS, SHIP:VERTICALSPEED).
	}
	if runMode = 0 {
		RCS OFF.
		SET thrott TO 0.
		SET updateSettings TO false.
	}

	printData2().
	//SET steeringArrow:VEC TO HEADING(steeringDir,steeringPitch):VECTOR.
	//SET steeringArrow:VEC TO launchPad:POSITION - ADDONS:TR:IMPACTPOS:POSITION.
	WAIT 0.01.
}
function printData2 {
	PRINT "runMode: " + runMode AT(0,1).
	PRINT "radar: " + ROUND(radar, 4) AT(0,2).

	PRINT "sBurnDist: " + ROUND(sBurnDist, 4) AT(0,3).
	PRINT "Vertical speed target: " + ROUND(climbPID:SETPOINT, 4) AT(0,4).
	PRINT "VERTICALSPEED: " + ROUND(SHIP:VERTICALSPEED, 4) AT(0,5).
	if ADDONS:TR:HASIMPACT = true { PRINT "Impact point dist from pad: " + ROUND(targetDist,4) at(0,6). }
	print "Time to Stop: " + round(sBurnTime,2) at(0,10).
	print "Dist to Stop: " + round(sBurnDist,2) at(0,11).

}