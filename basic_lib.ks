//Basic lib for usefull stuff -- by maehschaf

local loadTime to time:seconds.

//Important stuff
global optimalAngleOfAttack to 30.

//Message Prioritys for logging
global MessagePriority to Lexicon("ERROR", 0, "WARNING", 1, "INFO", 2, "DEBUG", 3).

function openTerminal {
	CORE:PART:GETMODULE("kOSProcessor"):DOEVENT("Open Terminal").
}

function playNote {
	PARAMETER freq IS 400, time IS 1.
	GETVOICE(0):PLAY(NOTE(freq, time)).
}

function println {
	parameter string, line, column is 0.
	print (string + ""):padright(terminal:width) at(column, line).
}

function printLog {
	//Use to log messages, implementation might change
	parameter string, messagePrio is MessagePriority:INFO, tts to False.
	PRINT string.
	if tts {
		RunOncePath("0:/tts_lib.ks").
		ttsLog(string, messagePrio).
	}
}

function isFile {
	Parameter path.
	if Exists(path) {
		return open(path):isFile().
	}
	return False.
}

global function initTypeFunctionOverrides {
	//For semi object oriented programming with lexicons -> Overwrite getType and isType
	parameter self, type.
    set self:serializedtype to type.
    
    set self:gettype to {
        return self:serializedtype.
    }.

	//Cant overwrite isType
}

global function isStatusLanded {
	//More inclusive is landed. Returns true whenever the vessel is not flying or orbiting
	parameter vessel is ship.
	return vessel:status = "landed" or vessel:status = "splashed" or vessel:status = "prelaunch".
}

global function getGravityAcceleration {
	parameter b is body, alti is Altitude.
	return b:mu / (alti + b:radius)^2.
}

global function getMachNumber {
	Parameter vessel is ship.
	if vessel:body:atm:adiabaticindex = 0 or vessel:body:atm:altitudePressure(vessel:Altitude) = 0 {
		return 0.
	}
	return Sqrt((2 / vessel:body:atm:adiabaticindex) * (vessel:q / vessel:body:atm:altitudePressure(vessel:Altitude))).
}

global function getAvailableAcceleration {
	//Acceleration the active engines can provide
	Parameter vessel is ship.
	return (vessel:availablethrust() / vessel:mass).
}

local linAccPrevVessel is Ship.
local linAccPrevVelocity to linAccPrevVessel:velocity:surface.
local linAccPrevTime to time:seconds.
local linAccPrev to V(0,0,0).

global function getLinearAcceleration {
	//Not tested - gets the change in velocity
	Parameter vessel is ship, maxDeltaTime is 2.
	local deltaTime to time:seconds - linAccPrevTime.

	if (not (linAccPrevVessel = vessel)) or (deltaTime > maxDeltaTime) {
		set linAccPrevVelocity to vessel:velocity:surface.
		set linAccPrevTime to time:seconds.
		set linAccPrevVessel to vessel.
		set linAccPrev to V(0,0,0).
	}

	if deltaTime = 0 {
		set deltaTime to 0.00000001.
		return linAccPrev.
	}

	set linAccPrev to (linAccPrevVelocity - ship:velocity:surface) / deltaTime.
	set linAccPrevVelocity to vessel:velocity:surface.
	set linAccPrevTime to time:seconds.
	set linAccPrevVessel to vessel.
    //println("Updated linear acceleration, delta time: " + deltaTime + "  -  " + time:second, 20).
	return linAccPrev.
}

local angVelPrevVessel is Ship.
local angVelPrevTime is time:seconds.
local angVelPrevFacing is angVelPrevVessel:facing.
local angVelPrevResult is V(0,0,0).

global function getAngularVelocity {
	//x = pitch, y = yaw, z = roll | unit = degrees / second.. Note that the Kerbal Engineer readout is in sin(degrees / second) or something
	parameter vessel is ship, maxDeltaTime is 2.
    local deltaTime to time:seconds - angVelPrevTime.

	if (not (angVelPrevVessel = vessel)) or (deltaTime > maxDeltaTime) {
		set angVelPrevFacing to vessel:Facing.
		set angVelPrevTime to time:seconds.
		set angVelPrevVessel to vessel.
		set angVelPrevResult to V(0,0,0).
	}

	if deltaTime = 0 {
		set deltaTime to 0.00000001.
		return angVelPrevResult.
	}

    local foreVec to ((vessel:facing:forevector - angVelPrevFacing:forevector) * vessel:facing:inverse).
	local topVec to ((vessel:facing:topvector - angVelPrevFacing:topvector) * vessel:facing:inverse).

	set angVelPrevFacing to vessel:Facing.
	set angVelPrevTime to time:seconds.
	set angVelPrevVessel to vessel.
	set angVelPrevResult to V(arcsin(clamp(foreVec:y / deltaTime, -1, 1)), arcsin(clamp(foreVec:x / deltaTime, -1, 1)), arcsin(clamp(topVec:x / deltaTime, -1, 1))).
    //println("Updated angular vel, delta time: " + deltaTime + "  -  " + time:second, 20).
	return angVelPrevResult.
}

local angVelCache to List().

function getSmoothedAngularVelocity {
	//Smoothes the angular velocity by taking the average over the given smoothing period
	parameter smoothingPeriod, vessel is ship, maxDeltaTime is 2.

	local newAngVel to getAngularVelocity(vessel, maxDeltaTime).
	angVelCache:insert(0, List(time:seconds, newAngVel:x, newAngVel:y, newAngVel:z)).

	local sum to V(0,0,0).//Add the velocity
	for i in range(angVelCache:length , 0) {
		local cached to angVelCache[i - 1].
		if time:seconds > cached[0] + smoothingPeriod {//Remove data that is too old
			angVelCache:remove(i - 1).
		} else {
			local prevMeasureTime to choose angVelCache[i][0] if i < angVelCache:length else time:seconds - smoothingPeriod.//Oldest measurement is used till cutoff time
			set sum to sum + V(cached[1], cached[2], cached[3]) * (cached[0] - prevMeasureTime). //Weighted average
		}
	}

	local smoothedAngVel to V(sum:x / smoothingPeriod, sum:y / smoothingPeriod, sum:z / smoothingPeriod).
	//println(smoothedAngVel, 21).
	//println(angVelCache:dump(), 22).

	return smoothedAngVel.
}

function vectorRelativeDirection {
	//Returns a vector relative to a Direction. //Probably wrong: V(left/right, up/down, fore/back)
	PARAMETER vector, direction.
	if direction:istype("Vector") {
		set direction to lookdirup(direction, up:forevector).
	}
	return vector * direction:inverse.
}

function sign {
	Parameter num.
	return choose 0 if num = 0 else num / abs(num).
}

function linearAccelerationVelocity {
	//Returns the Velocity reached when accelerating with the given acceleration for the given distance
	Parameter acceleration, distance, initialVelocity is 0.
	return Sqrt(2 * acceleration * abs(distance) + initialVelocity^2).
}

function linearAccelerationDistance {
	//Returns the time and distance needed to accelerate.
	Parameter acceleration, targetSpeed, initialSpeed is 0.
	local accTime to targetSpeed / acceleration.
	return initialSpeed * accTime + 0.5 * acceleration * accTime^2.
}


function accelerationNeededToReachVelocityInDistance {
	//Returns the acceleration needed to reach a certain speed after a certain distance
	Parameter targetVelocity, targetDistance. //TODO add initial Velocity
	return targetVelocity^2 / (2 * targetDistance).
}

function scalarProj {
	//Scalar projection of two vectors. Find component of a along b. a(dot)b/||b|| - Copied from land_lib.ks
	parameter a.
	parameter b.
	if b:mag = 0 { PRINT "scalarProj: Tried to divide by 0. Returning 1". RETURN 1. } //error check
	RETURN VDOT(a, b) * (1/b:MAG).
}

function vectorRelativeGeoCoords {
	//Vector relative to SOI/Ground - returns east,up,north - Copied from land_lib.ks
	Parameter vect.
	local eastVect is VCRS(UP:VECTOR, NORTH:VECTOR).
	local eastComp IS scalarProj(vect, eastVect).
	local northComp IS scalarProj(vect, NORTH:VECTOR).
	local upComp IS scalarProj(vect, UP:VECTOR).
	RETURN V(eastComp, upComp, northComp).
}

function cardVel {
	//Convert velocity vectors relative to SOI into easting and northing.
	return vectorRelativeGeoCoords(SHIP:VELOCITY:SURFACE).
}

function geoDistance { 
	parameter geo1.
	parameter geo2.
	Parameter vessel is ship.

	set theta to vang(geo1:position - vessel:body:position, geo2:position - vessel:body:position).
	return theta * constant:degtorad * body:radius.
}

function relativeAngleDelta {
	//Get the difference between two angles with the smallest absolute value
	parameter relativeAngle, baseAngle.
	local delta to relativeAngle - baseAngle.
	if (delta > 180) {
        set delta to delta - 360.
    } else  if (delta < -180) {
        set delta to delta + 360.
    }
	return delta.
}

function clamp {
	//Claps the value to be between limitA and limitB
	parameter value.
	parameter limitA.
	parameter limitB is -limitA.
	if limitA > limitB {
		return MIN(limitA, MAX(limitB, value)).
	} else {
		return MIN(limitB, MAX(limitA, value)).
	}
}

function findSmallest {
	//Finds the smallest Element in a collection
	parameter collection.
	local smallest to "None".
	for e in collection {
		if smallest = "None" or e < smallest {
			set smallest to e.
		}
	}
	return smallest.
}

function findLargest {
	//Finds the largest Element in a collection
	parameter collection.
	local largest to "None".
	for e in collection {
		if largest = "None" or e > largest {
			set largest to e.
		}
	}
	return largest.
}

function doesVesselExist {
	//Checks if a vessel with the given name exists.
	Parameter vesselName.
	list targets in vessels.
	for v in vessels {
		if v:name = vesselName {
			return True.
		}
	}
	return False.
}

//Waypoints
//Lib for making and saving waypoints
global waypoints is Lexicon().

loadWaypointsFromFile().

function addWaypoint {
	//Adds a waypoint
    parameter name, position, altitude is position:TERRAINHEIGHT, info is "None", onBody is body.
    if position:istype("Vector") {
        set altitude to onBody:altitudeOf(position).
		set position to onBody:GEOPOSITIONOF(position).
    }
    set waypoints[name] to lexicon("GeoPos", position, "Altitude", altitude, "Info", info).
	saveWaypointsToFile().
}

function getWaypointPos {
	//Gets the position of a waypoint as a vector
	parameter waypoint, seaLvlAlt is "Safed".
	if sealvlAlt = "Safed" {
		return waypoint["GeoPos"]:altitudeposition(waypoint["Altitude"]).
	} else {
		return waypoint["GeoPos"]:altitudeposition(sealvlAlt).
	}
}

function saveWaypointsToFile {
    parameter path is "0:waypoints.json".
    writejson(waypoints, path).
}

function loadWaypointsFromFile {
    parameter path is "0:waypoints.json".
	if exists(path) {
    	set waypoints to readjson(path).
	}
    return waypoints.
}
//End Waypoints


//Orientation

function getPitch {
    parameter dir is ship:facing, upd is UP.
    if dir:istype("Direction") {
        set dir to dir:forevector.
    }
	if upd:istype("Direction") {
        set upd to upd:forevector.
    }
    return 90 - vectorangle(upd, dir).
}

//Copied from https://github.com/KSP-KOS/KSLib/blob/master/library/lib_navball.ks
function getHeading {
    parameter dir is ship:facing.

    if dir:istype("Direction") {
        set dir to dir:forevector.
    }

    local pointing is dir.
    local east is vcrs(up:vector, north:vector).

    local trig_x is vdot(north:vector, pointing).
    local trig_y is vdot(east, pointing).

    local result is arctan2(trig_y, trig_x).

    if result < 0 { 
        return 360 + result.
    } else {
        return result.
    }
}

function getRelativeHeading {
    Parameter relativeDir, baseDirection is ship:facing.
    return relativeAngleDelta(getHeading(relativeDir), getHeading(baseDirection)).
}

function getRoll { //by lg421 on the kOS discord
    parameter dir is ship:facing, upVec is UP:FOREVECTOR.

    local lup is vxcl(dir:vector, upVec):normalized.
    local lhorz is vcrs(lup, dir:vector).
    return arctan2(vdot(dir:topvector,lhorz), vdot(dir:topvector,lup)).
}

//End Orientation

//Interpolation
global function createRateInterpolator {
	Parameter startValue, targetValue, rate.
	local interper to Lexicon().
	set interper:rate to rate.
	set interper:target to targetValue.
	set interper:lastUpdate to time:seconds.
	set interper:output to startValue.
	set interper:update to {
		Parameter t is time:seconds.
		local timeDelta to t - interper:lastUpdate.
		set interper:output to interper:output + clamp(interper:target - interper:output, interper:rate * timeDelta).
		set interper:lastUpdate to t.
		return interper:output.
	}.
	return interper.
}


printLog("Loaded " + ScriptPath():name + " in " + round(time:seconds - loadTime, 2) + "s").