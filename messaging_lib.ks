//A lib that allows adding handlers for messages sent through the kOS Message System. -- by maehschaf
parameter handleShipQueue is true.

local loadTime to time:seconds.

global messageHandlers to List().

addDefaultMessageHandlers().
if handleShipQueue {
    local timestamp to time:seconds.
    WHEN time:seconds > timestamp + 3 THEN {
        handleMessageQueue(SHIP:messages).
    }
}

//Send a Message to the given destination with the given content which is from the given type and therefore handled by a MessageHandler that accepts this type.
function sendMessage {
    parameter destination, content, type is "unknown".
    if destination:istype("String") {
        set destination to Vessel(destination).
    }
    local message to Lexicon("Type", type, "Content", content).
    print "Sending " + type + " message".
    return destination:connection:sendmessage(message).
}

//Add a new handler function -- A handler function must accept exactly one argument; The message to be handled which is a Lexicon:
// Type - determines the type of Message this handler should handle
// OnMessage - should be a function delegate accepting exactly one argument, a Message Lexicon. This function is called to handle the Message - Do whatever you want
// ConsumeMessage - determines if other handlers are allowed to handle the same message or if this one should be the last
function addMessageHandler {
    parameter type, onMessage, consumeMessage is false.
    messageHandlers:add(Lexicon("Type", type, "OnMessage", onMessage, "ConsumeMessage", consumeMessage)).
}

//This lib will start to handle all Messages received in the given queue
function handleMessageQueue {
    parameter queue.
    When not queue:empty then {
        handleMessage(queue:peek()).
        queue:pop().
        return true. //Continue Listening...
    }
}

local function handleMessage {
    parameter message.
    //Transfer message information to content lexicon
    if not message:content:haskey("SentAt") {
        message:content:add("SentAt", message:sentat).
        message:content:add("ReceivedAt", message:receivedat).
        message:content:add("Sender", message:sender).
        message:content:add("HasSender", message:hassender).
    }

    print message:content["Type"] + " Message received".
    
    for handler in messageHandlers {
        if message:content["Type"] = handler["Type"] or message:content["Type"] = "all" or handler["Type"] = "all" {

            handler["OnMessage"]:call(message:content).

            if handler["ConsumeMessage"] {
                break.
            }
        }
    }
}

//Default Handlers
local function addDefaultMessageHandlers {
    addMessageHandler("basic-runProgram", handleBasicRunProgramMessage@).
    addMessageHandler("basic-executeCode", handleBasicExecuteCodeMessage@).
}

//

//Run program -- Try to somehow get parameters to work!
local function handleBasicRunProgramMessage {
    parameter message.
    runpath(message["Content"]).
}

function sendRunProgramMessage {
    parameter destination, programPath.
    sendMessage(destination, programPath, "basic-runProgram").
}

//Execute Code via vessel_info_lib
local function handleBasicExecuteCodeMessage {
    parameter message.
    runoncepath("0:vessel_info_lib.ks").
    executeCode(message["Content"]).
}

function sendExecuteCodeMessage {
    parameter destination, programPath.
    sendMessage(destination, programPath, "basic-executeCode").
}

printLog("Loaded " + ScriptPath():name + " in " + round(time:seconds - loadTime, 2) + "s").