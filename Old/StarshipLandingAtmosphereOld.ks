//Starship Landing
runoncepath("0:Starship/StarshipParts.ks").

CLEARSCREEN.
PRINT "Starship Landing".
IF DEFINED targetPosition {
	PRINT "Target Position found: " + targetPosition.
}
SAS OFF.
RCS off.

CORE:PART:GETMODULE("kOSProcessor"):DOEVENT("Open Terminal").

SET secAlt TO 0.
SET radarOffset TO 0.//15.9.

RUNONCEPATH("0:land_lib.ks").

//Suicide burn "inspired" by land.ks
LOCK gravityAcceleration TO body:mu / (altitude + body:radius)^2.
LOCK maxAcc TO SHIP:AVAILABLETHRUST() / SHIP:MASS.
LOCK maxVerticalAcc TO maxAcc - gravityAcceleration.
LOCK dragAcc TO gravityAcceleration + scalarProj(SHIP:SENSORS:ACC, UP:VECTOR).
LOCK horizontalDragAcc TO scalarProj(SHIP:SENSORS:ACC, VectorExclude(Up:vector, ship:velocity:surface)).

LOCK sBurnTime TO -SHIP:VERTICALSPEED/maxVerticalAcc + GROUNDSPEED/maxAcc.
LOCK sBurnDist TO SHIP:VERTICALSPEED^2 / (2 * (maxVerticalAcc + dragAcc / 2)) + GROUNDSPEED^2 / (maxAcc + horizontalDragAcc).
LOCK sBurnDistVac TO SHIP:VERTICALSPEED^2 / (2 * maxVerticalAcc).

function slowResourceTransfer {
	parameter fromTank.
	parameter toTank.
	parameter transferTime.
	FROM {local x is transferTime + 0.1.} UNTIL x < 0 STEP {set x to x-0.1.} DO {
		SET backTransferOx TO TRANSFER("oxidizer", fromTank, toTank,  frontTanks[0]:RESOURCES[1]:Amount/transferTime*0.1).
		SET backTransferLf TO TRANSFER("liquidfuel", fromTank, toTank, frontTanks[0]:RESOURCES[0]:Amount/transferTime*0.1).
		SET backTransferOx:ACTIVE TO true.
		SET backTransferLf:ACTIVE TO true.
		WAIT 0.1.
	}
}

SET THROTTLE TO 0.

LOCK STEERING TO HEADING(90, 47.19) + R(0,0,0).
IF ADDONS:TR:HASIMPACT {
	LOCK STEERING TO ADDONS:TR:PLANNEDVECTOR.
}


FOR w IN frontWinglets {
	w:GETMODULE("ModuleControlSurface"):SETFIELD("authority limiter", 150).
}

HUDTEXT ("Transfering fuel to front", 10, 3, 20, rgb(1,1,0.5), true).
slowResourceTransfer(backTanks, frontTanks, 10).

HUDTEXT ("Transitioning to aerodynamic flight", 10, 3, 20, rgb(1,1,0.5), true).
WAIT UNTIL AIRSPEED < 900.
LOCK STEERING TO HEADING(90, 0).
IF ADDONS:TR:HASIMPACT {
	LOCK STEERING TO ADDONS:TR:correctedvector.
}
RCS OFF.

FOR e IN starshipEngines {
	HUDTEXT ("igniting " + e:title, 10, 3, 20, rgb(1,1,0.5), true).
	e:ACTIVATE().
}

CLEARSCREEN.
LOCK flipDistance TO sBurnDist + secAlt + -VERTICALSPEED * 4.

WAIT UNTIL flipDistance < ALT:RADAR - radarOffset.

UNTIL ALT:RADAR < flipDistance { //Do flip!! -------------<
	PRINT "Flip Altitude: " + flipDistance AT (0,1).
	PRINT "Suicide Burn Altitude: " + sBurnDist AT (0,2).
	PRINT "Suicide Burn Time: " + sBurnTime AT (0, 3).
	PRINT "Time till impact: " + addons:tr:timetillimpact AT (0, 4).
	PRINT "Altitude: " + ALT:RADAR AT(0,6).
	print addons:tr:timetillimpact <= sBurnTime at (0, 8).
}

FOR w IN frontWinglets {
	w:GETMODULE("ModuleControlSurface"):SETFIELD("authority limiter", 0).
	HUDTEXT ("disabeling " + w:title, 10, 3, 20, rgb(1,1,0.5), true).
}

//doflip
LOCK STEERING TO lookdirup(UP:FOREVECTOR * 1.5 + SRFRETROGRADE:FOREVECTOR, -north:starvector).
HUDTEXT ("Doing flip", 20, 3, 20, rgb(1,1,0.5), true).
SET t TO 0.
LOCK THROTTLE TO t.

WHEN ALT:RADAR - radarOffset < sBurnDist + secAlt THEN {
	SET t TO 1.
	HUDTEXT ("Starting Suicide Burn at " + ALT:RADAR, 20, 3, 20, rgb(1,1,0.5), false).
}

slowResourceTransfer(frontTanks, backTanks, 2).

WAIT UNTIL t > 0.
CLEARSCREEN.

when alt:radar < 80 then {
	GEAR ON.
}

LOCK targetSpeed TO MAX(ALT:RADAR / 10, 15).
WHEN ALT:RADAR - radarOffset < 20 THEN {
	SET targetSpeed TO 5.
	WHEN ALT:RADAR - radarOffset < 5 THEN {
		lock steering to LookDirUp(up:forevector + SRFRETROGRADE:FOREVECTOR * 0.1, -north:starvector).
		SET targetSpeed TO 0.5.
	}
}
UNTIL SHIP:STATUS = "SPLASHED" OR SHIP:STATUS = "LANDED" {
	PRINT "Suicide Burn Altitude: " + sBurnDist AT (0,2).
	PRINT "Suicide Burn Time: " + sBurnTime AT (0, 3).
	PRINT "Altitude: " + ALT:RADAR AT(0,6).
	SET t TO MAX(0, MIN(t, 1)).
	SET t TO ((SHIP:MASS * gravityAcceleration) / SHIP:AVAILABLETHRUST) + ((-SHIP:VERTICALSPEED - targetSpeed) / 5).
}

SET THROTTLE TO 0.
WAIT 5.
CLEARSCREEN.
HUDTEXT ("Vehicle saved", 20, 3, 20, rgb(1,1,0.5), true).

