//Simple boot Script to start the vessel_gui.
runoncepath("0:vessel_info_lib.ks").

if vesselInfo["VesselClass"] = "Unknown" {
    enterClass().
}

runoncepath("0:vessel_gui.ks").

local function enterClass {
    LOCAL gui IS GUI(300).
    LOCAL label is gui:ADDLABEL("Vessel Class:").
    SET label:STYLE:FONTSIZE TO 15.
    SET label:STYLE:ALIGN TO "CENTER".
    SET label:STYLE:HSTRETCH TO True.
    set label:tooltip to "Vessel Class".
    LOCAL textField TO gui:ADDTEXTFIELD().
    LOCAL done TO gui:ADDBUTTON("Done").
    SET done:ONCLICK TO enteredClass@.
    local isDone to False.
    function enteredClass {
        SET isDone TO TRUE.
        set vesselInfo["VesselClass"] to textfield:text.
        set vesselInfo["Name"] to vesselInfo["Name"]:replace("Unknown", vesselInfo["VesselClass"]).
        saveVesselInfo().
    }

    gui:SHOW().

    WAIT UNTIL isDone.
    gui:HIDE().
}