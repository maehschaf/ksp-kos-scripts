//A lib for Vessel Information and Mission Management -- by maehschaf
PARAMETER vesselClass IS "Unknown", vesselName IS "Unnamed Vessel".
WAIT 0.01.

//Helper variables
LOCAL codeExecutionId TO 0.

local parameterStart to "$%".
local parameterEnd to "%$".
local parameterDefault to "$&".

//Starship Information File
GLOBAL vesselInfo TO LEXICON("Name", vesselName, "VesselClass", vesselClass, "Construction", TIME:SECONDS, "FirstLaunch", -1, "MissionLog", LIST(), "ActiveMission", "None", "PayloadLog", LIST(), "LocationLog", LIST(BODY:NAME)).

GLOBAL checkForActiveMissions to True.

loadVesselInfo().
checkVesselNaming().
updateVesselInfo().
saveVesselInfo().
startNameChecker().


//MissionTest
//LOCAL m TO loadmission("DockingTest").
//set m["Parameters"]["text"] to "Parameters actually work amazingly!!!!!!".
//runMission(m).

//Mission System -- Use %$parameter name$% to add mission parameters into your code. You can also set default values: $%parameter name$&default%$

function makeMission {
	PARAMETER name, vesselClass IS "Unknown", parameters IS findparameters(codeList), codeList IS LIST("PRINT \'This is Mission " + name + ", hi!\'.", "WAIT 10.").
	return LEXICON("Name", name, "VesselClass", vesselClass, "Description", "A Mission", "StartTime", -1, "EndTime", -1, "CurrentCodeIndex", 0, "CodeList", codeList, "Parameters", parameters).
}

//Thinking about removing this and moving this to code - the only useful thing it does is saving the mission... The editing part isnt even working
function createMission {
	PARAMETER name, vesselClass IS "Unknown".
	LOCAL mission TO makeMission(name, vesselClass).
	saveMission(mission).
	EDIT ("0:/Missions/" + mission["Name"] + ".mission").
	findParameters(mission["CodeList"], mission["Parameters"]).
	return mission.
}

//Starts a loop constantly checking for an active mission and runs it if present
function startCheckForActiveMissionLoop {
	set checkForActiveMissions to true.
	UNTIL not checkForActiveMissions {
		checkForActiveMission().
	}
}

//Set the given mission to the active mission -- will actually run the mission if a checkForActiveMission loop is running
function setActiveMission {
	parameter mission.
	set vesselInfo["ActiveMission"] to mission.
	saveVesselInfo().
}

//Cancels the currently active mission
function cancelActiveMission {
	local m to vesselInfo["ActiveMission"].
	PRINT "Canceled Mission " + m["Name"].
	SET m["EndTime"] TO TIME:SECONDS.
	SET vesselInfo["ActiveMission"] TO "None".
	vesselInfo["MissionLog"]:ADD(m).
	unlock steering.
	unlock throttle.
	saveVesselInfo().
}

//Runs the given mission, if the second parameter is true it will let the current active mission finish first.
function runMission {
	PARAMETER m, finishPrevious IS TRUE.
	IF m:TYPENAME = "String" {
		SET m TO loadMission(m).
	}
	IF finishPrevious AND vesselInfo["ActiveMission"] <> "None" {
		runMission(vesselInfo["ActiveMission"], FALSE).
	}

	IF m["VesselClass"] <> vesselInfo["VesselClass"] {
		PRINT "WARNING: Mission " + m["Name"] + " is not made for Vessel class " + vesselInfo["vesselClass"].
	}

	set vesselInfo["ActiveMission"] to m.

	IF m["StartTime"] = -1 {
		SET m["StartTime"] TO TIME:SECONDS.
		PRINT "Starting Mission " + m["Name"].
		saveVesselInfo().
	} ELSE {
		IF m["StartTime"] > time:seconds {
			PRINT "Waiting for Mission Start" + TIME(m["StartTime"]):CALENDAR + " " + TIME(m["StartTime"]):CLOCK.
			WAIT UNTIL time:seconds >= m["StartTime"].
		}
		PRINT "Continuing Mission " + m["Name"].
	}
	UNTIL m["CurrentCodeIndex"] >= m["CodeList"]:LENGTH {

		local code to m["CodeList"][m["CurrentCodeIndex"]].
		set code to replaceParameters(code, m["Parameters"]). //Parameters
		
		executeCode(code). // Kinda important

		//print(m["CodeList"][m["CurrentCodeIndex"]]). //Dont print what we are doing ;)
		SET m["CurrentCodeIndex"] TO m["CurrentCodeIndex"] + 1.

		if DEFINED vesselGui {
			buildMainWindow().
		}
		saveVesselInfo().
	}
	PRINT "Finished Mission " + m["Name"].
	SET m["EndTime"] TO TIME:SECONDS.
	SET vesselInfo["ActiveMission"] TO "None".
	vesselInfo["MissionLog"]:ADD(m).
	unlock steering.
	unlock throttle.
	saveVesselInfo().
	if DEFINED vesselGui {
		buildMainWindow().
	}
}

function checkForActiveMission {
	IF vesselInfo["ActiveMission"] <> "None" {
		if checkParameters(vesselInfo["ActiveMission"]) {
			runMission(vesselInfo["ActiveMission"], FALSE).
		} else {
			HUDTEXT("Error: Not all parameters are set on the active mission, this prevented it from running!", 10, 2, 20, red, true).
		}
	}
}

function saveMission {
	PARAMETER mission, path is "0:/Missions/".
	set path TO path(path):combine(mission["Name"] + ".mission").
	if not exists(path) {
		create(path).
	}
	local f to open(path).
	f:CLEAR.

	f:writeln("Name: " + mission["Name"]).
	f:writeln("VesselClass: " + mission["VesselClass"]).
	f:writeln("Description: " + mission["Description"]).
	f:writeln("FormatVersion: " + "1.0"). //Version number
	//f:writeln("StartTime: " + mission["StartTime"]).

	f:writeln("").

	//Code
	f:writeln("Code:").
	for code in mission["CodeList"] {
		f:writeln(code).
	}
}

function loadMission {
	PARAMETER missionPath, parameterOverwrite is 0.

	if not exists(missionPath) {
		return false.
	}
	local mission to lexicon("Name", "Unnamed Vessel", "VesselClass", "Unknown", "Description", "A Mission", "StartTime", -1, "EndTime", -1, "CurrentCodeIndex", 0, "CodeList", List(), "Parameters", lexicon()).
	local fi to open(missionPath):READALL():ITERATOR.

	local codeMode to false.
	UNTIL NOT fi:NEXT {
		if fi:value:startswith("Code:") {
			set codeMode To true.
		} else {
			if not codeMode and fi:value:length > 0 {
				local dotIndex to fi:value:find(":").
				SET mission[fi:value:substring(0, dotIndex):trim] TO fi:value:substring(dotIndex + 1, fi:value:length - (dotIndex + 1)):trim. //ab: cd
			} else {
				mission["CodeList"]:ADD(fi:value).
			}
		}
	}
	findParameters(mission["CodeList"], mission["Parameters"]).
	//Print "Found Mission Parameters: " + mission["Parameters"].
	if parameteroverwrite <> 0 {
		for p in parameteroverwrite:keys {
			set mission["Parameters"][p] to parameteroverwrite[p].
		}
	}
	return mission.
}

//Executes the KerboScript code givan as parameter, escape " with \' -- internally copies it to MissionSkripts/mission_skript_0.ks and runs this file.
function executeCode {
	PARAMETER code.
	LOCAL path TO "MissionSkripts/mission_skript_" + codeExecutionId + ".ks".
	IF NOT CORE:VOLUME:EXISTS(path) {
		CORE:VOLUME:CREATE(path).
	}
	LOCAL f TO CORE:VOLUME:OPEN(path).
	f:CLEAR().
	f:WRITE(code:REPLACE("\'", char(34))).
	SET codeExecutionId TO codeExecutionId + 1.
	CD("0:/").
	runpath("1:/" + path).
	CORE:VOLUME:delete(path).
}

local function findParameters {
	parameter codeList, parameters is lexicon().
	parameters:clear().
	for lineOfCode in codeList {
		local cIndex to 0.
		Until cindex = -1 or cindex >= lineofcode:length {
			local startIndex to lineOfCode:findat(parameterStart, cIndex + 1).
			local endIndex to lineOfCode:findat(parameterEnd, cIndex + 1).
			if startIndex <> -1 and endIndex <> -1 {
				local defaultParameter to "".
				local paramIndex to lineOfCode:findat(parameterDefault, startIndex + parameterStart:length + 1).
				if paramIndex <> -1 and paramIndex < endindex {
					set defaultparameter to lineOfCode:substring(paramIndex + parameterStart:length, endindex - (paramIndex + parameterStart:length)).
					set endindex to paramIndex.
				}
				set parameters[lineOfCode:substring(startIndex + parameterStart:length, endindex - (startIndex + parameterStart:length))] to defaultParameter.
			}
			set cIndex to endIndex.
		}

	}
	return parameters.
}

local function replaceParameters {
	parameter code, parameters.
	local defaultParameters to findparameters(List(code)).
	for k in defaultParameters:keys {
		local parameterWildcard to parameterstart + k + parameterend. //$%name%$
		if defaultParameters[k] <> "" { //Create different wildcard using the default value.
			set parameterwildcard to parameterstart + k + parameterdefault + defaultParameters[k] + parameterend. //$%name$&default%$
		}
		set code to code:replace(parameterwildcard, parameters[k]).
	}
	return code.
}

function checkParameters {
	parameter mission.
	for k in mission["Parameters"]:keys {
		if mission["Parameters"][k] = "" {
			return false.
		}
	}
	return true.
}

function startVesselInfoUpdaters {
	//Body log
	ON SHIP:BODY:NAME {
		vesselInfo["LocationLog"]:ADD(BODY:NAME).
	}


}

//Info that should change dynamically or sth like that....
function updateVesselInfo {
	checkVesselNaming().
	IF SHIP:STATUS <> "prelaunch" and vesselInfo["FirstLaunch"] = -1 {
		SET vesselInfo["FirstLaunch"] TO TIME:SECONDS - MISSIONTIME.
	}

	IF vesselInfo["LocationLog"][vesselInfo["LocationLog"]:LENGTH - 1] <> BODY:NAME {
		vesselInfo["LocationLog"]:ADD(BODY:NAME).
	}
}

//Mission Save Vessel Switching - removes the current line of code when called from a mission.
function switchVessel {
	parameter vessel.
	if not vessel:isType("Vessel") {
		set vessel to Vessel(vessel).
	}
	if vesselinfo["ActiveMission"] <> "None" {
		set vesselinfo["ActiveMission"]["CodeList"][vesselinfo["ActiveMission"]["CurrentCodeIndex"]] to "Print \'Switch already done\'.".
	}
	savevesselinfo().
	KUniverse:forceactive(vessel).
}


//Messaging stuff
runoncepath("0:messaging_lib.ks").
addMessageHandler("mission-runMission", handleMissionRunMissionMessage@).

function sendRunMissionMessage {
	parameter destination, mission.
	sendMessage(destination, mission, "mission-runMission").
}

local function handleMissionRunMissionMessage {
	parameter message.
	setactivemission(message["Content"]).
}

//Naming stuff
local function startNameChecker {
	ON SHIP:SHIPNAME {
		checkVesselNaming().
		return true.
	}
}

function nameVessel {
	PRINT "Asking for name...".
	LOCAL gui IS GUI(300).
	LOCAL label is gui:ADDLABEL("Enter a unique name for this Vessel:").
	SET label:STYLE:FONTSIZE TO 15.
	SET label:STYLE:ALIGN TO "CENTER".
	SET label:STYLE:HSTRETCH TO True.
	set label:tooltip to "Name".
	LOCAL nameField TO gui:ADDTEXTFIELD().
	LOCAL done TO gui:ADDBUTTON("Done").
	LOCAL isDone IS FALSE.
	function doneChecker {
	  SET isDone TO TRUE.
	}
	SET done:ONCLICK TO doneChecker@.
	gui:SHOW().
	SET gui:Y TO gui:Y - 100.
	WAIT UNTIL isDone.
	PRINT "Done, naming Vessel " + vesselInfo["VesselClass"] + " " + nameField:TEXT.
	gui:HIDE().
	SET vesselInfo["Name"] TO vesselInfo["VesselClass"] + " " + nameField:TEXT.
	SET CORE:VOLUME:NAME TO vesselInfo["Name"].
	SET SHIP:SHIPNAME TO CORE:VOLUME:NAME.
}

local function checkVesselNaming {
	//PRINT "Updating vessel name..".
	IF vesselInfo["name"] = "Unnamed Vessel" {
		nameVessel().
	}
	IF SHIP:SHIPNAME <> vesselInfo["Name"] {
		IF countVessels() > 1 {
			SET SHIP:SHIPNAME TO countVessels() + " Docked Vessels".
		} ELSE {
			SET SHIP:SHIPNAME TO vesselInfo["Name"].
		}
	}
	SET CORE:VOLUME:NAME TO vesselInfo["Name"].
}

local function countVessels {
	LIST VOLUMES in vols.
	LOCAL cnt TO 0.
	FOR v IN vols {
		IF V:EXISTS("VesselInfo.txt") {
			SET cnt TO cnt + 1.
		}
	}
	RETURN cnt.
}

function saveVesselInfo {
	WRITEJSON(vesselInfo, getVesselInfoFileName()).
}

function loadVesselInfo {
	IF EXISTS( getVesselInfoFileName()) {
		SET vesselInfo TO READJSON(getVesselInfoFileName()).
	}
	return vesselInfo.
}

function getVesselInfoFileName {
	return "1:/VesselInfo.json".
}