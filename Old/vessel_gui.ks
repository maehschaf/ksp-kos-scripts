// Gui for Vessels to Start Missions -- by maehschaf
parameter startActiveMissionCheckLoop is true, startMessagingSystem is true. //Start checking for active missions when finished, causes the program to never end...
runoncepath("0:/vessel_info_lib.ks").

global vesselGui to Gui(500, 200).
set vesselGui:x to 100.
set vesselGui:y to 100.

buildMainWindow().

vesselGui:show().

//Main Vessel GUI Window

local lNameHeader to 0.

//First row in a potential future grid
local row1Box to 0.

//General Vessel Info
local infoBox to 0.

//Current mission info
local missionBox to 0.


function buildMainWindow {
    parameter g is vesselGui.
    g:clear.
    set g:style:hstretch to false.

    set lNameHeader to g:addlabel("<b>" + vesselInfo["Name"] + "</b>").
    set lNameHeader:style:fontsize to 19.
    set lNameHeader:style:textcolor to yellow.
    set lNameHeader:style:align to "center".

    set row1Box to g:addhbox().

    set infoBox to row1Box:addvlayout().

    //local lImage to g:addlabel("").
    //set lImage:image to "".

    //local lPVessel to infoBox:addLabel("<b>Vessel Info:</b>").
    //set lPVessel:style:fontsize to 16.
    //set lPVessel:style:textcolor to white.

    local lName to infoBox:addlabel("Vessel Name: <color=white>" + vesselInfo["Name"] + "</color>").
    set lName:style:fontsize to 14.
    set lName:style:align to "left".

    local lclass to infoBox:addlabel("Vessel Class: <color=white>" + vesselInfo["vesselClass"] + "</color>").
    set lclass:style:fontsize to 14.
    set lclass:style:align to "left".

    infoBox:addSpacing(14).

    local lFirstLaunch to infoBox:addlabel("First Launch: <color=white>" + TIME(vesselInfo["FirstLaunch"]):calendar + " " + TIME(vesselInfo["FirstLaunch"]):clock + "</color>").
    set lFirstLaunch:style:fontsize to 14.
    set lFirstLaunch:style:align to "left".

    infoBox:addSpacing().

    local bConsole to infoBox:addButton("Console").
    set bConsole:style:hstretch to false.
    set bConsole:onclick to toggleConsole@.
    set bConsole:tooltip to "Opens a command prompt. Warning: Wrong commands will cause the entire program to crash, if thats the case simply reboot.".

    set missionBox to row1Box:addvlayout().
    buildMissionPanel().
}

local function buildMissionPanel {
    parameter g is missionBox.
    g:clear().
    set g:style:width to 250.
    set g:style:height to 200.

    local mission to vesselInfo["ActiveMission"].

    //local lPMission to g:addLabel("<b>Current Mission:</b>").
    //set lPMission:style:fontsize to 16.
    //set lPMission:style:textcolor to white.

    if mission <> "None" {
        buildSingeMissionBox(g, mission).

        g:addSpacing().

        local bCancelMission to g:addButton("Cancel Mission").
        set bCancelMission:onclick to onCancelMission@.
    } else {
        local lNoMission to g:addLabel("<b>Currently no active mission</b>").
        set lNoMission:style:align to "center".
        set lNoMission:style:vstretch to true.

        local bSelectMission to g:addButton("Select Mission").
        set bSelectMission:style:align to "center".
        set bSelectMission:style:margin:h to 30.

        set bSelectMission:onclick to openMissionSelection@.

    }
}

local function onCancelMission {
    cancelActiveMission().
    buildMainWindow().
    vesselGui:hide().
    wait 0.01.
    reboot.
}

local function buildSingeMissionBox {
    parameter g, mission.

    local lName to g:addlabel("<b>Mission:  <color=white>" + mission["Name"] + "</color></b>").
    set lName:style:fontsize to 16.
    ///set lName:style:textcolor to green.

    local lVesselClass to g:addlabel("Made for Vessel class: <color=white>" + mission["vesselClass"] + "</color>").
    set lVesselClass:style:fontsize to 14.

    local lDescription to g:addlabel("Description: <color=white>" + mission["Description"] + "</color>").
    set lDescription:style:fontsize to 14.
    set ldescription:style:padding:v to 10.

    local lStartTime to g:addlabel("Time started: <color=white>" + Time(mission["StartTime"]):calendar + " " + Time(mission["StartTime"]):clock + "</color>").
    set lStartTime:style:fontsize to 14.

    if mission["EndTime"] <> -1 {
        local lEndTime to g:addlabel("Time ended: <color=white>" + Time(mission["EndTime"]):calendar + " " + Time(mission["EndTime"]):clock + "</color>").
        set lEndTime:style:fontsize to 14.
    }

    local lProgress to g:addlabel("Progress: <color=white>" + (mission["CurrentCodeIndex"] - 1) + "/" + (mission["CodeList"]:length - 1) + " Tasks completed</color>").
    set lProgress:style:fontsize to 14.
}


local function openMissionSelection {
    buildFileBrowser(missionSelectGui, selectActiveMission@, "0:Missions/", "<b>Select a Mission: </b>").
    //set missionSelectGui:Y to vesselGui:Y - 100.
    //set missionSelectGui:X to vesselGui:X + 50.
    missionSelectGui:show().
}

local function selectActiveMission {
    parameter path.
    if path:endswith(".mission") and exists(path) and path <> "" {
        //Open Mission window
        buildMissionLaunchGui(path).
        return true.
    } else {
        HUDTEXT("Error: The selection is not a Mission File!", 10, 2, 20, yellow, true).
        return false.
    }
}

//Mission Launch Gui
local missionLaunchGui to GUI(300, 600).

local function buildMissionLaunchGui {
    parameter missionPath, g is missionLaunchGui.
    g:clear().

    local mission to loadMission(missionPath).

    local lHeader to g:addlabel("<b>Mission " + mission["Name"] + "</b>").
    set lHeader:style:align to "center".
    set lHeader:style:textcolor to yellow.
    set lHeader:style:fontsize to 18.

    local lName to g:addLabel("Name: <color=white>" + mission["Name"] + "</color>").
    set lName:style:fontsize to 14.

    local lVesselClass to g:addlabel("Made for Vessel class: <color=white>" + mission["vesselClass"] + "</color>").
    set lName:style:fontsize to 14.

    local lDescription to g:addLabel("Description: <color=white>" + mission["Description"] + "</color>").
    set lDescription:style:fontsize to 14.

    local lTasks to g:addLabel("Mission Tasks: <color=white>" + (mission["CodeList"]:length - 1) + "</color>").
    set lTasks:style:fontsize to 14.

    if mission["Parameters"]:length > 0 {
        local lParameters to g:addLabel("Parameters:").
        local scrollBox to g:addscrollbox.
        scrollBox:addSpacing(14).

        local function onParameterChange {
            parameter p, pValue.
            set mission["Parameters"][p] to pValue.
        }

        for p in mission["Parameters"]:keys {
            local listLineLayout to scrollBox:addhlayout().

            local lParamName to listLineLayout:addLabel("<b>" + p + ": </b>").
            set lParamName:style:textcolor to white.
            set lParamName:style:hstretch to true.

            local tfParamValue to listLineLayout:addTextField(mission["Parameters"][p]).
            set tfParamValue:style:width to 150.
            set tfParamValue:onchange to onParameterChange@:bind(p).
        }
    }

    local bottomBar to g:addhlayout().

    local bCancel to bottomBar:addButton("Cancel").
    set bCancel:onclick to {g:hide().}.

    local bConfirm to bottomBar:addButton("Start Mission").
    set bConfirm:onclick to onStartMission@:bind(g, mission).
    
    g:gui:show().
}

local function onStartMission {
    parameter g, mission.
    if checkParameters(mission) {
        g:gui:hide().
        setActiveMission(mission).
        buildMainWindow().
    } else {
        HUDTEXT("Error: Failed to Start - Not all parameters are set!", 10, 2, 20, yellow, true).
    }
}

//Console --
local consoleGui to GUI(300,10).
buildConsoleGui().

local function buildConsoleGui {
    local lConsole to consolegui:addLabel("<b>Console:</b>").
    set lconsole:style:textcolor to yellow.
    set lconsole:style:padding:top to 0.

    local commandLine to consoleGui:addTextField("").
    set commandline:style:fontsize to 18.
    set commandline:onconfirm to {
        parameter code.
        executecode(code).
        set commandLine:text to "".
    }.
    set consolegui:visible to false.
}

local function toggleConsole {
    if consolegui:visible {
        consolegui:hide().
	    CORE:PART:GETMODULE("kOSProcessor"):DOEVENT("Close Terminal").
    } else {
        consolegui:show().
        //clearscreen.
        CORE:PART:GETMODULE("kOSProcessor"):DOEVENT("Open Terminal").
    }
}

//Mission Selection FileBrowser
local missionSelectGui to GUI(400, 400).


//File Browser!!
local function buildFileBrowser {
    parameter g, action, directory is "0:", title is "<b>Select a File: </b>".
    g:clear().
    
    local lHeader to g:addlabel(title).
    set lHeader:style:align to "center".
    set lHeader:style:fontsize to 15.


    local fileListBox to g:addscrollbox().

    CD(directory).
    list files in fileList.

    if not path():length = 0 {
        local bMission to fileListBox:addButton("  << One directory up").
        set bMission:onclick to {buildFileBrowser(g, action, "..", title).}.
        set bMission:style:align to "left".
    }

    for f in fileList {
        local bMission to fileListBox:addButton(f:name).
        set bMission:onclick to onFileSelect@:bind(f).
        set bMission:toggle to true.
        set bMission:exclusive to true.
    }

    local function onFileSelect {
        Parameter file.
        if file:isfile {
            for w in g:widgets {
                if w:istype("TextField") {
                    set w:text to "" + path():combine(file:name).
                }
            }
        } else {
            buildFileBrowser(g, action, file:name, title).
        }
    }

    local tfPath to g:addTextField("" + path()).
    set tfPath:onchange to { 
        parameter path.
        if not path:endswith(".") and exists(path) and path <> "" {    
            buildFileBrowser(g, action, path, title).
        }
    }.

    local bottomBar to g:addhlayout().

    local bCancel to bottomBar:addButton("Cancel").
    set bCancel:onclick to {g:hide().}.

    local bOkay to bottomBar:addButton("Okay").
    set bOkay:onclick to {
        action:call(tfPath:text).
        g:hide().
    }.
}

//Should continue mission popup
local Function shouldContinueMissionGui {
    local continueMissionGui to GUI(400, 200).

    local lHeader to continueMissionGui:addlabel("<b>Continue Mission?</b>").
    set lHeader:style:align to "center".
    set lHeader:style:textcolor to yellow.
    set lHeader:style:fontsize to 18.

    local infoText to continueMissionGui:addLabel("The active Mission '" + vesselInfo["ActiveMission"]["Name"] + "' is not finished, should it be continued?").

    continueMissionGui:addSpacing(8).

    local autoContinueCountdown to 5.

    local autoContinueText to continueMissionGui:addLabel("").
    set autoContinueText:style:hstretch to true.

    local bottomBar to continueMissionGui:addhlayout().
    
    local bCancel to bottomBar:addbutton("Cancel").
    set bCancel:style:textColor to red.
    set bCancel:onclick to {continueMissionGui:hide(). onCancelMission().}.

    local bContinue to bottomBar:addbutton("Continue").
    set bContinue:style:textcolor to green.
    set bContinue:onclick to {set autoContinueCountdown to -1.}.

    continueMissionGui:show().

    until autoContinueCountdown < 0 or not continueMissionGui:visible {
        set autoContinueText:text to "<b>The mission will automaticly continue in <color=red>" + autoContinueCountdown + "</color> seconds...</b>".
        wait 1.
        set autoContinueCountdown to autoContinueCountdown - 1.
    }
    continueMissionGui:hide().
}


//Show/Hide Button in the lower right corner
local showHideGui to GUI(70).
local bShowHide to showHideGui:addButton("").
buildShowHideGui().

local function buildShowHideGui {
    parameter g is showHideGui.
    set g:draggable to false.
    set g:x to -53.
    set g:y to -35.
    set g:style:bg to "".

    set bShowHide:style:align to "center".
    set bShowHide:tooltip to "Show or hide the vessels Gui".
    showHideGui:show().
    showVesselGui().
}

local function hideVesselGui {
    set bShowHide:onclick to showVesselGui@.
    set bShowHide:text to "Show Vessel Gui".
    vesselGui:hide().
    missionSelectGui:hide().
}

local function showVesselGui {
    set bShowHide:onclick to hideVesselGui@.
    set bShowHide:text to "Hide Vessel Gui".
    buildMainWindow(). //refresh the gui
    vesselGui:show().
}


//Not really Gui related stuff that is important when this file is used as a boot script.

if startMessagingSystem {
    runoncepath("0:messaging_lib.ks").
    handleMessageQueue(SHIP:messages).
}

//Starts Active Mission check loop
if startActiveMissionCheckLoop {
    if vesselInfo["ActiveMission"] <> "None" {
        shouldContinueMissionGui().
    }
    startCheckForActiveMissionLoop().
}