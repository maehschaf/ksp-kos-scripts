runoncepath("0:atmospheric_steering_lib.ks").

//STAGE.
//openTerminal().
sas off.
wait 0.1.
clearscreen.

set config:IPU to max(800, config:IPU).
if false {
    local dist to 30000.

    LOCAL d TO ship.Loaddistance.
    SET d:LANDED:UNLOAD TO dist + 1000.
    SET d:PRELAUNCH:UNLOAD TO dist + 1000.
    SET d:FLYING:UNLOAD TO dist + 1000.
    SET d:SPLASHED:UNLOAD TO dist + 1000.
    WAIT 0.1.
    SET d:LANDED:PACK TO dist.
    SET d:PRELAUNCH:PACK TO dist.
    SET d:FLYING:PACK TO dist.
    SET d:SPLASHED:PACK TO dist.
    WAIT 0.1.
}


on ag10 {
    unlock steering.
    SET SHIP:CONTROL:NEUTRALIZE to True.
    unlock throttle.
    unlock WheelSteering.
    Preserve.
}

setupWarningSounds().

if ship:status = "Landed" or ship:status = "PreLaunch" {
    //taxiTo(body:geopositionof(getWaypointPos(waypoints["KSCRunwayWestEnd"]) - north:vector * 30), 10).
    taxiTo(waypoints["KSCRunwayWestEnd"], 10).
    takeoff(waypoints["KSCRunwayWestEnd"], waypoints["KSCRunwayEastEnd"], 50).
}
LOCK THROTTLE TO planeThrottlePID:UPDATE(time:seconds, airspeed).
LOCK STEERING TO steerTowardsGeoPos(waypoints["IslandRunwayWestEnd"]["GeoPos"], 3000, airspeed * 0.2).
set planeThrottlePID:setpoint to 1500.
ag1 off.

//
//local bezierList1 to List(ship:position, ship:position + ship:facing:forevector * 1000, Vessel("Nautilus I"):position + UP:forevector * 500 + NORTH:forevector * 8000 - NORTH:starvector * 2000, Vessel("Nautilus I"):position + UP:forevector * 500).


//if ship:GeoPosition:lng < waypoints["KSCRunwayEastEnd"]["GeoPos"]:lng {
//    lock steering to steerTowardsGeoPos(Vessel("Nautilus I"):GeoPosition, 3000).
//    wait until Vessel("Nautilus I"):GeoPosition:altitudeposition(altitude):mag < 500.
//}

wait until isTimeToAccelerate(waypoints["IslandRunwayWestEnd"]["GeoPos"]:position:mag, aircraftInfo:approachSpeed).

land(waypoints["IslandRunwayWestEnd"], waypoints["IslandRunwayEastEnd"], 30, 7000, 0.25, 20, 3000).

taxiTo(waypoints["IslandRunwayEastEnd"], 10).
takeoff(waypoints["IslandRunwayEastEnd"], waypoints["IslandRunwayWestEnd"], 50).

//Travel to runway
set planeThrottlePID:setpoint to 1500.
LOCK STEERING TO steerTowardsGeoPos(waypoints["KSCRunwayEastEnd"]["GeoPos"], 3000, airspeed * 0.2).

wait until isTimeToAccelerate(waypoints["KSCRunwayEastEnd"]["GeoPos"]:position:mag, aircraftInfo:approachSpeed).

land(waypoints["KSCRunwayEastEnd"], waypoints["KSCRunwayWestEnd"], 30, 2000, 0.25, 20, 3000).

//local runwayMidPoint to getWaypointPos(waypoints["KSCRunwayEastEnd"], 100) + 0.5 * (getWaypointPos(waypoints["KSCRunwayEastEnd"]) - getWaypointPos(waypoints["KSCRunwayWestEnd"], 50)).

//local firstCorrectionPoint to getControlPointForP(getWaypointPos(waypoints["KSCRunwayEastEnd"], 200) - NORTH:starvector * 800, ship:position + ship:facing:forevector * 1000, getWaypointPos(waypoints["KSCRunwayWestEnd"]), 0.2).
////local bezierList2 to List(ship:position, ship:position + ship:facing:forevector * 1000, firstcorrectionpoint, getWaypointPos(waypoints["KSCRunwayEastEnd"]), runwayMidPoint, getWaypointPos(waypoints["KSCRunwayWestEnd"], 30)).
//
//startfollowcurve(bezierlist2, 50, 25, 20, true).
//lock steering to basicAirplaneSteering(0,0).
wait 3.
reboot.


Print "Done!".
unlock steering.
SET SHIP:CONTROL:NEUTRALIZE to True.