//Runs a loop that LoopingFunctions can be added to so you dont have to start new loops all the time - by maehschaf

//How to use:
//  Basically just add LoopingFunctions to the main loop using addToMainloop(LoopingFunction("name", {print "Your code goes here."}))
//  Then use startMainloop() to start looping the Looping functions every physics tick.
//      Note that startMainloop() is a blocking function if the "threaded" parameter is not set to true. So you have to stop
//  You can still add and remove LoopingFunctions while the mainloop is running
//  Use stopMainloop() to stop the loop again.
RunOncePath("0:/basic_lib.ks").

local loadTime to time:seconds.

local mainloop to list().
local running to False.

global function LoopingFunction {
    //If only one function is given onStart will be used as onLoop and no functions will be run at start and end
    //You may set onStart and onEnd to "None" if you dont want to use them
    //If allow Rerun is False the looping function cant be added to the mainloop again after it has ended once
    Parameter name, onStart, onLoop is "None", onEnd is "None", allowRerun is True.
    if onLoop = "None" {
        set onLoop to onStart.
        set onStart to "None".
    }

    local lf to Lexicon().
    set lf:name to name.
    set lf:onStart to onStart.
    set lf:onLoop to onLoop.
    set lf:onEnd to onEnd.

    set lf:loopStartTime to -1.
    set lf:loopEndTime to -1.
    set lf:loopCount to 0.

    set lf:allowRerun to allowRerun.

    set lf:isRunning to {
        return lf:loopStartTime <> -1 and lf:loopEndTime = -1.
    }.

    return lf.
} 

global function addToMainloop {
    //Adds this LoopingFunction to the mainloop and calls onStart
    Parameter lf.
    if getMainloopFunctionByName(lf:name) = "None" and (lf:loopStartTime = -1 or lf:allowRerun) {//Check if name already exists and the function has either never  started before or reruns are allowed
        mainloop:add(lf).
        set lf:loopStartTime to -1.
        return True. //Success!
    } else {
        return False. //Failed to add
    }
}

global function removeFromMainLoop {
    //Removes the LoopingFunction with the given name from the mainloop
    Parameter name.

    from {local i to 0.} until i >= mainloop:length() step {set i to i + 1.} do {
        local lf to mainloop[i].
        if lf:name = name {
            mainloop:remove(i).
            set lf:loopEndTime to time:seconds.
            if not (lf:onEnd = "None") {
                lf:onEnd:call().
            }
            return True.
        }
    }
    return False. //Function with that name not in mainloop
}

global function clearMainloop {
    Parameter callOnEnd to True.
    for lf in mainloop {
        set lf:loopEndTime to time:seconds.
        if not (lf:onEnd = "None") and callOnEnd {
            lf:onEnd:call().
        }
    }
    mainloop:clear().
}

global function runMainloopOnce {
    Parameter callOnStart to True.
    local cp to mainloop:copy().
    for lf in cp { //TODO: Make sure the copy() is not a memory leak!
        //Call onStart if this is the first run
        if callOnStart and lf:loopStartTime = -1 {
            set lf:loopStartTime to time:seconds.
            set lf:loopEndTime to -1.
            set lf:loopCount to 0.
            if not (lf:onStart = "None") {
                lf:onStart:call().
            }
        }

        //Call onLoop
        lf:onLoop:call().
        set lf:loopCount to lf:loopCount + 1.
    }
}

global function startMainloop {
    //Starts the mainloop.
    //If "threaded" is False it is a blocking function until startMainloop() is called, otherwise the loop will run in a trigger
    //Returns false if the loop is already running
    //TODO: Maybe implement a variable update frequency?
    Parameter threaded to False.
    if running {
        return False. //Prevent running two loops at the same time
    }
    set running to True.
    if threaded {   //Run as trigger - can ruin performance I guess
        when True then {
            runMainloopOnce(True).
            return running.
        }
    } else {        //Run in main program, therefore is a blocking function
        until not running {
            runMainloopOnce(True).
            wait 0.
        }
    }
    return True.
}

global function stopMainloop {
    //Stopps the mainloop. You can keep its current state by setting callClearMainloop to False.
    Parameter callClearMainloop to True.
    set running to False.
    if callClearMainloop {
        clearMainloop().
    } else {
        for lf in mainloop {
            set lf:loopEndTime to time:seconds.
            if not (lf:onEnd = "None") {
                lf:onEnd:call().
            }
        }
    }
}

global function getMainloopFunctionByName {
    Parameter name.
    for f in mainloop {
        if f:name = name {
            return f.
        }
    }
    return "None".
}

global function getMainloop {
    return mainloop.
}

global Function isMainloopRunning {
    return running.
}

global function addLoopingFunctionToMainloop {
    //Simplyfiy things
    Parameter name, onStart, onLoop is "None", onEnd is "None", allowRerun is True.
    local lf to LoopingFunction(name, onStart, onLoop, onEnd, allowRerun).
    if addToMainloop(lf) {
        return lf.
    } else {
        return False.
    }
}

printLog("Loaded " + ScriptPath():name + " in " + round(time:seconds - loadTime, 2) + "s").