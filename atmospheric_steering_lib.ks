//Lib to simplify steering planes (or plane like vessels) in atmosphere
runoncepath("0:basic_lib.ks").

local loadTime to time:seconds.

local rollPID to pidloop(0.019, 0, 0.01). //Rewrite with pitch roll and (maybe) yaw rates.. TODO!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!... prob use the steering_lib.ks stuff
local pitchPID to pidloop(0.18, 0.105, 0.15).
local yawPID to pidloop(0.06, 0, 0.04).

local headingRollPID to pidloop(2.7, 0, 0.2).
local verticalSpeedPID to pidloop(1.1, 0, 0.02).

local altitudePID to pidloop(0.18, 0, 0.02).

global planeThrottlePID to pidloop(0.08, 0, 0.01, 0, 1).

on abort {
    Print "Autopilot Disengaged, shutting down".
    shutdown.
}


//Prob gonna remove this, just use variables I guess
//Info about the aircraft. Keys: StallSpeed, StallSpeedFlaps, TakeoffSpeed, LandingSpeed, Flaps, SpeedBrakes, LiftDumpers, ReverseThrust
global aircraftInfo to Lexicon(
    "minControlQ", 0.1, //Important!
    "defaultBankAngle", 30, "maxBankAngle", 60, "defaultSinkRate", 0.25, "maxPitch", 20, "dragDeceleration", -2, "stallSpeed", 62, "stallSpeedFlaps", 50, "takeOffSpeed", 60, "landingSpeed", 100, "approachSpeed", 160, "maxGroundPitch", 4.5, "defaultTaxiSpeed", 8, 
    "flaps", { parameter state is "none". if state = "full" or state = "landing" { ag1 on. } else { ag1 off.}},  //Other states are full, landing, takeoff
    "SpeedBrakes", { parameter amount. if amount > 0 ag4 on. else ag4 off. },//TODO still quick and dirty code
    "LiftDumpers", { parameter state is true. if state ag3 on. else ag3 off. },
    "ReverseThrust", { parameter state is true. if state ag2 on. else ag2 off. },
    "hasReverseThrust", false, 
    "useReverseThrustAndBrakes", false
    ).

//orientation moved to basic_lib.ks

//Turns
function getTurnRadius {
    parameter bankAngle is getRoll(), speed is Airspeed.
    return speed^2 / (getGravityAcceleration() * tan(bankAngle)).
}

function getBankForTurnRadius {
    Parameter radius, speed is Airspeed.
    return ArcTan((speed^2 / radius) / getGravityAcceleration()).
}

//Altitude
local craftBounds to SHIP:bounds.

function updateCraftBounds {
    set craftBounds to ship:bounds.
}

function getExactRadarAltitude {
    return max(0, craftBounds:bottomaltradar).
}

function getExactAltitude {
    return craftBounds:bottomalt.
}

//MOVED INTO basicAirplaneSteering
//Try to get finer control


//local function airspeedControlModifier {
//    parameter minControlQ is aircraftInfo:minControlQ.
//    
//    local af to (1 / max(Ship:q, minControlQ)) * minControlQ. //Maybe split minControlQ into tuning Q and min Control Q....
//    println("Q: " + round(Ship:q, 5) + " Airspeed control mod: " + round(af, 4), 23).
//    return af.
//}

local PIDresetTime to 30. //steering PIDs will be reset every x seconds
local lastPIDreset to time:seconds. 

//Steering functions - use "lock steering to <functionYouWantToUse>.
function basicAirplaneSteering {
    parameter pitch, roll, addPitchWhileBanking is 0.

    local control to ship:control.

    //minControlQ is the minimum DynamicPressure (Q) needed to control the craft --> The PID values are tuned for this q
    local QControlModifier to (1 / max(Ship:q, aircraftInfo:minControlQ)) * aircraftInfo:minControlQ. //Maybe split minControlQ into tuning Q and min Control Q....

    set rollpid:setpoint to roll.
    set pitchpid:setpoint to pitch + abs(sin(roll)) * addPitchWhileBanking.
    set yawPID:setpoint to pitch.
    set control:pitch to (cos(roll) * pitchpid:update(time:seconds, getPitch()) * QControlModifier).
    set control:roll to (-rollpid:update(time:seconds, getRoll()) * QControlModifier).
    set control:yaw to (sin(roll) * yawPID:update(time:seconds, getPitch()) * QControlModifier). //TODO what is better in sin()? roll or getRoll() -- decide for pitch too (cos(roll))
    if abs(roll) < 1 {
        set control:yaw to 0.
    }

    //reset pids to prevent the ITerm from doing strange things
    if time:seconds - lastPIDreset > PIDresetTime {
        pitchpid:reset().
        yawpid:reset().
        rollpid:reset().
        set lastPIDreset to time:seconds.
    }
    println(round(control:yaw , 4), 22).
    return "kill".
}

function steerTowards {
    parameter targetHeading, targetPitch, maxBankAngle is aircraftInfo:defaultBankAngle, additionalPitchWhileBanking is 0.
    set headingrollpid:maxoutput to maxbankangle.
    set headingrollpid:minoutput to -maxbankangle.

    local deltaHeading to relativeAngleDelta(targetHeading, getHeading()).
    print "Heading - Current: " + round(getHeading(), 1) + " Delta: " + round(deltaheading, 1) + " Target: " + round(targetheading, 1) AT(0,10).
    set headingrollpid:setpoint to 0.
    return basicAirplaneSteering(targetPitch, headingrollpid:update(Time:seconds, deltaheading), additionalPitchWhileBanking).
}

function steerTowardsAlt {
    parameter targetHeading, targetAltitude, maxVerticalSpeed is 20, maxBankAngle is aircraftInfo:defaultBankAngle, maxPitchAngle is aircraftInfo:maxPitch, additionalPitchWhileBanking is 0.
    return steerTowards(targetHeading, getPitchForAltitude(targetAltitude, maxPitchAngle, maxVerticalSpeed), maxBankAngle, additionalPitchWhileBanking).
}

function steerTowardsGeoPos {
    parameter geopos, targetAltitude, maxVerticalSpeed is 20, maxBankAngle is aircraftInfo:defaultBankAngle, maxPitchAngle is aircraftInfo:maxPitch.
    return steerTowardsAlt(geopos:heading, targetaltitude, maxVerticalSpeed, maxbankangle, maxPitchAngle).
}

function entrySteering {
	//REDO!!!!
    parameter velocityVector, pitch, bank.

    local velDirection to LookDirUp(velocityVector, up:forevector).
    local forwardVec to (cos(pitch) * velocityVector:normalized) + sin(pitch) * velDirection:topvector.
    set forwardVec to cos(bank) * forwardVec + sin(bank) * velDirection:starvector.
    return LookDirUp(forwardVec, -velocityVector).
}

//Pitch utility

function getPitchForVerticalSpeed {
    parameter targetVerticalSpeed, maxPitchAngle is aircraftInfo:maxPitch.
    set verticalspeedpid:maxoutput to abs(maxPitchAngle).
    set verticalspeedpid:minoutput to -abs(maxPitchAngle).
    set verticalspeedpid:setpoint to targetVerticalSpeed.
    println("Vertical Speed Target: " + round(targetVerticalSpeed, 2) + " Vertical Speed: " + round(VerticalSpeed, 2), 20).
    println("Target Pitch: " + round(verticalSpeedPID:update(time:seconds, VerticalSpeed), 4) + " Current Pitch: " + round(getPitch(), 4), 21).
    return verticalSpeedPID:output.
}

function getVerticalSpeedForAltitude {
    parameter targetAltitude, maxClimbSpeed is 20, maxSinkSpeed is maxClimbSpeed.
    set altitudePID:maxoutput to abs(maxClimbSpeed).
    set altitudePID:minoutput to -abs(maxSinkSpeed).
    set altitudePID:setpoint to targetAltitude.
    println("Target Altitude: " + round(targetAltitude) + " Current Altitude: " + round(getExactAltitude()), 8).
    return altitudePID:update(time:seconds, getExactAltitude()). 
}

function getPitchForAltitude {
    parameter targetAltitude, maxPitchAngle is aircraftInfo:maxPitch, maxClimbSpeed is 40, maxSinkSpeed is maxClimbSpeed.
    return getPitchForVerticalSpeed(getVerticalSpeedForAltitude(targetAltitude, maxClimbSpeed, maxSinkSpeed), maxPitchAngle).
}

//Wheelsteering
local wheelSteeringPID to PidLoop(0.05, 0, 0.02, -1, 1).

function wheelSteerTowards {
    parameter hdg.

	if hdg:isType("Lexicon") {
		set hdg to hdg["GeoPos"].
	}

    if hdg:istype("GeoCoordinates") {
        set hdg to hdg:heading.
    }

    set wheelSteeringPID:setpoint to 0.

    local cHdg to getHeading().

    set ship:control:wheelsteer to wheelSteeringPID:update(time:seconds, relativeAngleDelta(hdg, cHdg)) * 10 / airspeed.

    return cHdg.
}


//Blocking flight sequences
//
function taxiTo {
    //Taxis to the given waypoint
    parameter geopos, taxiSpeed is aircraftInfo:defaultTaxiSpeed.

	if geopos:isType("Lexicon") {
		set geopos to geopos["GeoPos"].
	}

    sas off.
    brakes off.
    updateCraftBounds().
    planeThrottlePID:reset().

    set planeThrottlePID:setpoint to taxiSpeed.
    local directionalGroundspeed to groundspeed.
    if vectorangle(ship:facing:forevector, ship:velocity:surface) > 120 {
        set directionalgroundspeed to -directionalgroundspeed.
    }
    LOCK THROTTLE TO planeThrottlePID:UPDATE(time:seconds, directionalGroundspeed).
    LOCK WHEELSTEERING to wheelSteerTowards(geopos).
    local controlPart to ship:controlpart.
    local closest to geopos:position:MAG.
    UNTIL floor((geopos:position - controlPart:position):mag) > closest and (geopos:position - controlPart:position):MAG < (craftBounds:furthestcorner(geopos:position) - controlPart:position):mag {
        set directionalgroundspeed to groundspeed.
        set closest to (geopos:position - controlPart:position):MAG.
        if vectorangle(ship:facing:forevector, ship:velocity:surface) > 120 {
            set directionalgroundspeed to -directionalgroundspeed.
        }
        if directionalGroundspeed > taxispeed or (directionalgroundspeed < 0 and directionalgroundspeed < taxiSpeed) {
            brakes on.
        } else {
            brakes off.
        }
    }
    set planeThrottlePID:setpoint to 0.
    unlock wheelsteering.
    brakes on.
}

function takeOff {
    //Lets a plane take off
    parameter runwayStart, runwayEnd, finishedAtAltitude is 50.

	if runwayStart:isType("Lexicon") {
		set runwayStart to runwayStart["GeoPos"].
	}

	if runwayEnd:isType("Lexicon") {
		set runwayEnd to runwayEnd["GeoPos"].
	}

    local startAlt to Altitude.

    verticalSpeedPID:reset().
    altitudePID:reset().
    planeThrottlePID:reset().
    headingRollPID:reset().
    ClearScreen.
    sas off.
    brakes off.
    set ship:control:neutralize to true.

    local runwayDirection to (runwayend:position - runwaystart:position):normalized().
    taxito(runwayStart, 3).
    taxito(body:geopositionof(runwaystart:position + runwaydirection * 35), 3).
    LOCK WHEELSTEERING to wheelSteerTowards(runwayend).

    WHEN (getExactRadarAltitude()) > 15 or (getExactRadarAltitude()) >= finishedAtAltitude - 1 then {
        gear off.
        updateCraftBounds().
    }

    UNTIL abs(runwayend:bearing) < 0.5 {
        if groundspeed > 3 {
            brakes on.
        } else {
            brakes off.
        }
    }

    brakes off.
    LOCK THROTTLE to 1.
    aircraftInfo:flaps("takeoff").

    wait until groundspeed > aircraftInfo:takeOffSpeed.

    WHEN getExactRadarAltitude() > 0.1 then {
        LOCK STEERING to basicairplanesteering(min(startpitch + clamp((getExactRadarAltitude()) * 0.8, -10, aircraftInfo:maxPitch), getPitchForVerticalSpeed(airspeed * aircraftInfo:defaultSinkRate)), 0).
        Print "Taking off!".
    }
    local startPitch to getPitch().
    local cpitch to startPitch.

    Lock STEERING to basicairplanesteering(cpitch, 0).
    until cpitch > startPitch + aircraftInfo:maxGroundPitch {
        set cpitch to cpitch + 0.08.
        wait 0.05.
    }

    wait UNTIL Altitude > startAlt + finishedataltitude.

    updateCraftBounds().
    aircraftInfo:flaps("none").
    unlock Throttle.
    unlock wheelSteering.
    unlock steering.
    set ship:control:neutralize to true.
}

function land {
	//Lands a plane
    Parameter runwayStartPos, runwayEndPos, bankAngle is aircraftInfo:defaultBankAngle, approachPointDistance is 2000, maxSinkRate is aircraftInfo:defaultSinkRate, startAltitude is altitude, turnMode is "auto", maxPitchAngle is aircraftInfo:maxPitch, runwayAltitude is "Unknown". //TurnModes are left, right and auto

	if runwayEndPos:isType("Lexicon") {
		set runwayAltitude to runwayEndPos["Altitude"].
		set runwayEndPos to runwayEndPos["GeoPos"].
	}

	if runwayStartPos:isType("Lexicon") {
		set runwayAltitude to runwayStartPos["Altitude"].
		set runwayStartPos to runwayStartPos["GeoPos"].
	}

	if runwayAltitude = "Unknown" {
		set runwayAltitude to runwayStartPos:terrainheight.
	}

    ClearScreen.
    lock THROTTLE to planeThrottlePID:update(time:seconds, airspeed).
    
    pitchPID:reset().
    rollPID:reset().
    yawPID:reset().

    verticalSpeedPID:reset().
    altitudePID:reset().
    planeThrottlePID:reset().
    headingRollPID:reset().

    local landingDirection to (runwayEndPos:position - runwayStartPos:position):normalized.
    local approachPoint to body:geopositionof(runwayStartPos:position + (-landingDirection * approachPointDistance)).

    set startAltitude to max(altitude, startAltitude).
    local decentStartDist to 0.
    //local approachPointAltitude to runwayAltitude + (approachPointDistance / 10).
    local approachPointAltitude to runwayAltitude + (approachPointDistance / aircraftInfo:landingSpeed) * 5.
    local decelarationStartDist to 0.
    local overspeed to 0.

    local function pitchControl {
        parameter phase, distance.

        if decentStartDist = 0 {
            set decentStartDist to ((startAltitude - approachPointAltitude) / (aircraftInfo:approachSpeed * maxSinkRate)) * max((aircraftInfo:approachSpeed + planeThrottlePID:setpoint) / 2, aircraftInfo:approachSpeed) * 2.
        }

        local targetAlt to approachPointAltitude.

        println("Distance: " + distance + " decendStartDistance: " + decentStartDist,30).
        if abs(distance) > decentStartDist and phase > 1 { //Distance randomly jumps when turning!!!
            set targetAlt to startAltitude.
        } else {
            set targetAlt to approachPointAltitude + (distance / decentStartDist) * (startAltitude - approachPointAltitude).
        }

        local runwayStartDist to vectorRelativeDirection(runwayStartPos:position, ship:facing):x.

        if phase = 0 {
            if runwayStartDist > 0 {
                set targetAlt to runwayAltitude + 5 + max((runwayStartPos:position:mag / approachPointDistance) * (approachPointAltitude - runwayAltitude), 0).
            } else {
                if getExactAltitude() - targetalt > 200 { //Way too high to land
                    print "Too high! Go around".
                    reboot.
                }
                if abs(VDOT(runwayStartPos:altitudeposition(Altitude), landingDirection)) > 50 {
                    print "Not on runway! Go around".
                    reboot.
                }
                set targetAlt to runwayAltitude.
            }
        }

        //Make sure we are not too low
        if (targetAlt < runwayAltitude + 10) or (getExactAltitude() < runwayAltitude + 10) {
            if runwayStartDist > 0 {
                set targetAlt to max(runwayAltitude + 11, targetAlt).
                playNote(800, 0.1).
            }
        }

        //Airspeed control

        //Gradual slowdown while approaching...
        if phase = 2 {
            local distToApproachSpeed to approachPoint:altitudeposition(altitude):mag - (2 * getTurnRadius(bankAngle, aircraftInfo:approachSpeed)).
            if decelarationStartDist = 0 {
                set decelarationStartDist to distToApproachSpeed.
                set overspeed to airspeed - aircraftInfo:approachSpeed.
            }
            set planeThrottlePID:setpoint to max(aircraftInfo:approachSpeed + (distToApproachSpeed / decelarationStartDist) * overSpeed, aircraftInfo:approachSpeed).
            println(planeThrottlePID:setpoint, 25).
            println(throttle, 26).
        }

        //Final landing speed
        if abs(runwayStartPos:bearing) < 15 and phase = 1 {
            set planeThrottlePID:setpoint to aircraftInfo:landingSpeed.
        }

        //TODO Speedbrake, WIP/quick and dirty code
        if airspeed > planeThrottlePID:setpoint + 30 {
            aircraftInfo:speedBrakes(1).
        } else {
            aircraftInfo:speedBrakes(0).
        }

        println("Runwayalt: " + round(runwayAltitude) + " Dist to runway: " + round(runwayStartDist), 18).

        return getPitchForAltitude(targetAlt, maxPitchAngle, choose airspeed * maxSinkRate if getExactRadarAltitude() > 100 else (choose 10 if getExactRadarAltitude() > 50 else 5)).
    }
    
    //Flight sequence
    set planeThrottlePID:setpoint to aircraftInfo:approachSpeed.

    flyAlignmentCurve(approachPoint, landingDirection, bankAngle, pitchControl@, turnMode, false).
    
    local landingDir to LookDirUp(landingDirection, up:vector).

    local hod to HorizontalOffsetController(5, 10, true).
    local lock horizontalCorrection to hod:update(VDOT(runwayStartPos:altitudeposition(Altitude), landingDir:starvector), 0).

    verticalSpeedPID:reset().
    altitudePID:reset().
    headingRollPID:reset().

    lock steering to steerTowards(runwayEndPos:heading + horizontalCorrection, pitchControl(0, 0)).
    set planeThrottlePID:setpoint to aircraftInfo:landingSpeed.
    aircraftInfo:flaps("landing").
    lock WheelSteering to wheelSteerTowards(runwayEndPos).
    wait until getExactAltitude() < runwayAltitude + 100.
    gear on.
    wait 2.
    updateCraftBounds().
    wait until getExactRadarAltitude() < 50 and vectorRelativeDirection(runwayStartPos:position, ship:facing):x <= 0.

    print "Touchdown mode".
    lock steering to steerTowards(runwayEndPos:heading + horizontalCorrection, getPitchForVerticalSpeed(clamp(-getExactRadarAltitude() / 20, -3, 20)), 10).
    wait until getExactRadarAltitude() < 15.
    set planeThrottlePID:setpoint to 0.

    wait until ship:status = "landed" or ship:status = "splashed".

    local startPitch to getPitch().
    lock steering to basicAirplaneSteering(startPitch, 0).
    aircraftInfo:liftDumpers(true).
    aircraftInfo:flaps("none").
    brakes on.
    if aircraftInfo:hasReverseThrust {
        aircraftInfo:reverseThrust(true).
        lock THROTTLE to (1 - planeThrottlePID:update(time:seconds, airspeed)). //Reverse thrust control
    }

    until startpitch = 0 {
        set startPitch to clamp(startPitch - 0.1, 0, getPitch()).
        wait 0.05.
    }

    if (not aircraftInfo:useReverseThrustAndBrakes) and aircraftInfo:hasReverseThrust {
        brakes off.
    }
	unlock steering.
    set ship:control:neutralize to true.
    wait until groundspeed < 25.
    brakes on.
    wait until groundspeed < 15.
    lock THROTTLE to planeThrottlePID:update(time:seconds, airspeed).
    wait until groundspeed < 5.
    aircraftInfo:liftDumpers(false).
    aircraftInfo:reverseThrust(false).

    unlock WheelSteering.
}

Function flyAlignmentCurve {
    //Flys a curve that aligns the plane to be flying in the given direction at the given location
    Parameter targetGeoPos, targetDirection, bankAngle is 30, pitchCallback is { parameter phase, distance. return getPitchForVerticalSpeed(0). }, turnMode is "auto", debug is false. //Phases: 0 = Aligned, 1 = Final Turn, 2 = Flying to heading Alignment circle, 3 = Increasing distance to runway for lineup turns -- TurnModes: left, right, auto

    if targetDirection:istype("Vector") {
        set targetDirection to LookDirUp(targetDirection, up:vector).
    }

	if targetGeoPos:isType("Lexicon") {
		set targetGeoPos to targetGeoPos["GeoPos"].
	}

    //TurnMode
    local turnModeMultiplier to 1.
    if turnMode = "right" or turnMode = "auto" {
        set turnModeMultiplier to 1.
    } else if turnMode = "left" {
        set turnModeMultiplier to 1.
    } else {
        set turnMode to "auto".
    }

    //Auto turn mode
    local function checkAutoTurnMode {
        if turnMode = "auto" {
            if vectorRelativeDirection(ship:position - targetGeoPos:position, targetDirection):y < 0 { //Turn direction condition
                set turnModeMultiplier to -1.
            } else {
                set turnModeMultiplier to 1.
            }
        }
    }

    checkAutoTurnMode().

    local lock targetTurnRadius to getTurnRadius(bankAngle, aircraftInfo:approachSpeed). //use airspeed or approachSpeed?
    local lock midPoint to body:geopositionof(targetGeoPos:altitudeposition(Altitude) + targetDirection:starvector:normalized * turnModeMultiplier * targetTurnRadius).
    local lock alignmentPos to body:geopositionof(getCircleAlignmentPoint(midPoint:altitudeposition(Altitude), targetTurnRadius, targetDirection, turnModeMultiplier)).

    if debug {
        ClearVecDraws().
        print "Debug is enabled.".
        VecDrawArgs({return midPoint:altitudeposition(Altitude).}, {return alignmentPos:altitudeposition(Altitude) - midPoint:altitudeposition(Altitude).}, yellow, "", 1, true, 2, true, false).
        VecDrawArgs({return midPoint:altitudeposition(0).}, {return up:forevector * Altitude.}, green, "", 1, true, 5, true, false).
        VecDrawArgs({return alignmentPos:altitudeposition(0).}, {return up:forevector * Altitude.}, red, "", 1, true, 5, true, false).
    }

    //Phase 3: flying straight to get enough space to turn, Phase 2: Flying to hdg alignment circle, Phase 1: Flying hdg alignment circle, Phase 0: On final approach
    local function calcRemainingDistance {
        Parameter phase.

        local dist to 0.

        if phase > 0 { //add hdg alignment circle
            local angle to getRelativeHeading(targetDirection, ship:velocity:surface).
            if angle < 0 {
                set angle to 360 + angle.
            }
            if turnModeMultiplier = -1 {
                set angle to 360 - angle.
            }
            set dist to dist + (angle / 180) * Constant:pi * targetTurnRadius.
        }
        if phase > 2 { //add space to turn
            set dist to dist + (2 * getTurnRadius(bankAngle, max(airspeed, aircraftInfo:approachSpeed))) - alignmentPos:altitudeposition(Altitude):mag.
        } else if phase > 1 { //add distance to alignmentpos
            set dist to dist + alignmentPos:altitudeposition(Altitude):mag.
        }

        println("Estimated remaining distance: " + round(dist), 7).
        return dist.
    }

    //Make sure there is enough space to turn to the alignment position
    lock steering to basicAirplaneSteering(pitchCallback:call(3, calcRemainingDistance(3)), 0).

    print "Targeting first approach point.".
    print "Turn Mode: " + turnMode.
    //TODO: rework that condition! Dont forget to change calc Remaining Distance function!
    wait until midpoint:altitudeposition(Altitude):mag > 2 * getTurnRadius(bankAngle, max(airspeed, aircraftInfo:approachSpeed)).//(1 + abs(sin(relativeAngleDelta(alignmentPos:heading, getHeading())))) * targetTurnRadius. //Rework to consider both turn directions

    checkAutoTurnMode().
    print "Turn Direction: " + (choose "right" if turnModeMultiplier = 1 else "left").

    //Flying to hdg alignment circle
    lock Steering to steerTowards(alignmentPos:heading, pitchCallback:call(2, calcRemainingDistance(2)), bankAngle).

    print "Targeting final approach point.".
    wait until abs(vectorRelativeDirection(alignmentPos:altitudeposition(Altitude), ship:velocity:surface):x) < airspeed and alignmentPos:altitudeposition(Altitude):mag < targetTurnRadius.

    verticalSpeedPID:reset().
    altitudePID:reset().
    planeThrottlePID:reset().
    headingRollPID:reset().

    print "Flying final turn".

    local hoc to HorizontalOffsetController(10, 10, true).

    local lock correction to hoc:update(midPoint:altitudeposition(Altitude):mag, targetTurnRadius).
    lock steering to basicAirplaneSteering(pitchCallback:call(1, calcRemainingDistance(1)), -turnModeMultiplier * ((getBankForTurnRadius(targetTurnRadius) + correction))).

    wait until ((abs(vectorRelativeDirection(targetGeoPos:altitudeposition(Altitude), ship:velocity:surface):x) < airspeed and abs(getRelativeHeading(targetDirection)) < 45) and targetGeoPos:altitudePosition(Altitude):mag < 1000) or abs(getRelativeHeading(targetDirection)) < 1.

    if debug {
        ClearVecDraws().
    }
    print "Aligned".
    lock steering to steerTowards(getHeading(targetDirection), pitchCallback:call(0, calcRemainingDistance(0)), bankAngle).
}

//Internal Utility

local function getCircleAlignmentPoint {
    parameter circleMidpoint, circleRadius, targetDirection, turnModeMultiplier is 1.
    local angle to (getRelativeHeading(targetDirection, ship:velocity:surface)) * turnModeMultiplier - (180 * turnModeMultiplier).
    return circleMidpoint + (Sin(angle) * targetDirection:forevector + Cos(angle) * targetDirection:starvector * turnModeMultiplier) * circleRadius.
}

//Utility
function HorizontalOffsetController {
    parameter maxCorrectionBankAngle is 10, maxCorrectionSpeed is 10, debug is false.
    local this to Lexicon().
    
    set this:maxCorrectionBankAngle to maxCorrectionBankAngle.
    set this:maxCorrectionSpeed to maxCorrectionSpeed.
    set this:debug to debug.

    set this:setup to {
        set this:prevTime to time:seconds.
        set this:prevError to 0.
    }.

    set this:update to {
        parameter currentDistance, targetDistance.
        local error to currentDistance - targetDistance.
        local deltaTime to time:seconds - this:prevTime.
        local errorDelta to (error - this:prevError) / deltaTime.
        local targetErrorDelta to -clamp(error / 8, -10, 10).
        local correction to -clamp((targetErrorDelta - errorDelta) * 0.4, 10, -10).

        if debug {
            println("Horizontal Offset error: " + round(error, 1), 5).
            println("Error delta: " + round(errorDelta, 1) + " Target error delta: " + round(targetErrorDelta, 1), 4).
            println("Correction: " + correction, 12).
        }

        set this:prevError to error.
        set this:prevTime to time:seconds.

        return correction.
    }.
    
    this:setup().

    return this.
}

function getDistanceToAccelerate { //Distance to reach target speed with given acceleration
    parameter targetSpeed is 0, acceleration is 0.1, currentSpeed is airspeed.
    return ((currentSpeed - targetSpeed)^2) / (2*acceleration).
}

Function isTimeToAccelerate {
    Parameter distance, targetSpeed is 0, acceleration is -0.1, currentSpeed is Airspeed.
    return distance <= abs(getDistanceToAccelerate(targetSpeed, acceleration, currentSpeed)).
}

//High level functions

function travelToRunway {
    //Flys to and lands at the runway defined by the two waypoints
    //Runway start and end waypoints, travel Altitude, distance of the approach point from the runway start
    parameter runwayStart, runwayEnd, travelAltitude is 2000, travelSpeed is 300, approachPointDistance is 5000.

    sas off.
    lock THROTTLE to planeThrottlePID:update(time:seconds, airspeed).
    set planeThrottlePID:setpoint to aircraftInfo:approachSpeed.
    
    //reset pids
    verticalSpeedPID:reset().
    altitudePID:reset().
    planeThrottlePID:reset().
    headingRollPID:reset().

    //TODO get actual approach point -> replace runwayStart["GeoPos"] and change the code as nesseceary
    lock STEERING to steerTowardsGeoPos(runwayStart["GeoPos"], travelAltitude, airspeed * aircraftInfo:defaultSinkRate, aircraftInfo:defaultBankAngle, aircraftInfo:maxPitch).

    ClearScreen.

    local lock landingSectorRadius to 3.2 * getTurnRadius(aircraftInfo:defaultBankAngle, aircraftInfo:approachSpeed) + approachPointDistance.

    if runwayStart["GeoPos"]:altitudeposition(Altitude):mag >  landingSectorRadius { //Check if already close to destination
        wait until abs(runwayStart["GeoPos"]:bearing) < 5.
        set planeThrottlePID:setpoint to travelSpeed.
    }


    until (landingSectorRadius >= runwayStart["GeoPos"]:altitudeposition(Altitude):mag) or (runwayStart["GeoPos"]:altitudeposition(Altitude):mag <= ((travelAltitude - runwayStart["Altitude"]) / (aircraftInfo:approachSpeed * aircraftInfo:defaultSinkRate)) * aircraftInfo:approachSpeed * 2) { //See line 359
        if isTimeToAccelerate(runwayStart["GeoPos"]:altitudeposition(Altitude):mag - landingSectorRadius, aircraftInfo:approachSpeed, aircraftInfo:dragDeceleration, planeThrottlePID:setpoint) {
            set planeThrottlePID:setpoint to max(aircraftInfo:approachSpeed, planeThrottlePID:setpoint - 0.1).
        } else {
            set planeThrottlePID:setpoint to min(travelSpeed, planeThrottlePID:setpoint + 0.1).
        }
        println("Direct distance to target:" + round(runwayStart["GeoPos"]:altitudeposition(Altitude):mag) + "m", 1).
    }

    land(runwayStart, runwayEnd, aircraftInfo:defaultBankAngle, approachPointDistance, aircraftInfo:defaultSinkRate, travelAltitude, "auto", aircraftInfo:maxPitch).
}

function travelFromRunwayToRunway {
    parameter fromRunwayStart, fromRunwayEnd, toRunwayStart, toRunwayEnd, travelAltitude is 2000, travelSpeed is 300, approachPointDistance is 5000.
    if status = "landed" or status = "prelaunch" or status = "splashed" {
        taxiTo(fromRunwayStart).
        takeoff(fromRunwayStart, fromRunwayEnd, 50).
    }
    travelToRunway(toRunwayStart, toRunwayEnd, travelAltitude, travelSpeed, approachPointDistance).
}



//currently just for fun.. TODO redo with more accurate position predicion
function setupWarningSounds {
    parameter tts to True.
    if tts {
        RunOncePath("0:tts_lib.ks").
    }
    on round(time:seconds) { //Run every second
        if isStatusLanded() {
            return true.
        }
        if Airspeed < aircraftInfo:stallspeed and status = "flying" {//Stall
            GetVoice(1):play(SlideNote(150, 450, 1)).
            if tts {
                ttsLog("Stall!, Stall", 1, MessagePriority:ERROR).
            }
        }
        if VerticalSpeed < -(airspeed / 3) {
            GetVoice(2):play(SlideNote(400, 600, 1)).
            if tts {
                ttsLog("Sink Rate! Pull, Up!", 1, MessagePriority:ERROR).
            }
        }
        if (getExactRadarAltitude() < 100 or getExactAltitude() < 80) and not gear { //Too low
            GetVoice(2):play(SlideNote(400, 600, 1)).
            if tts {
                ttsLog("Too low! Terrain!", 1, MessagePriority:ERROR).
            }
        }
        if abs(getRoll()) > aircraftInfo:maxBankAngle {
            if tts {
                ttsLog("bank angle", 1, MessagePriority:ERROR).
            }
        }
        if (getExactRadarAltitude() + VerticalSpeed * 2 < 0 and not gear) { //Crash within 2 seconds - Feeeeep!
            GetVoice(4):play(Note(1200, 2, 1, 500)).
        }
        return true.
    }
    on round(time:seconds * 2) {
        if isStatusLanded() {
            return true.
        }
        if (getExactRadarAltitude() + VerticalSpeed * 10 < 0 and not gear) or status = "splashed" { //Crash within 10 seconds
            GetVoice(3):play(Note(100, 0.31, 0.3, 200)).
            if tts {
                ttsLog("Too low! Terrain!", 1, MessagePriority:ERROR).
            }
        }
        return true.
    }
}

printLog("Loaded " + ScriptPath():name + " in " + round(time:seconds - loadTime, 2) + "s").