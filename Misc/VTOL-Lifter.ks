@LazyGlobal off.
runoncepath ("0:basic_lib.ks").
WAIT 0.1.
openTerminal().
core:part:controlfrom().
GEAR OFF.
BRAKES ON.

//Parts
SET stageDock TO SHIP:PARTSTAGGED("stageDockSR")[0].
SET stageDockL TO SHIP:PARTSTAGGED("stageDockSL")[0].
SET lifterMainDock TO SHIP:PARTSTAGGED("lifterMainDock")[0].
SET jetEngines TO SHIP:PARTSNAMED("turboJet").

FOR e IN jetEngines {
	e:ACTIVATE().
	SET e:PRIMARYMODE TO False.
}

SET done TO false.
WAIT 0.001.

//Basic Control -- Change theading and target Velocity to move the vessel
LOCK gravityAcceleration TO body:mu / (altitude + body:radius)^2.
SET targetVelocity TO V(0, 0, 0). // x(Front/Top), y(Side/Starboard), z(Up/Forward) movement
SET theading TO 90.
SET maxAngle TO 3.
SET STEERINGMANAGER:PITCHPID:KD TO 0.65.
SET STEERINGMANAGER:ROLLPID:KD TO 0.1.
LOCK velTop to VDOT(SHIP:VELOCITY:SURFACE, ship:facing:forevector).
LOCK velStar to VDOT(SHIP:VELOCITY:SURFACE, ship:facing:starvector).
LOCK forwardAngle TO clamp((velTop - targetVelocity:X) * 1, -maxAngle, maxAngle).
LOCK sideAngle TO clamp((velStar - targetVelocity:Y) * 1, -maxAngle, maxAngle).
LOCK STEERING TO HEADING(theading, forwardAngle) + R(0, 0, sideAngle).
LOCK THROTTLE TO ((SHIP:MASS * gravityAcceleration) / SHIP:AVAILABLETHRUST) + (targetVelocity:Z - SHIP:VERTICALSPEED) / 10.


//Flight Path Control
SET TARGET TO VESSEL(BODY:NAME + " Starship Booster").
SET targetPart TO TARGET:PARTSTAGGED("stageDockBR")[0].
SET targetPartL TO TARGET:PARTSTAGGED("stageDockBL")[0].

LOCK targetVelocity TO V(10, 0, MIN(5, (VDOT(targetPart:NODEPOSITION, UP:FOREVECTOR) + VDOT(stageDock:NODEPOSITION, UP:FOREVECTOR) + 10) / 5)).
LOCK theading TO TARGET:HEADING.
LOCK ttopDist TO VDOT(targetPart:NODEPOSITION - stageDock:NODEPOSITION, ship:facing:forevector).
LOCK tsideDist TO VDOT(targetPart:NODEPOSITION - stageDock:NODEPOSITION, ship:facing:starvector).
LOCK theightDist TO VDOT(targetPart:NODEPOSITION - stageDock:NODEPOSITION, ship:facing:topvector).

WHEN ttopDist < 150 THEN {
	SET maxAngle TO 1.5.
	LOCK targetVelocity TO V(clamp(ttopDist / 2, -1, 1), clamp(tsideDist / 1.9, -1, 1), 0).
	LOCK theading TO VANG(targetPartL:NODEPOSITION - targetPart:NODEPOSITION, NORTH:FOREVECTOR) + 90.
	if theading > 180 OR theading < -180 {
		LOCK theading TO VANG(targetPartL:NODEPOSITION - targetPart:NODEPOSITION, NORTH:FOREVECTOR) + 270.
	}
	PRINT "Rotation Aligned to: " + (VANG(targetPart:NODEPOSITION - targetPartL:NODEPOSITION, NORTH:FOREVECTOR) - 90) AT(0,20).
	WHEN ABS(ttopDist) < 0.05 and ABS(tsideDist) < 0.06 and ABS(velTop) < 0.02 and ABS(velStar) < 0.02 THEN {
		PRINT "Starting Descent" AT(0,13).
		LOCK targetVelocity TO V(clamp(ttopDist / 2, -0.1, 0.1), clamp(tsideDist / 2, -0.1, 0.1), clamp(theightDist / 8, -0.5, -0.05)).
		SET maxAngle TO 1.
		RCS ON.
		WHEN not HASTARGET THEN {
			SET maxAngle TO 3.
			SET ttopDist TO 0.
			SET theightDist TO 0.
			SET theading TO theading.
			SET tsideDist TO 0.
			SET targetVelocity TO V(0, 0, -1).
			PRINT "Docked with First Stage" AT(0, 14).
			FOR e IN jetEngines {
				SET e:PRIMARYMODE TO True.
			}
			WAIT 2.
			lifterMainDock:UNDOCK().
			PRINT "Lifter undocked from Starship" AT(0, 15).
			SET targetVelocity TO V(10, 0, 0).
			WHEN velTop > 5 THEN {
				BRAKES OFF.
				SET targetVelocity TO V(0, 0, -3).
				WHEN SHIP:STATUS = "Landed" THEN {
					SET done To True.
				}
			}
		}
	}
}

//Outputs
CLEARSCREEN.
PRINT "Running".
UNTIL done {
	PRINT "Velocity: Current/Target" AT(0, 2).
	PRINT "Vertical: " + SHIP:VERTICALSPEED + " / " + targetVelocity:Z + "       " AT(0,3).
	PRINT "Forward: " + velTop + " / " + targetVelocity:X + "       " AT(0,4).
	PRINT "Sideways: " + velStar + " / " + targetVelocity:Y + "       " AT(0,5).
	PRINT "ForwardAngle: " + forwardAngle AT(0, 6).
	PRINT "SideAngle: " + sideAngle AT(0, 7).
	
	PRINT "Current Heading: " + theading  AT (0, 9).
	PRINT "Target Distance Top: " + ttopDist  AT (0, 10).
	PRINT "Target Distance Side: " + tsideDist  AT (0, 11).
	PRINT "Target Distance Height: " + theightDist  AT (0, 12).
	WAIT 0.01.
}

LOCK THROTTLE TO 0.
UNLOCK STEERING.
PRINT "Lifter Landed" AT(0, 16).
STEERINGMANAGER:RESETTODEFAULT().