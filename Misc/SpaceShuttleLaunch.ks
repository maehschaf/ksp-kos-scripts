//Spaceshuttle Launch

sas off.
CLEARSCREEN.
PRINT "Space Shuttle Launch".
WAIT 5.

SET targetalt TO 70000.


SET V0 TO GETVOICE(0).
SET SRBs TO SHIP:PARTSDUBBED("MassiveBooster").
SET SSMEs TO SHIP:PARTSDUBBED("SSME").
SET OMSpods TO SHIP:PARTSDUBBED("radialLiquidEngine1-2").

LOCK THROTTLE TO 0.8.
FROM {local countdown is 10.} UNTIL countdown = 0 STEP {SET countdown to countdown - 1.} DO {

	HUDTEXT ("T: -" + countdown, 1, 2, 150, rgb(1,1,1), false).
	IF countdown = 5 { 
		STAGE.
		HUDTEXT ("Main Engine Ignition", 10, 3, 20, rgb(1,1,0.5), true).
	} ELSE IF countdown = 4 {
		LOCK THROTTLE TO 0.9.
	} ELSE IF countdown = 3 {
		LOCK STEERING TO HEADING(180, 85) + R(0,0,180).
		HUDTEXT ("Engines gimbaled to liftoff configuration", 10, 3, 20, rgb(1,1,0.5), true).
	}
	V0:PLAY(NOTE(400, 0.1)).

	WAIT 1.

}
HUDTEXT ("SRB Ignition", 10, 3, 20, rgb(1,1,0.5), true).
V0:PLAY(NOTE(400, 0.3)).
LOCK STEERING TO HEADING(180, 83)  + R(0,0,180).
LOCK THROTTLE TO 1.
STAGE.
HUDTEXT ("Launchclamps released", 10, 3, 20, rgb(1,1,0.5), true).
HUDTEXT ("Liftoff", 3, 2, 150, rgb(1,1,1), false).

WHEN SRBs[0]:THRUST < 1 THEN {
	STAGE.
	HUDTEXT ("Booster Seperation", 10, 3, 20, rgb(1,1,0.5), true).

}

WAIT UNTIL ALTITUDE > 200.
HUDTEXT ("Starting Roll Program", 20, 3, 20, rgb(1,1,0.5), true).
SET roll TO 180.
UNTIL roll <= 90 {
	SET roll TO roll - (0.01 * 180) / 7.
	LOCK STEERING TO HEADING(roll, 85)  + R(0,0,180).
	WAIT 0.01.
}
HUDTEXT ("Roll Program Finished", 20, 3, 20, rgb(1,1,0.5), true).

SET roll TO 180.
UNTIL APOAPSIS > targetalt{
	SET pitch TO (targetalt-APOAPSIS)/targetalt * 85.
	LOCK STEERING TO HEADING(90, pitch) + R(0,0,roll).
	IF ALTITUDE > 35000 AND roll > 0 {
		SET roll TO roll - (0.01 * 180) / 20.
	}
	WAIT 0.01.
}
LOCK STEERING TO PROGRADE.
UNTIL PERIAPSIS > 55000 {
	LOCK THROTTLE TO MAX(MIN(((60 - ETA:APOAPSIS) / 60 + 0.5), 1), 0.1).
	WAIT 0.1.
}
FOR SSME in SSMEs {
	SSME:SHUTDOWN().
}
LOCK THROTTLE TO 0.
HUDTEXT ("Main Engine CutOff", 10, 3, 20, rgb(1,1,0.5), true).
WAIT (ETA:APOAPSIS / 2).
RCS ON.
STAGE.
HUDTEXT ("External Tank Seperation", 10, 3, 20, rgb(1,1,0.5), true).
SET SHIP:CONTROL:TOP TO 1.
WAIT 10.
SET SHIP:CONTROL:TOP TO 0.
WAIT UNTIL ETA:APOAPSIS <= 2.
FOR OMSpod in OMSpods {
	OMSpod:ACTIVATE().
}
LOCK THROTTLE TO 0.5.
WAIT UNTIL PERIAPSIS > targetalt or ALTITUDE = PERIAPSIS.
LOCK THROTTLE TO 0.
HUDTEXT ("Successfull orbit insertion", 20, 3, 20, rgb(1,1,0.5), true).
HUDTEXT ("Opening Cargo Bay Doors in 50 seconds", 5, 3, 20, rgb(1,1,0.5), true).

WAIT 50.
BAYS ON.
HUDTEXT ("Opening Cargo Bay", 20, 3, 20, rgb(1,1,0.5), true).
WAIT 5.
SHIP:PARTSDUBBED("HighGainAntenna5")[0]:GETMODULE("ModuleDeployableAntenna"):DOACTION("extend antenna", true).
LOCK STEERING TO SUN:POSITION + R(0,-90,0).
WAIT 10.
PRINT "Skript complete!".
V0:PLAY(NOTE(400, 0.3)).
sas on.
