//Helicopter
Parameter defaultBladeAngle is 10.0, maxCyclicAngleChangePitch is 5, maxCyclicAngleChangeRoll is 8, maxCollectiveAngle is 15, tailRotorDirection is -1. //-1 = tail rotor introduces clockwise spin / pulls to the left | 1 = tail rotor introduces counterclockwise spin / pulls to the right
runoncepath("0:basic_lib.ks").
runoncepath("0:atmospheric_steering_lib.ks").

wait 0.1.

openTerminal().

if ship:status = "prelaunch" { //Startup
    wait 1.
    stage.
    Brakes on.
    for fc in ship:partsdubbed("FuelCellArray") {
        fc:getModule("ModuleResourceConverter"):doaction("start fuel cell", true).
    }
}
if ship:status = "Landed" {
    Brakes on.
}

set config:ipu to 800.

//Parts
local bladeAngleServos to ship:partstagged("BladeAngle").
local rotor to ship:partstagged("MainRotor")[0].
local tailRotor to ship:partstagged("TailRotor").
local tailRotorServos to ship:partstagged("TailRotorServo").

local bladeData to Lexicon().
for bas in bladeAngleServos {
    set bladeData[bas:cid] to Lexicon("defaultoffset", getBladeAngleError(bas), "prevAngle", 0, "prevTargetAngle", 0).

}

ClearScreen.

println("Maehschafs Helicopter Control Script", 0).
println("Helicopter: " + shipname, 2).

function emulateSwashplate {
    parameter forePos, sidePos, pitchAng, rollAng.
    return forePos * -pitchAng + sidePos * rollAng.
}

local prevFacing to ship:facing.
local lastTime to time:seconds.

//x = yaw, y = pitch, z = roll | unit = sin(degrees / second)
function getAngularVelocity {
    local deltaTime to time:seconds - lastTime.
    local foreVec to ((ship:facing:forevector - prevFacing:forevector) * ship:facing:inverse).
    return V((foreVec:x), (foreVec:y), (((ship:facing:topvector - prevFacing:topvector) * ship:facing:inverse):x)) / deltaTime.
}

function getBladeAngle {
    parameter bladeServo.
    local function ang {
        return round(vang(bladeServo:children[0]:facing:topvector, ship:facing:topvector), 2).
    }
    if bladeData:haskey(bladeServo:cid) {
        return ang() - bladeData[bladeServo:cid]["defaultoffset"].
    } else {
        return ang().
    }
}

function getBladeAngleError {
    parameter bladeServo.
    return round(getBladeAngle(bladeServo) - getBladeTargetAngle(bladeServo), 2).
}

function getBladeTargetAngle {
    Parameter bladeServo.
    return bladeServo:getmodule("ModuleRoboticRotationServo"):getField("target angle").
}

function getRotorAngle {
    parameter blade.
    local bladePos to vectorRelativeDirection(vxcl(ship:facing:topvector, blade:position - rotor:position), facing):normalized.
    return ArcTan2(bladePos:y, bladePos:x).
}

ClearVecDraws().
//VecDraw({return rotor:position + ship:facing:topvector * 1.5.}, {return vxcl(ship:facing:topvector, bladeAngleServos[0]:position - rotor:position).}, red, "", 1, true, 0.1).
//vecdraw(V(0,0,0), {return getAngularVelocity() * 50 * Facing.}, green, "", 1, true, 0.5).

local prevHeading to getHeading().
local prevRotorAngle to getRotorAngle(bladeAngleServos[0]).

// PIDs
local pitchPid to PidLoop(2.2, 0, 0, -1, 1).
local rollPid to PidLoop(0.8, 0, 0, -1, 1).
local headingPid to PidLoop(10, 0, 0, -10, 18).

local correctionFactor to 1. //Important

//Target pitch/yaw/roll
local pitch to 0.
local roll to 0.
local yaw to 0.

lock steering to "kill".
lock throttle to ship:control:pilotmainthrottle * 1.1.

local collective to defaultBladeAngle.
local tailRotorBladeAngle to 5.

//Measure Servo delay
local testTarget to getBladeAngle(bladeAngleServos[0]) + 1.
bladeAngleServos[0]:getmodule("ModuleRoboticRotationServo"):setField("target angle", testTarget).
wait until getBladeAngle(bladeAngleServos[0]) <= testTarget.
local servoReactionTime to time:seconds - lastTime.

until false {
    local deltaTime to time:seconds - lastTime.

    println(servoReactionTime, 1).

    local cRoll to getRoll().
    local cPitch to getPitch().
    local cHdg to getHeading().
    local cRotorAngle to getRotorAngle(bladeAngleServos[0]).
    local angVel to getAngularVelocity().

    //Throttle
    println("Throttle: " + round(ship:control:pilotmainthrottle * 100) + "%", 3).
    //set ship:control:pilotmainthrottle to clamp(ship:control:pilotmainthrottle, prevThrottle - 0.001 * deltaTime, prevThrottle + 0.001 * deltaTime). //Prevent too fast throtteling
    set collective to clamp(collective + ship:control:pilotfore * 0.5, 0, maxCollectiveAngle).

    //Main Rotor
    if rotor:getmodule("ModuleRoboticServoRotor"):hasField("torque limit(%)") { //Better safe than sorry
        rotor:getmodule("ModuleRoboticServoRotor"):setField("torque limit(%)", ship:control:pilotmainthrottle * 100).
    }
    //Tail Rotor (yaw control)
    for tr in tailRotor {
        if tr:getmodule("ModuleRoboticServoRotor"):hasField("torque limit(%)") {
            tr:getmodule("ModuleRoboticServoRotor"):setField("torque limit(%)", ship:control:pilotmainthrottle * 2 * 100).
        }
    }
    for trs in tailRotorServos {
        trs:getmodule("ModuleRoboticRotationServo"):setField("target angle", tailRotorBladeAngle).
    }

    //Steering
    set pitch to clamp(5 * ship:control:pilotpitch, 5, -5).
    set pitch to clamp(pitch, 15, -25).
    set pitchPid:setpoint to -(cPitch / 100) + pitch.

    set roll to clamp(1 * ship:control:pilotroll, 1, -1).
    set roll to clamp(roll, -20, 20).
    set rollPid:setpoint to (cRoll / 30) + roll.


    set headingPid:setpoint to ship:control:pilotyaw * 5.
    set tailRotorBladeAngle to 10 + headingPid:update(time:seconds, angVel:x).

    //Emulate Swashplate (roll and pitch control)
    for bas in bladeAngleServos {

        local bladePos to vectorRelativeDirection(vxcl(ship:facing:topvector, bas:position - rotor:position), facing):normalized.
        local correction to -((relativeAngleDelta(cRotorAngle, prevRotorAngle) / deltaTime) * (servoReactionTime) * correctionFactor).

        bas:getmodule("ModuleRoboticRotationServo"):setField("target angle", collective + round(emulateSwashplate(bladePos:x * cos(correction) + bladePos:y * sin(correction), -bladePos:x * sin(correction) + bladePos:y * cos(correction), -pitchPid:update(time:seconds, angVel:y) * maxCyclicAngleChangePitch, -rollPid:update(time:seconds, angVel:z) * maxCyclicAngleChangeRoll))).
        set bladeData[bas:cid]["prevAngle"] to getBladeAngle(bas).
        set bladeData[bas:cid]["prevTargetAngle"] to getBladeTargetAngle(bas).
    }

    println(getRotorAngle(bladeAngleServos[0]), 15).
    println("Rotor delta: " + relativeAngleDelta(cRotorAngle, prevRotorAngle), 16).
    println("Rotor delta per seconds: " + relativeAngleDelta(cRotorAngle, prevRotorAngle) / deltaTime, 17).
    println("Rotations per seconds: " + round((relativeAngleDelta(cRotorAngle, prevRotorAngle) / deltaTime) / 360, 1), 18).
    println("Correction angle: " + (-relativeAngleDelta(cRotorAngle, prevRotorAngle) / deltaTime) * servoReactionTime, 19).

    //Debug outputs
    println("Target pitch: " + pitchPid:setpoint, 5).
    println("Target roll: " + rollPid:setpoint, 6).

    println("Current roll input: " + rollPid:output, 10).//round(clamp(clamp((cRoll - roll) / 8, -1, 1) + ship:control:pilotroll, -1, 1), 5), 10).
    println("Current pitch input: " + pitchpid:output, 11).//round(clamp(clamp(-(cPitch - pitch) / 15, -1, 1) + ship:control:pilotpitch * 0.35, -1, 1), 5), 11).
    //if tailRotor[0]:getmodule("ModuleRoboticServoRotor"):hasField("torque limit(%)") {
    //    println("Current yaw input: " + round(tailRotor[0]:getmodule("ModuleRoboticServoRotor"):getField("torque limit(%)"), 5), 12).
    //}
    println("Current yaw input: " + tailRotorBladeAngle, 12).
    println("Yaw speed: " + (prevHeading - cHdg) / deltaTime, 13).

    local angVel to getAngularVelocity().
    println("x: " + round(angVel:x, 2), 22).
    println(" y: " + round(angVel:y, 2), 23).
    println(" z: " + round(angVel:z, 2), 24).

    set prevRotorAngle to cRotorAngle.
    set prevHeading to cHdg.
    set prevFacing to ship:facing.
    set lastTime to time:seconds.
    Wait 0.
}