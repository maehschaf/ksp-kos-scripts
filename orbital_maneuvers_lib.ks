//orbital_maneuvers_lib
//Functions for performing orbital maneuvers like Docking, rendezvous and utility functions
runoncepath("0:basic_lib.ks").

local loadTime to time:seconds.

function findClosestApproach {
	PARAMETER startTime IS TIME:SECONDS, endTime IS TIME:SECONDS + ORBIT:PERIOD, tests is 100, mainVessel IS SHIP, secondaryVessel IS TARGET, timeOfClosestApproach IS startTime.
	LOCAL timeStep TO (endTime - startTime) / tests.
	FROM {local t is startTime.} UNTIL t > endTime STEP {set t to t + timeStep.} DO {
		if DISTANCEAT(t, secondaryVessel, mainVessel) < DISTANCEAT(timeOfClosestApproach, secondaryVessel, mainVessel) {
			SET timeOfClosestApproach TO t.
		}
	}
	//PRINT "Current closest approach: (Time: " + round(timeOfClosestApproach, 2) + " Distance: " + round(DISTANCEAT(timeOfClosestApproach, secondaryVessel, mainVessel)) + ")".
	if endTime - startTime > 0.1 {
		return findClosestApproach(timeOfClosestApproach - timeStep, timeOfClosestApproach + timeStep, tests, mainVessel, secondaryVessel, timeOfClosestApproach).
	} else {
		PRINT "Closest: (Time: " + round(timeOfClosestApproach, 2) + " Distance: " + round(DISTANCEAT(timeOfClosestApproach, secondaryVessel, mainVessel)) + ")".
		return timeOfClosestApproach.
	}
}

//Executes a Maneuver Node, timewarps if needed
function executeManeuverNode {
	PARAMETER node is NEXTNODE, burnBeforeNode is 0.5, upVector is North:vector.

	RCS OFF.
	SAS OFF.
	LOCAL burnTime TO node:DELTAV:MAG / (SHIP:AVAILABLETHRUST / SHIP:MASS).
	LOCK STEERING TO LOOKDIRUP(node:BURNVECTOR, upVector).
	IF node:ETA > 1.2 * 60 { //Timewarp if more than 1.2 minutes.
		KUNIVERSE:TIMEWARP:WARPTO(TIME:SECONDS + (node:ETA - burnTime / 2) - 60).
	}
	WAIT UNTIL kuniverse:timewarp:rate <= 1.
	WAIT UNTIL VANG(node:BURNVECTOR, SHIP:FACING:FOREVECTOR) < 1.
	PRINT "Waiting for Maneuver Node in " + node:ETA + " seconds...".
	WAIT UNTIL node:ETA <= burnTime * burnBeforeNode.
	PRINT "Starting " + round(node:deltaV:mag, 1) + "m/s burn.".
	local deltaV0 is node:deltaV.
	LOCK THROTTLE TO (node:DELTAV:MAG / (SHIP:AVAILABLETHRUST / SHIP:MASS)).
	WAIT UNTIL vdot(node:DELTAV, deltaV0) < 0.5.
	LOCK THROTTLE TO 0.
	unlock steering.
	PRINT "Done.".
	PRINT "Remaining DeltaV: " + node:DELTAV:MAG.
	PRINT "Node ETA: " + node:ETA.
}

//Creates a rendezvous maneuver for the current vessel with the given vessel. Only works with simple, almost circular orbits and follows my own behaivour when making rendezvous maneuvers... Time consuming but rather reliable.
function makeRendezvousManeuver {
	PARAMETER targetVessel IS TARGET.

	//IF ORBIT:PERIOD >= targetVessel:ORBIT:PERIOD {
	//	PRINT "Error: Current Vessel has to be in a faster (= lower) orbit than the target to use this function!".
	//	return.
	//}

	CLEARSCREEN.

	//Do everything outside of maneuvernodes for more control...
	LOCAL mTime TO TIME:SECONDS + ETA:PERIAPSIS.
	LOCAL mPrograde To calculateVelocityAtApsis(ALTITUDEAT(SHIP, mTime), (targetVessel:ORBIT:APOAPSIS + targetVessel:ORBIT:PERIAPSIS) / 2) - SHIP:VELOCITY:ORBIT:MAG.
	LOCAL node TO NODE(mTime, 0, 0, mPrograde).
	LOCAL closestApproachTime TO findClosestApproach(mTime, mTime + ORBIT:PERIOD, 10, SHIP, targetVessel).
	LOCAL closestApproachValue TO DISTANCEAT(closestApproachTime, targetVessel).

	ADD node.

	//Move the maneuver in orbit steps into the future
	UNTIL FALSE {
		REMOVE node.
		SET mTime TO mTime + ORBIT:PERIOD.
		LOCAL mPrograde To calculateVelocityAtApsis(ALTITUDEAT(SHIP, mTime), (targetVessel:ORBIT:APOAPSIS + targetVessel:ORBIT:PERIAPSIS) / 2) - SHIP:VELOCITY:ORBIT:MAG.
		SET node TO NODE(mtime, 0, 0, mPrograde).
		ADD node.
		LOCAL newTime TO findClosestApproach(mTime, mTime + ORBIT:PERIOD, 10, SHIP, targetVessel).
		if DISTANCEAT(newTime, targetVessel) <= closestApproachValue {
			SET closestApproachTime TO newTime.
			SET closestApproachValue TO DISTANCEAT(closestApproachTime, targetVessel).
		} else {
			SET mTime TO mTime - ORBIT:PERIOD.
			LOCAL mPrograde To calculateVelocityAtApsis(ALTITUDEAT(SHIP, mTime), (targetVessel:ORBIT:APOAPSIS + targetVessel:ORBIT:PERIAPSIS) / 2) - SHIP:VELOCITY:ORBIT:MAG.
			break.
		}
	}


	//Now changing time step by step
	LOCAL step TO 1.
	LOCAL turnCounter TO 0.
	UNTIL FALSE {
		REMOVE node.
		SET mTime TO mTime + step.
		LOCAL mPrograde To calculateVelocityAtApsis(ALTITUDEAT(SHIP, mTime), (targetVessel:ORBIT:APOAPSIS + targetVessel:ORBIT:PERIAPSIS) / 2) - SHIP:VELOCITY:ORBIT:MAG.
		SET node TO NODE(mtime, 0, 0, mPrograde).
		ADD node.
		LOCAL newTime TO findClosestApproach(mTime, mTime + ORBIT:PERIOD, 10, SHIP, targetVessel).
		if DISTANCEAT(newTime, targetVessel) <= closestApproachValue {
			SET closestApproachTime TO newTime.
			SET closestApproachValue TO DISTANCEAT(closestApproachTime, targetVessel).
			IF turncounter > 4 { //Make sure to get the new value AFTER turning as maneuver node.
				break.
			}
		} else {
			SET closestApproachTime TO newTime.
			SET closestApproachValue TO DISTANCEAT(closestApproachTime, targetVessel). //Setting this here does not really make sense but it works... \o.o/ -- We just want it to get closer dont care if closer than best value yet...
			SET step TO -step.
			PRINT "TURN".
			SET turnCounter TO turnCounter + 1.
		}
	}

	LOG "Final closest Approach: " + DISTANCEAT(closestApproachTime, targetVessel) TO "CloseApproaches.txt".
	PRINT "Final closest Approach: " + DISTANCEAT(closestApproachTime, targetVessel).
	return node.
}

function makeImpactAtGeoPositionManeuver {
	parameter targetGeoPos, maxDistanceToTarget is 20000.
	addons:tr:settarget(targetGeoPos).

	local node to Node(time:seconds + 15, 0, 0, 0).
	add node.

	local function waitForImpactChange {
		if addons:tr:hasimpact {
			local prevImpactPos to Addons:tr:impactpos.
			local startTime to time:seconds.
			until time:seconds - startTime > 1 {
				if addons:tr:hasimpact {
					if prevImpactPos:lng = Addons:tr:impactpos:lng and prevImpactPos:lat = Addons:tr:impactpos:lat {
						wait 0.01.
					} else {
						break.
					}
				}
			}
		} else {
			makeImpact().
			wait 1.
		}
	}

	local function makeImpact {
		if node:orbit:Periapsis > body:atm:height {
			set node:prograde to calculateVelocityAtApsis(AltitudeAt(ship, time:seconds + node:eta), body:atm:height * 0.9) - VelocityAt(ship, time:seconds + node:eta):orbit:mag.
		}
		until addons:tr:hasimpact {
			set node:prograde to node:prograde - 0.01.
			wait 0.01.
		}
	}

	local function minimizeDistanceViaBurn {
		local minDist to geoDistance(Addons:tr:impactpos, targetGeoPos).
		local step to 0.01.
		until node:orbit:Periapsis < 0 {
			set node:prograde to node:prograde - step.

			waitForImpactChange(). //Wait until trajectories applys the changes.
			wait 0.1. //Maybe remove?

			if addons:tr:hasimpact and node:orbit:periapsis > body:atm:height * 0.2 and (geoDistance(Addons:tr:impactpos, targetGeoPos) <= minDist or abs(geoDistance(Addons:tr:impactpos, targetGeoPos) - minDist) < 500) and node:eta >= 0 {
				set minDist to geoDistance(Addons:tr:impactpos, targetGeoPos).
				print "min: " + round(minDist).
			} else {
				set node:prograde to node:prograde + step.
				waitForImpactChange(). //Wait until trajectories applys the changes.
				print "Finished at: " + round(minDist).
				break.
			}
			set step to clamp(minDist / 100000, 0.000000001, 0.1).
		}
		return minDist.
	}

	makeImpact().

	local dist to minimizeDistanceViaBurn().
	until false {
		if dist < maxDistanceToTarget {
			break.
		} else {		
			print dist.
			set node:eta to node:eta + orbit:period / 16.
			if node:Orbit:Periapsis < body:atm:height * 0.2 {
				set node:Prograde to 0.
			}
			waitForImpactChange().
			makeImpact().
			set dist to minimizeDistanceViaBurn().
		}
	}
	print dist.

	return node.
}

//Calculates the Velocity at either the apoapsis or periapsis of an orbit with the given apoapsis and periapsis height using the Vis-Viva-Equation. The returned velocity is the velocity at the apsis given as first argument. If you give the height of the Apoapsis as the first argument the velocity at apoapsis will be returned and vice versa.
function calculateVelocityAtApsis {
	PARAMETER currentApsis, otherApsis, currentBody IS BODY.
	LOCAL semiMajorAxis TO (currentApsis + currentBody:radius + otherApsis + currentBody:radius) / 2.
	return SQRT((currentBody:MU / (currentApsis + currentBody:radius)) * ((otherApsis + currentBody:radius) / semiMajorAxis)).
}


//The distance of two vessels (default is SHIP and TARGET) at a given universal timestamp.
function distanceAt {
	PARAMETER timeStamp IS TIME:SECONDS, ship2 IS TARGET, ship1 IS SHIP.
	return (POSITIONAT(ship2, timeStamp) - POSITIONAT(ship1, timeStamp)):MAG.
}

function AltitudeAt {
	PARAMETER vessel, time.
	return (POSITIONAT(vessel, time) - vessel:BODY:POSITION):MAG - vessel:body:radius.
}

//Gives the time until or from (negative) the last apoapsis assuming a stable orbit
function timeDifferenceApoapsis {
	IF ETA:APOAPSIS > ORBIT:PERIOD / 2 {
		return ETA:APOAPSIS - ORBIT:PERIOD.
	} ELSE {
		return ETA:APOAPSIS.
	}
}

//Gives the time until or from (negative) the last periapsis assuming a stable orbit
function timeDifferencePeriapsis {
	IF ETA:PERIAPSIS > ORBIT:PERIOD / 2 {
		return ETA:PERIAPSIS - ORBIT:PERIOD.
	} ELSE {
		return ETA:PERIAPSIS.
	}
}

printLog("Loaded " + ScriptPath():name + " in " + round(time:seconds - loadTime, 2) + "s").