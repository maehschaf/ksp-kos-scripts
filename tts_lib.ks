//Library for text to speech audio output using an external python application
RunOncePath("0:/basic_lib.ks").

local loadTime to time:seconds.

local transferFile to path("0:/Data/tts_kOS.json").
local transferList to List().
local ttsEnabled to False.

WriteJson(transferList, transferFile).

local messageId to 0.
local changed to False.


global function setTTSEnabled {
    Parameter enabled.

    set ttsEnabled to enabled.

    printLog("Text to Speech " + (choose "enabled" if enabled else "disabled" )).

    if enabled {
        when changed and HomeConnection:isconnected and Exists(transferFile) then {
            if HomeConnection:isconnected and Exists(transferFile) {
                WriteJson(transferList, transferFile).
                MovePath(transferFile, "0:/Data/tts_python.json").
                set changed to False.
            }
            return ttsEnabled.
        }
    }
}

setTTSEnabled(True).

global function ttsLog {
    parameter text, timeout is 1, priority is MessagePriority:INFO, rate is 150, volume is 1.
    
    local newTransferList to List().

    //Cleanup
    for i in transferList {
        if KUniverse:realtime < i:timecreated + i:timeout or (i:timeout <> -1 and i:timecreated + 600 < Kuniverse:realtime){
            newTransferList:add(i).
        }
    }
    newTransferList:add(TTSMessage(text, priority, timeout, volume, rate)).
    set changed to True.

    set transferList to newTransferList.
}

local function TTSMessage {
    Parameter text, priority, timeout is -1, volume is 1.0, rate is 150.

    local self to Lexicon().
    set self:text to text.
    set self:priority to priority.
    set self:timeout to timeout.
    set self:volume to volume.
    set self:rate to rate.
    set self:timecreated to KUniverse:realtime.
    set self:id to messageId.
    set messageId to messageId + 1.

    initTypeFunctionOverrides(self, "TTSMessage").

    return self.
}

global function isCountdownNumber {
    //Tell me if this is a nice number to mention in a countdown
    Parameter number, startFinalCount is 10.
    return number < startFinalCount or Mod(number, 1000) = 0 or (Mod(number, 100) = 0 and number < 1000) or number = 50 or number = 20 or number = 10 or number = 5.
}


global function ttsStatusReport {
    ttsLog(ship:name + ": Status Report", 60).
    if MissionTime > 0 {
        ttsLog("M,E,T: " + time(missiontime):calendar + ", " + time(missiontime):clock + " into flight", 60).
    }
    ttsLog("Situation: " + ship:status, 60).
    if isStatusLanded() {
        ttsLog("On: " + body:name, 60).
        ttsLog("Altitude: " + round(Altitude, 1) + " Meters above sea level", 60).
        ttsLog("Ground Speed: " + round(groundspeed, 1) + " meters per second", 60).
    } else if ship:status = "flying" {
        ttsLog("Radar altitude: " + round(alt:radar, 1) + " meters", 60).
        ttsLog("Air Speed: " + round(Airspeed, 1) + " meters per second", 60).
        ttsLog("Vertical Speed: " + round(VerticalSpeed, 1) + " meters per second", 60).
        ttsLog("Ground Speed: " + round(groundspeed, 1) + " meters per second", 60).
        ttsLog("Mach Number: " + round(getMachNumber(), 1), 60).
    } else {
        ttsLog("Altitude: " + round(Altitude / 1000, 2) + " Kilometers above " + body:name, 60).
        ttsLog("Orbital Velocity: " + round(ship:velocity:orbit:mag) + " meters per second", 60).
    }
    list sensors in senslist.
    local sens to list().
    for s in senslist {
        sens:add(s:type).
    }

    if sens:contains("acc") {
        ttsLog("Acceleration: " + round(ship:sensors:acc:mag, 1) + " meters per second squared", 60).
    }
    if sens:contains("temp") {
        ttsLog("Temperature: " + round(ship:sensors:temp, 1) + " Kelvin", 60).
    }
    if sens:contains("pres") {
        ttsLog("Pressure: " + round(ship:sensors:pres, 1) + " Atmospheres", 60).
    }
    
    ttsLog("Sun exposure: " + round(ship:sensors:light * 100) + " percent", 60).
    if ship:crew():length() = 1 {
        local kerbal to ship:crew()[0].
        ttsLog(kerbal:trait + ": " + kerbal:name, 60).
    } else {
        ttsLog("Crew: " + ship:crew():length(), 60).
    }
}


printLog("Loaded " + ScriptPath():name + " in " + round(time:seconds - loadTime, 2) + "s").