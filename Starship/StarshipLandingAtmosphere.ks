//Script to perform accurate Landings with a Starship
parameter targetGeoPos.

set Config:ipu to 800.

RunOncePath("0:basic_lib.ks").
RunOncePath("0:atmospheric_steering_lib.ks").
RunOncePath("0:Starship/StarshipParts.ks").

sas off.

if Airspeed > 900 or not addons:tr:hasimpact {
    balanceCoM(1, 1).
    setWingletControlAuthority(150).
    runpath("0:Common/ControlledEntry.ks", targetGeoPos).
}

set Config:ipu to 800.

//
//Suicide burn "inspired" by land.ks
local LOCK gravityAcceleration TO getGravityAcceleration().
local LOCK maxAcc TO SHIP:AVAILABLETHRUST() / SHIP:MASS.
local LOCK maxVerticalAcc TO maxAcc - gravityAcceleration.
local LOCK dragAcc TO gravityAcceleration + scalarProj(SHIP:SENSORS:ACC, UP:VECTOR).
//local LOCK horizontalDragAcc TO abs(scalarProj(SHIP:SENSORS:ACC, VectorExclude(Up:vector, ship:velocity:surface))).

local LOCK sBurnTime TO -SHIP:VERTICALSPEED/maxVerticalAcc + GROUNDSPEED/maxAcc.
local LOCK sBurnAlt TO SHIP:VERTICALSPEED^2 / (2 * (maxVerticalAcc + dragAcc / 2)) + GROUNDSPEED^2 / (maxAcc).// + horizontalDragAcc).
local LOCK flipAlt to sBurnAlt + abs(VerticalSpeed) * 5.

local lock altAboveLandingPos to altitude - targetGeoPos:terrainheight.
if body:hasocean {
    lock altAboveLandingPos to altitude - max(0, targetGeoPos:terrainheight).
}

local function getForwardDistance {
    local groundDiff to VectorExclude(up:vector, targetGeoPos:position - ship:position).
    return vectorRelativeDirection(groundDiff, VectorExclude(up:vector, ship:Facing:forevector)):x.
}

local function getHorizontalDistanceVec {
    return VectorExclude(up:vector, targetGeoPos:position - ship:position).
}

local Function getHorizontalDistance {
    return getHorizontalDistanceVec():mag.
}


//Run Mode
local runMode to 0. //0 = entry (nose up), 1 = approach (nose down), 2 = final approach (nose up), 3 = landing


//Setup variables for different run modes
//Entry mode 0
lock Steering to heading(getHeading() + clamp(getRelativeHeading(targetGeoPos:position), -10, 10), 25) + R(0,0, clamp(-targetGeoPos:bearing * 1.9, getRoll() - 3, getRoll() + 3)).

setWingletControlAuthority(100).

//Approach mode 1
local AltitudePid to PidLoop(0.05, 0, 1, -25, 10).
local altToDistRatio to 1.

//Final approach mode 2 
local horizontalSpeedPid to pidloop(-0.5, 0, 0, -5, 50).

//Landing mode 3
//Target Speed during sBurn
local targetSpeed TO 20.//clamp(alt:radar / 10, 0.4, 20).

//Program sequence
when Altitude > getHorizontalDistance() / 3 then { //Go to Approach mode with nose down
    set runMode to 1.

    set altToDistRatio to Altitude / getHorizontalDistance().
    lock Steering to heading(getHeading() + clamp(getRelativeHeading(targetGeoPos:position), -10, 10), clamp(AltitudePid:update(time:seconds, altAboveLandingPos), getPitch() - 8, getPitch() + 8)) + R(0,0, clamp(-targetGeoPos:bearing * 1.9, getRoll() - 3, getRoll() + 3)).

    when (groundspeed < 100 or groundspeed > getHorizontalDistance() / 5) and runMode = 1 then {
        set runMode to 2.
        local theading to targetGeoPos:Heading.
        lock Steering to heading(theading, clamp(horizontalSpeedPid:update(time:seconds, groundspeed), getPitch() - 5, getPitch() + 5)) + R(0,0,0).
        set horizontalSpeedPid:setpoint to 50.
    }
}

//Steer during suicide Burn
local Function hoverSteeringVector {
    parameter maxAngle is 15.
    local targetVel to (getHorizontalDistanceVec():normalized * clamp(getForwardDistance() / 100, 0, 1)) * clamp(alt:radar / 100, 0, 1).
    print targetVel:mag at(0, 20).
    return (targetVel - vectorexclude(up:vector, ship:Velocity:surface) / 5):normalized * sin(maxAngle).
}

//Doing flip and Landing burn
when altAboveLandingPos > flipAlt then { //Make sure to not flip way to early
    when altAboveLandingPos <= flipAlt or getHorizontalDistance() < 100 then { //Do the flip!
        set runMode to 3.
        setWingletControlAuthority(0).
        HUDTEXT ("Doing flip", 20, 3, 20, rgb(1,1,0.5), true).
        LOCK STEERING TO lookdirup(UP:forevector + hoverSteeringVector(clamp(alt:radar / 20, 1, 10)), north:starvector).
        
        SET TRANSFERALL("oxidizer", frontTanks, backTanks):active to true.
        SET TRANSFERALL("liquidfuel", frontTanks, backTanks):active to true.

        when altAboveLandingPos <= sBurnAlt then {
            HUDTEXT ("Starting Suicide Burn at " + round(ALT:RADAR) + "m", 20, 3, 20, rgb(1,1,0.5), false).
            Lock throttle TO ((SHIP:MASS * gravityAcceleration) / SHIP:AVAILABLETHRUST) + ((-SHIP:VERTICALSPEED - targetSpeed) / 5).
            when alt:radar < 40 then {
                //LOCK STEERING TO lookdirup(UP:forevector + (VectorExclude(Up:vector, SrfRetrograde:vector) * 0.1), north:starvector).
                set targetSpeed to 5.
                when alt:radar < 5 then {
                    set targetSpeed to 0.5.
                }
            }
        
        }

        when alt:radar < 100 or addons:tr:timetillimpact < 10 then {
            Gear on.
            lock altAboveLandingPos to alt:radar.
        }
    }
}

//Shifting the CoM to pitch
local prevPitchError to 0.
local pitchCorrectionFactor to 1.
//when groundspeed < 200 then {
//    set pitchCorrectionFactor to 0.2.
//}


ClearScreen.
print "Starship Landing Atmosphere".

local lastTickTime to time:seconds.
local prevPitch to getPitch().
local prevPitchChange to 0.
Until SHIP:STATUS = "SPLASHED" or SHIP:STATUS = "LANDED" {
    local deltaTime to max(time:seconds - lastTickTime, 0.000001).
    set lastTickTime to time:seconds.

    if runMode = 1 {
        set AltitudePid:setpoint to altToDistRatio * getHorizontalDistance() + targetGeoPos:terrainheight + 1500.
    } else if runMode = 2 {
        set horizontalSpeedPid:setpoint to getForwardDistance() / 50.
    }

    if runMode < 3 {
        //Shifting the CoM to pitch
        local pitchError to getPitch(Steering) - getPitch().
        local pitchChange to (prevPitch - getPitch()) / deltaTime.
        print "pitch Change: " + pitchChange + "                    " at(0, 21).

        local targetPitchChange to clamp(pitchError / 5, -1, 1).

        print "targetPitchChange: " + targetPitchChange + "                    " at(0, 22).
        print "Fuel pumped: " + abs(targetPitchChange - pitchChange) at (0, 23).

        //if pitchChange < targetPitchChange {
        //    SET TRANSFER("oxidizer", frontTanks, backTanks, abs(targetPitchChange - pitchChange) * pitchCorrectionFactor):active to true.
        //    SET TRANSFER("liquidfuel", frontTanks, backTanks, abs(targetPitchChange - pitchChange) * pitchCorrectionFactor):active to true.
        //} else if pitchChange > targetPitchChange {
        //    SET TRANSFER("oxidizer", backTanks, frontTanks, abs(targetPitchChange - pitchChange) * pitchCorrectionFactor):active to true.
        //    SET TRANSFER("liquidfuel", backTanks, frontTanks, abs(targetPitchChange - pitchChange) * pitchCorrectionFactor):active to true.
        //}
        
        if pitchError > 0 and prevPitchError < pitchError or pitchChange < clamp(pitchError, -2, 0) { //Shift fuel back
            SET TRANSFER("oxidizer", frontTanks, backTanks, abs(pitchError) * pitchCorrectionFactor):active to true.
            SET TRANSFER("liquidfuel", frontTanks, backTanks, abs(pitchError) * pitchCorrectionFactor):active to true.
        } else if pitchError < 0 and prevPitchError > pitchError or pitchChange > clamp(pitchError, 2, 0) {
            SET TRANSFER("oxidizer", backTanks, frontTanks, abs(pitchError) * pitchCorrectionFactor):active to true.
            SET TRANSFER("liquidfuel", backTanks, frontTanks, abs(pitchError) * pitchCorrectionFactor):active to true.
        }
        set prevPitchError to pitchError.
        set prevPitch to getPitch().
        set prevPitchChange to pitchChange.
    }

    printInfo().
}

set Throttle to 0.
set ship:control:mainthrottle to 0.

wait 3.

unlock Throttle.
unlock Steering.
HUDTEXT ("Vehicle saved", 20, 3, 20, rgb(1,1,0.5), true).



//Prints
local function printInfo {
    if addons:tr:hasimpact {
        print "Flipping at altitude: " + round(flipAlt) + "            " at(0,2).
        print "Starting burn at altitude: " + round(sBurnAlt) + "            " at(0,3).
        print "Time until impact: " + time(addons:tr:timetillimpact):clock at(0,4).
        print "Time needed for burn: " + time(sBurnTime):clock at(0,5).

        print "Distance (impact to target) in km: " + round((geoDistance(Addons:tr:impactpos, targetGeoPos) / 1000), 2) at (0, 7).
    }
    print "Altitude above landing position:" + round(altAboveLandingPos, 1) at (0, 8).


    print "pitch: " + horizontalSpeedPid:output at(0, 11).
    print "heading: " + targetGeoPos:Heading at(0, 12).

    print "CoM offset: " + -vectorRelativeDirection(CoDPart:position, ship:Facing):x at(0,15).
    if addons:tr:hasimpact {
        print "Horizontal target dist: " + getHorizontalDistance() at(0,17).
        if runMode = 1 {
            print "Altitude trgt: " + AltitudePid:setpoint at(0,18).
        } else if runMode = 2 {
            print "Horizontal Speed trgt: " + horizontalSpeedPid:setpoint at(0,18).
        }
        
    }
}
