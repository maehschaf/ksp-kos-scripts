@LazyGlobal Off.
Parameter target is waypoints["KSCLandingPad1"]:geopos.
//Starship Entry Decent Landing -- by maehschaf

RunOncePath("0:/basic_lib.ks").
RunOncePath("0:/mainloop.ks").
RunOncePath("0:/steering_lib.ks").
RunOncePath("0:/Starship/Starship.ks").

RunOncePath("0:/Util/PIDTuner.ks").

RunOncePath("0:/boot/Vessel.ks").

local pitchControlBuffer to list().
local inputLexicon to Lexicon("overwrite", False, "heading", 0, "pitch", 0,"roll", 0, "state", FlapState:CONTROL, "initialheading", 0).
local estimatedflipTime to 5.

openTerminal().
//TOOODOOOOOO 
// - Add Data Loop
// - Add Status Loop that tracks things like electric charge and rcs use

CLEARVECDRAWS().
local vecdraw1 to VecDraw(V(0,0,0), V(0,0,0), RED, "", 1.0, True).
local vecdraw2 to VecDraw(V(0,0,0), V(0,0,0), GREEN, "", 1.0, True).
local vecdraw3 to VecDraw(V(0,0,0), V(0,0,0), Blue, "", 1.0, True, 2).

set vecdraw3:vectorupdater to { return -up:vector * ship:altitude. }.

//DEBUG
local printLoop to LoopingFunction("atmPrinting", {
    tunePIDloop(pitchPID, "pitch").
    tunePIDloop(yawPID, "yaw").
    tunePIDloop(rollPID, "roll").
    deletepath("0:/Data/ATMData.txt").
},{
    println("Q: " + ship:q * constant:atmtokpa, 12).
    log time:seconds + ";" + ship:q * constant:atmtokpa to "0:/Data/ATMData.txt".
    println("Pitch Trim: " + ship:control:pitchtrim, 13).
    println("Mach: " + getMachNumber(), 14).
    println("RelativeCoM Position: " + round(getRelativeCoMPosition():z, 2), 15).
    println("Throttle:" + round(Throttle, 2), 16).
    workaround:typename. //We need to call something from the GUI, otherwise it disappeares...
    smcg:update().
}, {ClearScreen.}).

//BALISTIC (before hitting the atmosphere)
local balisticLoop to LoopingFunction("balistic", {
    disableSteering(). //Maybe use our own steering in the future, for now use kOS default

    set inputLexicon:state to FlapState:CONTROL.
    set balisticLoop:steering to Prograde.
    lock steering to balisticLoop:steering.
},{
    set balisticLoop:steering to Prograde.

    updateFlaps(inputLexicon:state).

    if SHIP:BODY:ALTITUDEOF(PositionAt(ship, time:seconds + 30)) < ship:body:atm:height {
        removeFromMainLoop("balistic").
        ttsLog("Starting entry in 30 seconds").
        addToMainloop(entryLoop).
    }
},{
    unlock steering.
    disableSteering().
}).


//ENTRY -- IMPORTANT! Use lift based steering here to probaply... Back fins dont have enough authority, move CoM??
local entryLoop to LoopingFunction("starshipEntry", {//Start
    vesselInfo:initSteering("starshipEntry").
    antennaStarship:getModule("ModuleDeployableAntenna"):doAction("retract antenna", True).
    Bays off.
    Panels off.
    //TODO turn on fuel Cell!1 fuelCellStarship//!Could be multiple!
    setGearHingesLocked(False).
    set inputLexicon:state to FlapState:CONTROL.
},{//Loop
    setSteeringTarget(Heading(getHeading(ship:velocity:surface), getPitch(ship:velocity:surface) + 80, 0)).

    //Trim
    set ship:control:pitchtrim to getAveragePitchControl(100).

    updateSteering(1).
    updateFlaps(inputLexicon:state, ship:control:pitch, ship:control:yaw, ship:control:roll, FlapReferenceFrame:SRFRETROGRADE).

    updateCoM().

    if ship:q > 0 and getMachNumber() < 1 {
        removeFromMainLoop("starshipEntry").
        addToMainloop(descentLoop).
    }
},{//End
    disableSteering().
    set ship:control:pitchtrim to 0.
    playNote().
}).


//DESCENT
local descentLoop to LoopingFunction("starshipDescent", {
    vesselInfo:initSteering("starshipGlideDirect").
    set inputLexicon:state to FlapState:CONTROL.
    set inputLexicon:initialHeading to target:heading.
    set inputLexicon:heading to inputLexicon:initialHeading.
    RCS Off.
    useSeaLevelEngines(true).
    setGearHingesLocked(False).
    set ship:control:pitchtrim to 0.
    set descentLoop:bounds to ship:bounds.
    set descentLoop:accPid to PidLoop(6, 1, 6, -30, 30). //+ and - are inverted here
    tunePIDloop(descentLoop:accPid, "forward Acceleration").
    set descentLoop:lastForwardVelocity to 0.
    set descentLoop:lastTick to time:seconds - 1.

    set descentLoop:approachState to 0.
    set descentLoop:pitchInterper to createRateInterpolator(0, 0, 3).
    set descentLoop:headingInterper to createRateInterpolator(getHeading(), target:heading, 5).
},{
    //DATA and Flip/Suicide Burn
    local radarAltitude to descentLoop:bounds:bottomaltradar.
    local accDecel to accelerationNeededToReachVelocityInDistance(airspeed, radarAltitude - (estimatedFlipTime * abs(VerticalSpeed))).
    local accNeeded to getGravityAcceleration() + accDecel. //Probably a better way to do this -> Put in data loop or something
    //^^Replace getGravityAcceleration with measured acceleration

    //Steering
    local targetImpact is target:position.
    local currentImpact is ship:position.
    IF addons:tr:available and addons:tr:hasimpact {
	    //set currentImpact to addons:tr:impactpos:position.
    }
    local flatFacing to lookdirup(vectorexclude(up:vector, ship:facing:vector), up:vector).
    local relativeTargetImpact is vectorRelativeDirection(targetImpact, flatFacing).
    local relativeCurrentImpact is vectorRelativeDirection(currentImpact, flatFacing).
    local relativeImpactError is relativeTargetImpact - relativeCurrentImpact.

    local relativeVelocity to vectorRelativeDirection(Velocity:surface, flatFacing).

    local forwardAcceleration to (relativeVelocity:z - descentLoop:lastForwardVelocity) / (time:seconds - descentLoop:lastTick).

    set descentLoop:lastForwardVelocity to relativeVelocity:z.
    set descentLoop:lastTick to time:seconds.

    if descentLoop:approachState = 0 { //Turning to heading
        set descentLoop:accPid:setpoint to 0.
        if abs(getRelativeHeading(target:position)) < 5 and abs(getSmoothedAngularVelocity(1):y) < 0.5 {
            set descentLoop:approachState to 1.
        }
    } else if descentLoop:approachState = 1 {//Accelerate
        set descentLoop:accPid:setpoint to 1. //Accelerate with 3m/s^2
        if accelerationNeededToReachVelocityInDistance(relativeVelocity:z, relativeImpactError:z) > 0.2 {
            set descentLoop:approachState to 2.
        }
    } else if descentLoop:approachState = 2 {//Brake
        set descentLoop:accPid:setpoint to -accelerationNeededToReachVelocityInDistance(relativeVelocity:z, relativeImpactError:z).
        if abs(relativeImpactError:z) < 100 and abs(relativeVelocity:z) < 20 {
            set descentLoop:approachState to 3.
            set descentLoop:finalHeading to getHeading().
        }
    } else if descentLoop:approachState = 3 { //Above Landing Site
        local targetDistance to -50. //Move past the center of the site
        local targetSpeed to clamp((relativeImpactError:z - targetDistance) * 0.1, -10, 5).
        println("Target Forward Speed : " + round(targetSpeed, 1) + "m/s", 22).
        set descentLoop:accPid:setpoint to clamp((targetSpeed - relativeVelocity:z) * 0.8, -5, 1.5).
    }
    //Ideas:
    // - Acceleration based control
    // - Increase Speed until impact at landing side
    // - Hold and adjust that speed!

    if not inputLexicon:overwrite {
        set descentLoop:pitchInterper:target to choose 0 if descentLoop:approachState = 0 else -descentLoop:accPid:update(time:seconds, forwardAcceleration).
        set inputLexicon:pitch to descentLoop:pitchInterper:update().
        set descentLoop:headingInterper:target to choose descentLoop:finalHeading if descentLoop:approachState = 3 else target:heading. //Temp
        set inputLexicon:heading to descentLoop:headingInterper:update().
        set inputLexicon:roll to 0.
    }
    
    println("Forward Impact Error: " + round(relativeImpactError:z, 1) + " Impact distance from target: " + round((targetImpact - currentImpact):mag, 1), 18).
    println("", 19).
    println("Forward Speed: " + round(relativeVelocity:z, 1) + " Approach State: " + descentLoop:approachState, 20).
    println("Forward Acc: " + round(forwardAcceleration, 2) + " Target Acc: " + round(descentLoop:accPid:setpoint, 2), 21).
    
    //heading is temp, should add different steering modes like plane and stuff to steering_lib... Will lead to a redesign I guess...
    set inputLexicon:heading to getHeading() + clamp(relativeAngleDelta(inputLexicon:heading, getHeading()), 50).
    setSteeringTarget(Heading(inputLexicon:heading, inputLexicon:pitch, inputLexicon:roll)). //Roll should always be 0!

    updateSteering().
    updateFlaps(inputLexicon:state).

    //Balance CoM
    updateCoM().

    //TTS
    if isCountdownNumber(floor(alt:radar, -1)) and alt:radar > 1000 { //TODO doesnt work with passing numbers
        ttsLog("Altitude: " + floor(alt:radar, -1) + " meters").
    }
    
    //Start Flip
    if accNeeded > getAvailableAcceleration() * 0.8 { //lets pretend we only have 80% power to have a safety margin
        removeFromMainLoop("starshipDescent").
        addToMainloop(landingLoop).
    }
},{
    disableSteering().
    set ship:control:pitchtrim to 0.
}).

//LANDING
local landingLoop to LoopingFunction("starshipLanding", {
    vesselInfo:initSteering("starshipFlip").
    set landingLoop:hoversteering to up.
    
    useSeaLevelEngines(true).
    setGearHingesLocked(False).
    
    set landingLoop:flip to true.
    set landingLoop:throttle to 0.
    set landingLoop:bounds to ship:bounds.
    set landingLoop:touchdowntime to -1.
    set landingLoop:flipDuration to 4. //Important!
    set landingLoop:flipStartTime to time:seconds.
    set landingLoop:initialDirection to ship:facing.

    set inputLexicon:state to FlapState:DRAGCONTROL.
    lock throttle to landingLoop:throttle.

    ttsLog("Rotate!", 5).
},{
    
    //RCS
    local rcsgate to 5.
    local currentSteering to choose steering if getSteeringMode() = "none" else getSteeringDirection().
    if vang(currentSteering:FOREVECTOR, facing:forevector) > rcsgate or vang(currentSteering:topvector, facing:topvector) > rcsgate {
        rcs on.
    } else {
        rcs off.
    }

    if not isStatusLanded() {
        //Prevent from landing with horizontal velocity
        local targetAltitude to 0.45 + clamp(groundspeed * 0.05, 0, 30). //0.45 m is the offset with landing gear extended

        local radarAltitude to landingLoop:bounds:bottomaltradar.
        local relativeAccNeeded to accelerationNeededToReachVelocityInDistance(VerticalSpeed, max(radarAltitude - targetAltitude, 0.1)). //Make acceleration Needed handle negative stuff
        local accNeeded to getGravityAcceleration() + relativeAccNeeded.
        local burnTime to abs(VerticalSpeed) / relativeAccNeeded.
        
        println("Radar Altitude: " + radarAltitude, 17).
        println("Time to Landing: " + round(burnTime, 1) + "s", 20).

        //HOVER STEERING
        //Hover Steering
        local geoTargetOffset to vectorRelativeGeoCoords(target:position).
        local geoVelocity to vectorRelativeGeoCoords(ship:velocity:surface).
        //TargetVelocity
        local maxGeoTargetVelocity to max(radarAltitude - 20, 0) * 0.3.
        local geoTargetVelocity to V(geoTargetOffset:x, 0, geoTargetOffset:z) * 0.3. //Interpolate offset from target and height to get as close as possible but a smooth touchdown too...
        set geoTargetVelocity:x to clamp(geoTargetVelocity:x, -maxGeoTargetVelocity, maxGeoTargetVelocity).
        set geoTargetVelocity:z to clamp(geoTargetVelocity:z, -maxGeoTargetVelocity, maxGeoTargetVelocity).

        //Steering --- EEEEEEEEEEEEEH does not seem to always steer correctly?
        //TODO probaply need to use PIDs here...
        local eastSteer to clamp((geoTargetVelocity:x - geoVelocity:x) * 2.0, 10).
        local northSteer to clamp((geoTargetVelocity:z - geoVelocity:z) * 2.0, 10).
        println("Position Error - East : " + round(geoTargetOffset:x, 1) + "m North: " + round(geoTargetOffset:z, 1) + "m ", 18).
        println("Target Velocity - East : " + round(geoTargetVelocity:x, 1) + "m/s  North: " + round(geoTargetVelocity:z, 1) + "m/s", 23).
        println("East Steering: " + round(eastSteer, 1) + "°  North Steering: " + round(northSteer, 1) + "°", 24).

        set landingLoop:hoversteering to lookdirup(UP:vector, -landingLoop:initialDirection:vector) + ANGLEAXIS(northSteer, -NORTH:STARVECTOR) + ANGLEAXIS(eastSteer, -NORTH:FOREVECTOR). //Make sure we don't roll
        //TODO: Always roll in the direction of movement, never try to hover sideways!

        //DEBUG
        println("Acc Needed:" + round(accNeeded, 1) + " Acc Available: " + round(getAvailableAcceleration(), 1), 26).

        //FLIP STEERING
        //Steering in the flip part
        if landingLoop:flip {
            set vecdraw1:vector to getSteeringDirection:vector * 20.
            set vecdraw2:vector to getSteeringDirection:topvector * 20.

            local noLateralHoversteering to VectorExclude(landingLoop:initialDirection:starvector, landingLoop:hoversteering:vector). //Remove lateral component

            local flipAlpha to clamp((time:seconds - landingLoop:flipStartTime) / landingLoop:flipDuration, 0, landingLoop:flipDuration).
            local flipVec to noLateralHoversteering * flipAlpha + landingLoop:initialDirection:vector * (1 - flipAlpha).
            local flipUpVec to -landingLoop:initialDirection:vector * flipAlpha + up:vector * (1 - flipAlpha). //Fade between up and -initialDirection
            setSteeringTarget(lookdirup(flipVec, flipUpVec)).

            updateSteering().
            updateFlaps(inputLexicon:state, ship:control:pitch, ship:control:yaw, ship:control:roll, FlapReferenceFrame:VESSEL).

            //CoM Balancing -- Put it back first for faster flip, then to the front - Basically a quick jerk
            updateCoM((1 - flipAlpha * 2) * -8).

            if vang(landingLoop:hoversteering:vector, facing:vector) < 20 or abs(accNeeded - getAvailableAcceleration() * 0.9) < 1 { //Flip End Condition //The acc Needed conditions move pretty fast...
                disableSteering().

                lock steering to landingLoop:hoversteering.
                set landingLoop:flip to False.

                LOG "Flip Time: " + (time:seconds - landingLoop:flipStartTime) + "s on " + body:name to "0:Starship/EDLLog.txt".
                playNote(500, 0.1).
            }
        } else {
            set vecdraw1:vector to steering:vector * 20.
            set vecdraw2:vector to steering:topvector * 20.

            //Throttle
            if VerticalSpeed < -0.2 { //We don't want to go up again! //TODO --- make that smoother, speed and not acceleration control u know what i mean
                local accWanted to accNeeded / max(cos(VAng(ship:facing:vector, up:vector)), 0.5). //Accounting for vessel not being pointed straight up
                set landingLoop:throttle to abs(accWanted / getAvailableAcceleration()).
            } else {
                playNote(600, 0.1).
                set landingLoop:throttle to ((getGravityAcceleration() + (clamp(-radarAltitude * 0.5, 0, -1) - VerticalSpeed)) / getAvailableAcceleration()). //Counters gravity and accelerates to -0.1m/s vertical speed within 1 second
            }

            //Flaps
            if inputLexicon:state <> FlapState:DEPLOYED { //Flap Control!!
                if getPitch(ship:facing:topvector, landingLoop:hoversteering:vector) < 0 {
                    set inputLexicon:state to FlapState:RETRACTED.
                    updateFlaps(inputLexicon:state).
                } else  {
                    set inputLexicon:state to FlapState:PASSIVEREV.
                    updateFlaps(inputLexicon:state).
                }
            }

            
            //Landing Legs
            if burnTime < 10 and not gear { //Those legs take AGES!
                ttsLog("Gear", 5).
                gear on.
            }
            if burnTime < 2 and inputLexicon:state <> FlapState:DEPLOYED {
                set inputLexicon:state to FlapState:DEPLOYED.
                updateFlaps(inputLexicon:state).
            }
            if burnTime < 1 {
                setGearHingesLocked(True). //Lock Flaps
            }

            //CoM Balancing -- Put it all the way to the front
            updateCoM(0).
        }

        //TTS
        if isCountdownNumber(Ceiling(radarAltitude), 5) {
            ttsLog(Ceiling(radarAltitude), 0.5, MessagePriority:INFO, 200).
        }

    } else {
        setGearHingesLocked(True). //Still Lock Flaps, they are kinda important!
        if landingloop:touchdowntime = -1 {
            set landingLoop:touchdowntime to time:seconds.
            ttsLog("Touchdown!", 5).
            rcs on.
            lock throttle to 0.
            lock steering to up.
        } else if time:seconds > landingLoop:touchdowntime + 5 {
            ttsLog("Landing Complete!", 5).
            removeFromMainLoop("starshipLanding").
        }
    }
},{
    disableSteering().
    Unlock throttle.
    Unlock steering.
    set ship:control:pilotmainthrottle to 0.
    rcs off.
}).

clearMainloop().
SAS off.
ClearScreen.

local smcg to StarshipManualControlGui().
local workaround to smcg:gui.

addToMainloop(printLoop).
addToMainloop(balisticLoop).


if not vesselInfo:get("steeringFactors"):hasKey("starshipEntry") { //Rate based control on entry
    vesselInfo:get("steeringFactors"):add("starshipEntry", SteeringFactors(
        list(0.5, 0, 0), list(0.2, 0, 0), list(0.1, 0, 0.01),    //pitch, yaw, roll
        list(0.2, 0, 0, -10, 10), list(0.3, 0, 0, -10, 10), list(0.1, 0, 0, -10, 10)     //Rate pitch, yaw, roll
    )).
}

if not vesselInfo:get("steeringFactors"):hasKey("starshipGlideDirect") { //Direct control during atmospheric flight
    vesselInfo:get("steeringFactors"):add("starshipGlideDirect", SteeringControlFactors(
        list(0.05, 0.01, 0.04), list(0.02, 0, 0.04, -0.3, 0.3), list(0.006, 0.00005, 0.015, -0.5, 0.5)   //pitch, yaw, roll -- Funkt so semi gut.. Mit langsamen änderungen kommt er klar, sonst eher nich so..
    )).
}

if not vesselInfo:get("steeringFactors"):hasKey("starshipFlip") { //Direct control during "THE FLIP"
    vesselInfo:get("steeringFactors"):add("starshipFlip", SteeringControlFactors(
        list(0.04, 0.01, 0.2), list(0.02, 0, 0.04, -0.3, 0.3), list(0.006, 0.00005, 0.015, -0.5, 0.5)   //pitch, yaw, roll
    )).
}

local function getAveragePitchControl { //Prob use the I Term of the PID instead :D
    Parameter avgTime is 100. //Setting this to high causes lag!!
    local sum to 0.
    if pitchControlBuffer:length() >= avgTime {
        pitchControlBuffer:remove(0).
    }
    pitchControlBuffer:add(ship:control:pitch). //Maybe only add if pitch error is low?

    for v in pitchControlBuffer {
        set sum to sum + v.
    }


    return sum / avgTime.
}

//GUI

global function StarshipManualControlGui {

    local root to Lexicon().
    set root:gui to Gui(400, 200).

    set root:lheader to addHeader(root:gui, "Starship Manual Control").

    set root:cbOverwrite to root:gui:addCheckbox("AutoPilot", not inputLexicon:overwrite).
    set root:cbOverwrite:ontoggle to {
        Parameter new.
        set inputLexicon:overwrite to not new. //Confusing naming, I know
    }.

    set root:ltargetHeading to root:gui:addLabel("Heading: " + inputLexicon:Heading).
    set root:vsheading to root:gui:addHSlider(0, 0, 360).
    set root:vsheading:onchange to {
        Parameter val.
        set inputLexicon:heading to val.
        set root:ltargetHeading:text to "Heading: " + round(val, 2).
    }.

    set root:ltargetPitch to root:gui:addLabel("Pitch: " + inputLexicon:pitch).
    set root:vsPitch to root:gui:addHSlider(0, -90, 90).
    set root:vsPitch:onchange to {
        Parameter val.
        set inputLexicon:pitch to val.
        set root:ltargetPitch:text to "Pitch: " + round(val, 2).
    }.

    set root:ltargetRoll to root:gui:addLabel("Roll: " + inputLexicon:roll).
    set root:vsRoll to root:gui:addHSlider(0, -30, 30).
    set root:vsRoll:onchange to {
        Parameter val.
        set inputLexicon:roll to val.
        set root:ltargetRoll:text to "Roll: " + round(val, 2).
    }.

    if hasBooster() {
        root:gui:addSpacing(20).
        set root:bUndock to root:gui:addButton("Seperate from Booster").
        set root:bUndock:onclick to {
            decoupleBooster().
            root:bUndock:dispose().
        }.
    }

    set root:lFlapState to root:gui:addLabel("FlapState: " + inputLexicon:state).
    for fs in FlapState:keys() {
        local f to fs.
        local b to root:gui:addButton(f).
        set b:onclick to {
            set inputLexicon:state to f.
            set root:lFlapState:text to "FlapState: " + f.
        }.
    }

    root:gui:addSpacing(20).

    set root:gui:addButton("Close"):onclick to {
        root:gui:dispose().
    }.

    set root:update to {
        set root:lFlapState:text to "FlapState: " + inputLexicon:state.
        if not inputLexicon:overwrite {
            set root:vsheading:value to inputLexicon:heading.
            set root:vspitch:value to inputLexicon:pitch.
            set root:vsroll:value to inputLexicon:roll.
        }
    }.

    root:gui:show().

    return root.
}


//End GUI

startMainloop().