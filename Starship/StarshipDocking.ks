PARAMETER targetName IS TARGET:SHIPNAME, targetDockName IS "mainDockS", dockingAngle IS 0.

runoncepath("0:basic_lib.ks").
runoncepath("0:orbital_maneuvers_lib.ks").
runoncepath("0:Starship/StarshipParts.ks").
runoncepath("0:vessel_info_lib.ks").

CLEARSCREEN.
SAS OFF.
RCS OFF.
SET targetVessel TO VESSEL(targetName).
starshipControl:CONTROLFROM().

//Getting an encounter

LOCAL closestApproachTime TO findClosestApproach(TIME:SECONDS, TIME:SECONDS + ORBIT:PERIOD, 10, SHIP, targetVessel).

FOR n IN ALLNODES {
	REMOVE n.
}

FOR e IN starshipEngines {
	e:ACTIVATE.
}

IF DISTANCEAT(closestApproachTime, targetVessel) > 3000 {
	KUniverse:FORCEACTIVE(SHIP).
	SET TARGET TO targetVessel.
	LOCAL node TO makeRendezvousManeuver(targetVessel).

	IF DISTANCEAT(findClosestApproach(TIME:SECONDS + node:ETA, TIME:SECONDS + node:ETA + ORBIT:PERIOD, 10, SHIP, targetVessel)) < DISTANCEAT(TIME:SECONDS, targetVessel) {
		executeManeuverNode(node).
	}

	REMOVE node.

	SET closestApproachTime TO findClosestApproach(TIME:SECONDS, TIME:SECONDS + ORBIT:PERIOD, 10, SHIP, targetVessel).
}

KUNIVERSE:TIMEWARP:WARPTO(closestApproachTime - 60).

//Killing relative Velocity

LOCAL LOCK relativeVelocity TO targetVessel:VELOCITY:ORBIT - SHIP:VELOCITY:ORBIT.
LOCK STEERING TO LOOKDIRUP(relativeVelocity, NORTH:FOREVECTOR).
WAIT UNTIL targetVessel:POSITION:MAG < 200 or TIME:SECONDS > closestApproachTime - (relativeVelocity:MAG / (SHIP:AVAILABLETHRUST / SHIP:MASS)).
PRINT "Reducing relative Velocity".
LOCK THROTTLE TO (relativeVelocity:MAG / (SHIP:AVAILABLETHRUST / SHIP:MASS)).
WAIT UNTIL relativeVelocity:MAG < 0.5 OR VANG(relativeVelocity, SHIP:FACING:FOREVECTOR) > 20.
LOCK THROTTLE TO 0.


//Docking
WAIT UNTIL targetVessel:LOADED.
LOCAL targetDock TO targetVessel:PARTSTAGGED(targetDockName)[0].

LOCAL portRotation TO mainDockStarship:PORTFACING:INVERSE * SHIP:FACING.
LOCK STEERING TO LOOKDIRUP(-targetDock:PORTFACING:VECTOR, targetDock:PORTFACING:UPVECTOR * cos(dockingAngle) + targetDock:PORTFACING:STARVECTOR * sin(dockingAngle)) * portRotation.


WAIT UNTIL VANG(SHIP:FACING:FOREVECTOR, STEERING:FOREVECTOR) < 1.

PRINT "Starting Docking Procedure...".
RCS ON.

LOCK targetLocation TO (targetDock:NODEPOSITION + targetDock:PORTFACING:FOREVECTOR * 30 + targetDock:PORTFACING:STARVECTOR * 30).

LOCAL maxSpeed TO clamp(distanceToTargetPort / 100, 1, 5).
LOCAL targetVelocity TO V(0,0,0).
LOCAL shipVelocity TO V(0,0,0).

WHEN (targetLocation - mainDockStarship:NODEPOSITION):MAG < 20 THEN {
	PRINT "Reached first waypoint, " + round(distanceToTargetPort()) + "m from target, lowering speed to 1 m/s".
	UNLOCK targetLocation.
	LOCK targetLocation TO (targetDock:NODEPOSITION + targetDock:PORTFACING:FOREVECTOR * 20).
	SET maxSpeed TO 1.
	WHEN (targetLocation - mainDockStarship:NODEPOSITION):MAG < 5 THEN {
		PRINT "Reached second waypoint, " + round(distanceToTargetPort()) + "m from target".
		UNLOCK targetLocation.
		LOCK targetLocation TO (targetDock:NODEPOSITION + targetDock:PORTFACING:FOREVECTOR * 5).
		WHEN distanceToTargetPort() < 20 THEN {
			PRINT round(distanceToTargetPort()) + "m from target, lowering speed to 0.5 m/s".
			SET maxSpeed TO 0.5.
			WHEN distanceToTargetPort() < 5 THEN {
				PRINT round(distanceToTargetPort()) + "m from target, lowering speed to 0.2 m/s".
				SET maxSpeed TO 0.15.
				UNLOCK targetLocation.
				LOCK targetLocation TO targetDock:NODEPOSITION.
				WHEN distanceToTargetPort() < 1 THEN {
					PRINT round(distanceToTargetPort()) + "m from target, lowering speed to 0.1 m/s".
					SET maxSpeed TO 0.05.
				}
			}
		}
	}
}

UNTIL mainDockStarship:STATE <> "Ready" AND mainDockStarship:STATE <> "PreAttached" {
	SET targetVelocity TO vectorRelativeDirection((targetLocation - mainDockStarship:NODEPOSITION):NORMALIZED * clamp(maxSpeed, 0.1, (targetLocation - mainDockStarship:NODEPOSITION):MAG / 5), SHIP:FACING).
	SET shipVelocity TO vectorRelativeDirection(SHIP:VELOCITY:ORBIT - targetDock:SHIP:VELOCITY:ORBIT, SHIP:FACING).

	SET SHIP:CONTROL:FORE TO (targetVelocity - shipVelocity):X * 5.
	SET SHIP:CONTROL:STARBOARD TO (targetVelocity - shipVelocity):Y * 5.
	SET SHIP:CONTROL:TOP TO (targetVelocity - shipVelocity):Z * 5.

	PRINT "Distance: " + round(distanceToTargetPort(), 1) AT (0,11).
	PRINT "TargetSpeed: " + round(targetVelocity:MAG, 1) AT (0,12).
}
UNLOCK targetLocation.
RCS OFF.
playNote(300, 0.8).
playNote(0, 0.1).
playNote(150, 0.8).
playNote(0, 0.1).
playNote(400, 0.4).

CLEARSCREEN.

PRINT "Docking Complete!".
PRINT "Sucessfully docked with " + targetName + "!".


function distanceToTargetPort {
	return (targetDock:NODEPOSITION - mainDockStarship:NODEPOSITION):MAG.
}