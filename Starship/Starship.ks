@LazyGlobal Off.
//Create global variables and functions for Starship. -- by maehschaf

//dependencies
runoncepath("0:basic_lib.ks").

local loadTime to time:seconds.

global starshipDefaultCoMOffset to -4.4.

//Check if Booster is attached
global function hasBooster {
	return not SHIP:PARTSTAGGED("BoosterControl"):EMPTY.
}

//Check if Starship is attached
global function hasStarship {
	return not SHIP:PARTSTAGGED("StarshipControl"):EMPTY.
}

//Check if this is the Booster Control
global function isBooster {
	return core:tag = "BoosterControl".
}

//Check if this is the Starship Control
global function isStarship {
	return core:tag = "StarshipControl".
}

wait until ship:unpacked.

initParts().

//PARTS
local function initParts {
	//Starship
	if hasStarship() {
		printLog("Starship found").
		Global starshipControl TO SHIP:PARTSTAGGED("StarshipControl")[0].
		Global starshipEngines TO SHIP:PARTSTAGGED("StarshipEngine").
		Global starshipVacEngines TO SHIP:PARTSTAGGED("StarshipVacEngine").

		Global starshipFlapHingeFL to ship:partstagged("FrontFlapHingeSL")[0].
		Global starshipFlapHingeFR to ship:partstagged("FrontFlapHingeSR")[0].
		Global starshipFlapHingeBL to ship:partstagged("BackFlapHingeSL")[0].
		Global starshipFlapHingeBR to ship:partstagged("BackFlapHingeSR")[0].


		Global starshipFrontTanks TO SHIP:PARTSTAGGED("FrontTankS").
		Global starshipBackTanks TO SHIP:PARTSTAGGED("BackTankS").

		Global stageDocksStarship TO SHIP:PARTSTAGGED("stageDockS").
		Global boosterDecoupler to ship:partstagged("StarshipDecoupler")[0].

		Global mainDockStarship TO SHIP:PARTSTAGGED("mainDockS")[0].
		Global utilityDockStarship TO SHIP:PARTSTAGGED("utilityDockS")[0].

		Global solarPanelsStarship TO SHIP:PARTSTAGGED("SolarPanelS").
		Global fuelCellStarship TO SHIP:PARTSTAGGED("FuelCellS").
		Global antennaStarship TO SHIP:PARTSTAGGED("extAntennaS")[0].
	}

	//Booster
	if hasBooster() {
		printLog("Starship Booster found").
		Global boosterControl TO SHIP:PARTSTAGGED("BoosterControl")[0].
		Global boosterAirbrakes TO SHIP:PARTSTAGGED("AirbrakeB").
		Global boosterEngines TO SHIP:PARTSTAGGED("BoosterEngine").

		Global stageDocksBooster TO SHIP:PARTSTAGGED("stageDockB").

		Global mainDockBooster1 TO SHIP:PARTSTAGGED("mainDockB1")[0].
		Global mainDockBooster2 TO SHIP:PARTSTAGGED("mainDockB2")[0].
	}
}

//FLAP CONTROLS			
//#region
global FlapState to Lexicon(
	"CONTROL", 0,
	"DRAGCONTROL", 1,
	"DEPLOYED", 2,
	"PASSIVE", 3, //Front Flaps deployed, back Flaps retracted
	"NEUTRAL", 4,
	"RETRACTED", 5,
	"PASSIVEREV", 6 //Front Flaps retracted, back Flaps deployed
).

global FlapReferenceFrame to Lexicon(
	"VESSEL", 0,
	"SURFACE", 1,
	"SRFRETROGRADE", 2,
	"RETROGRADE", 3
).

global function getFlapReferenceFrameVector {
	Parameter flapRef.
	if flapRef = FlapReferenceFrame:VESSEL {
		return ship:Facing:topvector.
	} else if flapRef = FlapReferenceFrame:SURFACE {
		return up:vector.
	} else if flapRef = FlapReferenceFrame:SRFRETROGRADE {
		return -ship:velocity:surface.
	} else {
		return -ship:velocity:orbit.
	} 
}

global function updateFlaps {
//Updates flap position based on the given input and states, "control" defaults to ship:control values
//Possible states: control, deployed, landing, neutral, retracted -- See FlapState
	parameter state is FlapState:CONTROL, pitch is ship:control:pitch, yaw is ship:control:yaw, roll is ship:control:roll, flapRef is choose FlapReferenceFrame:SURFACE if state = FlapState:CONTROL else FlapReferenceFrame:VESSEL, debug is False.
	local flaps to list(0, 0, 0, 0).
	local flapsSide to list(0, 0, 0, 0).

	//Controls are inverted for some reason..
	set pitch to -pitch.
	set yaw to -yaw.
	set roll to -roll.

	if state = FlapState:CONTROL {
		//Wies gemacht wird:
		//Für jede Flap werden alle Input Werte addiert
		//Der größte Abstand zwischen zwei Werten wird gesucht, also welche Flaps sind am weitesten auseinander und wie weit (Immernoch in Input format also 1 bis -1, für zu Ergebnis zwischen 3 und -3)
		//Die addierten Input Werte aller Flaps werden durch den gerade errechneten größten Abstand geteilt, ist der größte Abstand unter 2 wird duch 2 geteilt
		//Der kleinste Wert wird gesucht. Dieser wird von allen Flap Werten abgezogen. So sind die Flaps immer möglichst weit unten.
		//Die Werte sollten sich nun zwischen 0 und 1 bewegen, daher kann man sie einfach mit dem maximalen Ausschlag der Flaps multiplizieren und dann anwenden.

		//Angle list: FrontLeft, FrontRight, BackLeft, Back Right

		//Main inputs for vertical/braking force
		set flaps[0] to pitch.
		set flaps[1] to pitch.
		set flaps[2] to -pitch. 
		set flaps[3] to -pitch. 

		//Inputs for side force
		set flapsSide[0] to - roll + yaw. 
		set flapsSide[1] to roll - yaw.
		set flapsSide[2] to - roll - yaw. 
		set flapsSide[3] to roll + yaw. 

		//Prob Overkill, but was usefull in the old system, see DRAGCONTROL
		local inputRange to findLargest(flaps) - findSmallest(flaps).

		for i in range(flaps:length) {
			set flaps[i] to flaps[i] / max(inputRange, 2).
		}

		//Subtract smallest value for maximum Deployment
		local smallest to findSmallest(flaps).
		for i in range(flaps:length) {
			set flaps[i] to flaps[i] - smallest.
			if debug {
				println(i + ": " + flaps[i], 1 + i). //Debug flap values
			}
		}

		applyFlapControls(flaps, flapRef, 0, 90 - optimalAngleOfAttack, flapsSide).
	} else if state = FlapState:DRAGCONTROL {
		//Old main CONTROL method
		//would probaply work fine if the flaps only generated drag, but since we have lift too -> use the new CONTROL

		//Wies gemacht wird:
		//Für jede Flap werden alle Input Werte addiert
		//Der größte Abstand zwischen zwei Werten wird gesucht, also welche Flaps sind am weitesten auseinander und wie weit (Immernoch in Input format also 1 bis -1, für zu Ergebnis zwischen 3 und -3)
		//Die addierten Input Werte aller Flaps werden durch den gerade errechneten größten Abstand geteilt, ist der größte Abstand unter 2 wird duch 2 geteilt
		//Der kleinste Wert wird gesucht. Dieser wird von allen Flap Werten abgezogen. So sind die Flaps immer möglichst weit unten.
		//Die Werte sollten sich nun zwischen 0 und 1 bewegen, daher kann man sie einfach mit dem maximalen Ausschlag der Flaps multiplizieren und dann anwenden.

		//Adding up the Inputs
		set flaps[0] to pitch + roll - yaw. 
		set flaps[1] to pitch - roll + yaw.
		set flaps[2] to -pitch + roll + yaw. 
		set flaps[3] to -pitch - roll - yaw. 

		local inputRange to findLargest(flaps) - findSmallest(flaps).

		for i in range(flaps:length) {
			set flaps[i] to flaps[i] / max(inputRange, 2).
		}

		//Subtract smallest value for maximum Deployment
		local smallest to findSmallest(flaps).
		for i in range(flaps:length) {
			set flaps[i] to flaps[i] - smallest.
			if debug {
				println(i + ": " + flaps[i], 1 + i). //Debug flap values
			}
		}

		applyFlapControls(flaps, flapRef, 0, 90).
	} else if state = FlapState:DEPLOYED {
		applyFlapControls(flaps, flapRef).
	} else if state = FlapState:PASSIVE {
		set flaps to list(0, 0, 1, 1).
		applyFlapControls(flaps, flapRef, 0, 90).
	} else if state = FlapState:PASSIVEREV {
		set flaps to list(1, 1, 0, 0).
		applyFlapControls(flaps, flapRef, 0, 90).
	} else if state = FlapState:NEUTRAL {
		applyFlapControls(flaps, flapRef, 0, 90).
	} else { //retracted, launch
		set flaps to list(1, 1, 1, 1).

		applyFlapControls(flaps, flapRef).
	}
}

function applyFlapControls {
	parameter inputArray is list(0, 0, 0, 0), flapRef is FlapReferenceFrame:VESSEL, minAngle is "None", maxAngle is "None", sideInputArray is list(0,0,0,0).
	//Angle list: FrontLeft, FrontRight, BackLeft, Back Right

	applySingleFlapControl(starshipFlapHingeFL, inputArray[0], sideInputArray[0], minAngle, maxAngle, flapRef).
	applySingleFlapControl(starshipFlapHingeFR, inputArray[1], sideInputArray[1], minAngle, maxAngle, flapRef).
	applySingleFlapControl(starshipFlapHingeBL, inputArray[2], sideInputArray[2], minAngle, maxAngle, flapRef).
	applySingleFlapControl(starshipFlapHingeBR, inputArray[3], sideInputArray[3], minAngle, maxAngle, flapRef).
}

local servoHingeModuleName to "ModuleRoboticServoHinge".
local hingeLimits to Lexicon(). //hingeLimits[part:cid][0] - 0 = min, 1 = max

function applySingleFlapControl {
	parameter hinge, input, sideInput, minAng is "None", maxAng is "None", flapRef is FlapReferenceFrame:VESSEL.

	local angleReference is getFlapReferenceFrameVector(flapRef). //What those angles are relative to... Basically the up vector for the flaps

	//Limits
	if not hingeLimits:haskey(hinge:cid) {
		set hingeLimits[hinge:cid] to list(getHingeMinimum(hinge), getHingeMaximum(hinge)).
	}

	local hmin to hingeLimits[hinge:cid][0].
	local hmax to hingeLimits[hinge:cid][1].
	
	local min to choose hmin if minAng = "None" else max(minAng, hmin).
	local max to choose hmax if maxAng = "None" else min(maxAng, hmax).

	//Main input
	local angle to (min + (max - min) * (1 - input)). //Input 0 = max, Input 1 = min

	//Side input
	if sideInput <> 0 { //-1 = generate less side forces, 1 = generate more side forces aka move closer to optimal AoA
		local authority to 45. //How much the flaps are allowed to move to generate side forces. Changing this will lead to retuning the pids etc.
		local angleError to (90 - optimalAngleOfAttack) - angle.
		set angle to angle + clamp(sign(angleError) * sideInput * authority, angleError, angle - sign(angleError) * authority). //TODO gotta clamp that to prevent overshooting optimalAoA
		set angle to clamp(angle, 90, 0).
		//printLog(hinge:tag + " orig angle: " + round(angle, 2)).
	}

	//Translate angles to correct FlapReferenceFrame
	if not flapRef = FlapReferenceFrame:VESSEL { //Safe some performance, the angle will be correct anyways (in theory only though, what if the hinges are mounted on an angle??)
		local sideOfCraft to sign(vectorRelativeDirection(hinge:position, ship:facing):x). //-1 when on left, 1 when on right
		set angle to angle + getRoll(ship:facing, angleReference) * sideOfCraft.
	}
	//printLog(hinge:tag + " sidein: " + round(sideInput, 2) + " Angle: " + round(angle, 2)).
	setHingeAngle(hinge, angle).
}

//HINGES
//TODO Maybe move to extra lib?
global function setHingeAngle {
//Sets the angle of the given servo hinge part
	Parameter hingePart, angle.
	local servoModule to hingePart:getmodule(servoHingeModuleName).
	if servoModule:hasField("target angle") {
		servoModule:setField("target angle", angle).
		//printLog("Updated " + hingePart:tag + "s at " + time:seconds + " to angle " + round(angle, 1)).
	} else {
		restartHinge(hingePart).
	}
}

global function getHingeAngle { //DONT USE Those values are only updated if the rightclock menus are open....
//Gets the current angle of the given servo hinge part
	Parameter hingePart.
	local servoModule to hingePart:getmodule(servoHingeModuleName).
	if servoModule:hasField("current angle") {
		return servoModule:getField("current angle").
	} else {
		return "None".
	}
}

global function getHingeTargetAngle { //DONT USE Those values are only updated if the rightclock menus are open....
//Gets the target angle of the given servo hinge part
	Parameter hingePart.
	local servoModule to hingePart:getmodule(servoHingeModuleName).
	if servoModule:hasField("target angle") {
		return servoModule:getField("target angle").
	} else {
		return "None".
	}
}

global function getHingeMinimum {
//Gets the lower limit of the hinge - Warning: Has problems when rightclick menu hasnt been opened since the hinge was turned on
	Parameter hingePart.
	local servoModule to hingePart:getmodule(servoHingeModuleName).
	if servoModule:hasField("target angle") and servoModule:hasAction("set maximum angle") {
		local targetAng to servoModule:getField("target angle").
		servoModule:doAction("set minimum angle", True).
		local minAng to servoModule:getField("target angle").
		servoModule:setField("target Angle", targetAng).
		return minAng.
	} else {
		printLog("Failed to get lower limit of " + hingePart:title + " " + hingePart:tag).
		return 0.
	}
}

global function getHingeMaximum {
//Gets the Uppder limit of the hinge - Warning: Has problems when rightclick menu hasnt been opened since the hinge was turned on
	Parameter hingePart.
	local servoModule to hingePart:getmodule(servoHingeModuleName).
	if servoModule:hasField("target angle") and servoModule:hasAction("set maximum angle") {
		local targetAng to servoModule:getField("target angle").
		servoModule:doAction("set maximum angle", True).
		local maxAng to servoModule:getField("target angle").
		servoModule:setField("target Angle", targetAng).
		return maxAng.
	} else {
		printLog("Failed to get upper limit of " + hingePart:title + " " + hingePart:tag).
		return 90.
	}
}

global function lockHinge {
//Locks the Hinge - Warning: Has problems when rightclick menu hasnt been opened since the hinge was turned on
	Parameter hingePart.
	local servoModule to hingePart:getmodule(servoHingeModuleName).
	if servoModule:hasAction("engage servo lock") {
		servoModule:doAction("engage servo lock", True).
	} else {
		printLog("Failed to lock " + hingePart:title + " " + hingePart:tag).
	}
}

global function unlockHinge {
//Unlocks the Hinge - Warning: Has problems when rightclick menu hasnt been opened since the hinge was turned on
	Parameter hingePart.
	local servoModule to hingePart:getmodule(servoHingeModuleName).
	if servoModule:hasAction("disengage servo lock") {
		servoModule:doAction("disengage servo lock", True).
	} else {
		printLog("Failed to unlock " + hingePart:title + " " + hingePart:tag).
	}
}

global function restartHinge {
//function that deals with stuck hinges
	parameter hingePart.
	local servoModule to hingePart:getmodule(servoHingeModuleName).
	if servoModule:hasField("motor") and not servoModule:getField("motor") {
		servoModule:setField("motor", True).
		printLog("Engaged motor drive of " + hingePart:title + " " + hingePart:tag).
	}
	if servoModule:hasField("locked") and servoModule:getField("locked") {
		servoModule:setField("locked", False).
		printLog("Disengaged servo lock of " + hingePart:title + " " + hingePart:tag).
	}
	printLog("Tried to restart hinge " + hingePart:title + " " + hingePart:tag).
}

global function setGearHingesLocked {
	parameter locked is true.
	if locked {
		lockHinge(starshipFlapHingeBL).
		lockHinge(starshipFlapHingeBR).
	} else {
		unlockHinge(starshipFlapHingeBL).
		unlockHinge(starshipFlapHingeBR).
	}
}

//Mögliche Gründe fürs flattern der flaps: 
// - Denkfehler bei der Flap Steuerungs logik -> Probieren ob minimale änderungen des Inputs zu große Wirkungen haben
// - GetAngularVelocity funktioniert nich richtig -> Mal nicht die aus basic verwenden sondern eigene schreiben
// - PIDs sind falsch kalibriert -> Nach obigem mal die alten werte ausprobieren
// - Sonst irgendwie anders die geschwindigkeit der Hinges reduzieren.

//set ship:control:pitch to ship:control:pilotpitch.
//set ship:control:yaw to ship:control:pilotyaw.
//set ship:control:roll to ship:control:pilotroll.

//#endRegion

//ENGINES
global function useSeaLevelEngines {
	Parameter asl. //True to use Sea Level Engines, False to use Vacuum Engines
	for e in starshipEngines{
		if asl {
			e:activate().
		} else {
			e:shutdown().
		}
	}
	for e in starshipVacEngines {
		if asl {
			e:shutdown().
		} else {
			e:activate().
		}
	}
}


//DECOUPLE
global function decoupleBooster {
	if hasStarship() {
		local decoupleModule to boosterDecoupler:getModule("ModuleDecouple").
		if decoupleModule:hasAction("decouple") {
			decoupleModule:doAction("decouple", true).
			wait 0.001.
		}
	}
	if hasStarship() {
		for dock in stageDocksStarship {
			local dockMod to dock:getModule("ModuleDockingNode").
			if dockMod:hasEvent("make primary docking node") {
				dockMod:doEvent("make primary docking node").
				wait 0.001.
			}
			if dockMod:hasEvent("undock") {
				dockMod:doEvent("undock").
				wait 0.001.
			}
			if dock:hasPartner() {
				dock:undock().
				wait 0.001.
			}
		}
	}
	wait 0.001.
}

//MOVE COM
global function getRelativeCoMPosition {
	Parameter referencePart is starshipControl.
	return -vectorRelativeDirection(referencePart:position, ship:Facing).
}

local OxTransfer to 0.
local LfTransfer to 0.

local function isTransferDone {
	parameter transfer. //Check if a transfer is running
	if transfer = 0 {
		return True.
	} else {
		return transfer:status = "Finished" or transfer:status = "Failed".
	}
}

//Move the center of mass relative to the given part by pumping fuel - All thats left from the Original version of this file
global function updateCoM {
	Parameter TargetCoMDistanceToReferencePart is starshipDefaultCoMOffset, referencePart is starshipControl, moveAmount is abs(TargetCoMDistanceToReferencePart - getRelativeCoMPosition(referencePart):z) * 10. //maybe do move amount differently

	if round(getRelativeCoMPosition(referencePart):z, 3) = TargetCoMDistanceToReferencePart or moveAmount < 0.05 { //Target reached!
		if not isTransferDone(OxTransfer) {
			set OxTransfer:active to False.
			set OxTransfer to 0.
		}
		if not isTransferDone(LfTransfer) {
			set LfTransfer:active to False.
			set LfTransfer to 0.
		}
		return.
	}

	if isTransferDone(OxTransfer) and isTransferDone(LfTransfer) { //All Done, Start new Transfers!
		//printLog("Transfer amount: " + moveAmount).
		IF TargetCoMDistanceToReferencePart > getRelativeCoMPosition(referencePart):z { //transfer forwards
			SET OxTransfer to TRANSFER("oxidizer", starshipBackTanks, starshipFrontTanks, moveAmount).
			SET LfTransfer to TRANSFER("liquidfuel", starshipBackTanks, starshipFrontTanks, moveAmount).
			set OxTransfer:active to True.
			set LfTransfer:active to True.
		} else {
			SET OxTransfer to TRANSFER("oxidizer", starshipFrontTanks, starshipBackTanks, moveAmount).
			SET LfTransfer to TRANSFER("liquidfuel", starshipFrontTanks, starshipBackTanks, moveAmount).
			set OxTransfer:active to True.
			set LfTransfer:active to True.
		}
	} else { //Still running
		return.
	}
}

printLog("Loaded " + ScriptPath():name + " in " + round(time:seconds - loadTime, 2) + "s").