//Starship to Orbit
PARAMETER targetAltitude IS 72000.

runoncepath("0:basic_lib.ks").
runoncepath("0:orbital_maneuvers_lib.ks").

CLEARSCREEN.
PRINT "Starship to Orbit as Second Stage".

WAIT 5.
incrLoadDistances().

PRINT "Waiting for stage seperation...".


WAIT UNTIL (stageDockStarshipL:STATE = "Ready" OR stageDockStarshipL:STATE = "Disengage") AND (stageDockStarshipR:STATE = "Ready" OR stageDockStarshipR:STATE = "Disengage"). // Wait till Stage Seperation
SET SHIP:SHIPNAME TO CORE:VOLUME:NAME.
core:part:controlfrom().
SAS OFF.
BRAKES OFF.
RCS OFF.
FOR e IN starshipEngines {
	e:ACTIVATE().
}
PRINT "Stage Seperation, raising Apoapsis to target Altitude".
LOCK THROTTLE TO MAX((targetAltitude - APOAPSIS) / 200, 0.05).
LOCK STEERING TO PROGRADE + R(0,-10,0).


WAIT UNTIL SHIP:APOAPSIS >= targetAltitude + 15. // Wait till apoapsis target reached
LOCK THROTTLE TO 0.
RCS ON.

LOCAL deltaV TO calculateVelocityAtApsis(targetAltitude, targetAltitude) - VELOCITYAT(SHIP, TIME:SECONDS + ETA:APOAPSIS):ORBIT:MAG.
LOCAL burnTime TO deltaV / (SHIP:AVAILABLETHRUST / SHIP:MASS).
print "Coasting to apoapsis, will start burn " + round(burnTime * 0.65, 1) + " seconds prior".
print "Target Velocity(Apoapsis): " + round(calculateVelocityAtApsis(targetAltitude, targetAltitude), 1) + " Current Velocity at Apoapsis: " + round(VELOCITYAT(SHIP, TIME:SECONDS + ETA:APOAPSIS):ORBIT:MAG, 1) + " Circularisation deltaV: " + round(deltaV, 1).

WAIT UNTIL ETA:APOAPSIS < burnTime * 0.65 + 30.
LOCK STEERING TO HEADING(90, 0).

WAIT UNTIL ETA:APOAPSIS < burnTime * 0.65. //Wait till circularisation
print "Started circularisation burn".
LOCAL t TO 1.
LOCK THROTTLE TO t.
RCS OFF.

LOCAL apoPID TO PIDLOOP(0.06, 0, 0, -10, 10).
SET apoPID:SETPOINT TO targetAltitude + 10.
LOCK STEERING TO HEADING(90, apoPID:UPDATE(TIME:SECONDS, APOAPSIS)).

WHEN ETA:APOAPSIS > ETA:PERIAPSIS THEN {
	PRINT("Passed Apoapsis").
	SET apoPID:KP TO -apoPID:KP.
}

UNTIL SHIP:PERIAPSIS >= targetAltitude OR SHIP:APOAPSIS > targetAltitude + 1000 OR ABS(timeDifferenceApoapsis()) > SHIP:ORBIT:PERIOD / 4 { // Wait till target Periapsis reached
	PRINT "Apoapsis correction angle: " + apoPID:OUTPUT AT (0, 12).
	PRINT "Remaining circularisation DeltaV: " + round(deltaV, 1) AT (0, 13).
	PRINT "Remaining circularisation burn time: " + round(burnTime, 1) AT (0, 14).
	SET deltaV TO calculateVelocityAtApsis(targetAltitude, targetAltitude) - VELOCITYAT(SHIP, TIME:SECONDS + ETA:APOAPSIS):ORBIT:MAG.
	SET burnTime TO deltaV / (SHIP:AVAILABLETHRUST / SHIP:MASS).
	IF timeDifferenceApoapsis() > burnTime {
		SET t TO MIN(burnTime / ETA:APOAPSIS, MAX((targetAltitude - PERIAPSIS) / 1000, 0.05)).
	} ELSE {
		SET t TO MAX((targetAltitude - PERIAPSIS) / 1000, 0.05).
	}
}

LOCK THROTTLE TO 0.
RCS ON.
CLEARSCREEN.
PRINT "Final Orbit: Apoapsis: " + round(SHIP:APOAPSIS) + " - Periapsis: " + round(SHIP:PERIAPSIS).
PRINT "Target altitude was " + targetAltitude.

IF PERIAPSIS < BODY:ATM:HEIGHT AND ETA:PERIAPSIS < ETA:APOAPSIS {
	PRINT "!Failed to reach Orbit, Starting Landing Program!".
	runpath("0:Starship/StarshipLandingAtmosphere.ks").
}
SET SHIP:CONTROL:PILOTMAINTHROTTLE TO 0.
LOCK STEERING TO SUN:POSITION.
WAIT 2.
PANELS ON.
antennaStarship:GETMODULE("ModuleDeployableAntenna"):DOACTION("toggle antenna", true).
WAIT 15.
SAS ON.
resetLoadDistances().

function incrLoadDistances {
	PARAMETER dist IS 250000.
	LOCAL d TO KUNIVERSE:DEFAULTLOADDISTANCE.
	
	SET suborbitalUnload to d:SUBORBITAL:UNLOAD.
	SET flyingUnload to d:FLYING:UNLOAD.
	SET orbitUnload to d:ORBIT:UNLOAD.
	SET suborbitalPack to d:SUBORBITAL:PACK.
	SET flyingPack to d:FLYING:PACK.
	SET orbitPack to d:ORBIT:PACK.
	
	SET d:ORBIT:UNLOAD TO dist.
	SET d:SUBORBITAL:UNLOAD TO dist.
	SET d:FLYING:UNLOAD TO dist.
	WAIT 0.001.
	SET d:ORBIT:PACK TO (dist - 1).
	SET d:SUBORBITAL:PACK TO (dist - 1).
	SET d:FLYING:PACK TO (dist - 1).
	PRINT "Increased Unload Distances".

}

function resetLoadDistances {
	LOCAL d TO KUNIVERSE:DEFAULTLOADDISTANCE.

	SET d:SUBORBITAL:PACK TO suborbitalPack.
	SET d:FLYING:PACK TO flyingPack.
	SET d:ORBIT:PACK TO orbitPack.
	WAIT 0.001.
	SET d:SUBORBITAL:UNLOAD TO suborbitalUnload.
	SET d:FLYING:UNLOAD TO flyingUnload.
	SET d:ORBIT:UNLOAD TO orbitUnload.
	PRINT "Reseted Unload Distances".
}