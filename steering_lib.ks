@LazyGlobal off.
//A lib to replace the default kOS steering -- by maehschaf

RunOncePath("0:/basic_lib.ks").

local loadTime to time:seconds.

//Those PIDs move the controls to achieve the desired rotation Rates (or just directly the controls if on control and not control rade mode)
global pitchPID to PidLoop(0.1, 0, 0, -1, 1).
global yawPID to PidLoop(0.1, 0, 0, -1, 1).
global rollPID to PidLoop(0.1, 0, 0, -1, 1).

//Those PIDs change the rotation rates to reach the desired orientation
global pitchRatePID to PidLoop(0.2, 0, 0, -10, 10).
global yawRatePID to PidLoop(0.2, 0, 0, -10, 10).
global rollRatePID to PidLoop(0.2, 0, 0, -10, 10).

//Steering modes are: none, rate, navball, direction
local steeringMode to "none".
local steeringDirection to "none".

//Current steering factors
local activeSteeringFactors to "none".
local continouslyApplySteeringFactors to False.

global function initSteering {
    //Initializes the steering factors, turns SAS off
    parameter factors.

    SAS Off.

    factors:apply().
}

global function setSteeringTargetRates {
    //Sets the steering to the given rotation rates - Changes the SteeringMode to "rate"
    Parameter pitch, yaw, roll.
    set steeringMode to "rate".

    set pitchPID:setpoint to pitch.
    set yawPID:setpoint to yaw.
    set rollPID:setpoint to roll.
}

global Function setSteeringTargetHold {
    //Sets the steering to hold the current attitude - Changes the SteeringMode to "rate"
    setSteeringTargetRates(0, 0, 0).
}

global function setSteeringTargetNavball {
    //Sets the steering to point in the given navball pitch, heading and roll - Changes the SteeringMode to "navball"
    parameter pitch, heading, roll.
    set steeringMode to "navball".
    set steeringDirection to Heading(heading, pitch, roll).

    set pitchRatePID:setpoint to 0.
    set yawRatePID:setpoint to 0.
    set rollRatePID:setpoint to 0.
}

global function setSteeringTarget {
    //Sets the steering to point in the given direction or vector - Changes the SteeringMode to "direction"
    parameter direction.
    set steeringMode to "direction".
    set steeringDirection to direction.

    set pitchRatePID:setpoint to 0.
    set yawRatePID:setpoint to 0.
    set rollRatePID:setpoint to 0.
}

global function disableSteering {
    set steeringMode to "none".
    set ship:control:neutralize to True.
}

global function updateSteering {
    //Updates the steering pids - Call this in a loop
    //If applyToControls is true the results will be applied to ship:control
    //angularVelocitySmoothingPeriod = Averages the angular velocity over hte given time to avoid vibrations
    parameter angularVelocitySmoothingPeriod to 1, applyToControls is True, vessel is ship, debug is True.

    local output to Lexicon(
        "pitchError", 0,
        "yawError", 0,
        "rollError", 0,
        "pitchRate", 0,
        "yawRate", 0,
        "rollRate", 0,
        "pitch", 0,
        "yaw", 0,
        "roll", 0
    ).

    if continouslyApplySteeringFactors and activeSteeringFactors <> "none" {
        activeSteeringFactors:apply().
    }

    if steeringMode = "none" {
        //if applyToControls and not vessel:control:neutral {
            //set vessel:control:neutralize to True.
        //}
        return output.
    } else if steeringMode = "navball" or steeringMode = "direction" {

        local topVector to (steeringDirection:topvector * ship:facing:inverse). //steeringDirection topvector in the ships facing reference frame
        local foreVector to (steeringDirection:forevector * ship:facing:inverse). //steeringDirection forevector in the ships facing reference frame
        
        set output:pitchError to arctan2(foreVector:y, foreVector:z). //x axis should be "side", ignore it
        set output:yawError to arctan2(foreVector:x, foreVector:z). //y axis should be "up", ignore it
        set output:rollError to arctan2(topVector:x, topVector:y). //z axis should be "forward", ignore it
        
        if debug {
            println("Steering Debug:", 0).
            println("Pitch Error: " + round(output:pitchError, 3), 1).
            println("Yaw Error: " + round(output:yawError, 3), 2).
            println("Roll Error: " + round(output:rollError, 3), 3).
        }

        if getSteeringOutputMode() <> "control" {
            set output:pitchRate to -pitchRatePID:update(time:seconds, output:pitchError).
            set output:yawRate to -yawRatePID:update(time:seconds, output:yawError).
            set output:rollRate to -rollRatePID:update(time:seconds, output:rollError).
        }

        set pitchPID:setpoint to output:pitchRate.
        set yawPID:setpoint to output:yawRate.
        set rollPID:setpoint to output:rollRate.
    }

    if getSteeringOutputMode() = "control" {//control -> skipping the rate part
        set output:pitch to vessel:control:pitchtrim - pitchPID:update(time:seconds, output:pitchError).
        set output:yaw to vessel:control:rolltrim - yawPID:update(time:seconds, output:yawError).
        set output:roll to vessel:control:yawtrim - rollPID:update(time:seconds, output:rollError).
    } else if getSteeringOutputMode() = "rate" {//rate
        set output:pitch to output:pitchRate.
        set output:yaw to output:yawRate.
        set output:roll to output:rollRate.
    } else if getSteeringOutputMode() = "ratecontrol" {//ratecontrol
        local angVel to choose getAngularVelocity() if angularVelocitySmoothingPeriod = 0 else getSmoothedAngularVelocity(angularVelocitySmoothingPeriod).

        //Authority changes should be made in the specific programms! No aouthority limiting here..
        set output:pitch to vessel:control:pitchtrim + pitchPID:update(time:seconds, angVel:x).
        set output:yaw to vessel:control:rolltrim + yawPID:update(time:seconds, angVel:y).
        set output:roll to vessel:control:yawtrim + rollPID:update(time:seconds, angVel:z).

        if debug {
            println("", 4).
            println("Rates:", 5).
            println("Pitch Rate Error: " + round(pitchPID:error, 3), 6).
            println("Yaw Rate Error: " + round(yawPID:error, 3), 7).
            println("Roll Rate Error: " + round(rollPID:error, 3), 8).
        }
    }

    if applyToControls {
        set vessel:control:pitch to output:pitch.
        set vessel:control:yaw to output:yaw.
        set vessel:control:roll to output:roll.
    }

    return output.
}

//SteeringFactors

//Rate Factors only
global function SteeringRateFactors {
    //When using this the output of the Steering will be the needed rotational rates
    //Create SteeringRateFactors "Object" Parameters: pitchRate, yawRate and rollRate factors
    //Every parameter takes a list with the p, i and d term for the specific PIDLoop.
    //min and max output can be given as 4th and 5th list entrys optionally.
    //list(p, i, d, min, max)
    Parameter pitchRateFactors is list(0.2, 0, 0, -10, 10), yawRateFactors is list(0.2, 0, 0, -10, 10), rollRateFactors is list(0.2, 0, 0, -10, 10).

    local self to Lexicon().

    set self:pitchRate to pitchRateFactors.
    set self:yawRate to yawRateFactors.
    set self:rollRate to rollRateFactors.
    set self:serializedtype to "SteeringRateFactors".

    loadSteeringFactors(self).

    return self.
}

//Control Factors only
global function SteeringControlFactors {
    //When using this the Steering will use the orientation error directly to output the needed control values
    //Create SteeringControlFactors "Object" Parameters: pitch, yaw and roll factors
    //Every parameter takes a list with the p, i and d term for the specific PIDLoop.
    //list(p, i, d, min, max)
    Parameter pitchFactors is list(0.1, 0, 0), yawFactors is list(0.1, 0, 0), rollFactors is list(0.1, 0, 0).

    local self to Lexicon().

    set self:pitch to pitchFactors.
    set self:yaw to yawFactors.
    set self:roll to rollFactors.

    set self:serializedtype to "SteeringControlFactors".

    loadSteeringFactors(self).

    return self.
}

//Both Factors
global function SteeringFactors {
    //When using this the Steering will first calculate the needed rotation rate and then output the control values needed to achieve those rates
    //Create SteeringFactors "Object" Parameters: pitch, yaw, roll, pitchRate, yawRate and rollRate factors
    //Every parameter takes a list with the p, i and d term for the specific PIDLoop.
    //min and max output can be given as 4th and 5th list entrys optionally (rate factors only!)
    //list(p, i, d, min, max)
    //Dynamic pressure multiplier: if useDynamicPressure is true the steering output will be divided by (dynamic pressure * dynamicPressureMultiplier) -> A bigger multiplier results in smaller outputs
    Parameter pitchFactors is list(0.1, 0, 0), yawFactors is list(0.1, 0, 0), rollFactors is list(0.1, 0, 0), pitchRateFactors is list(0.2, 0, 0, -10, 10), yawRateFactors is list(0.2, 0, 0, -10, 10), rollRateFactors is list(0.2, 0, 0, -10, 10).

    local self to Lexicon().

    set self:pitch to pitchFactors.
    set self:yaw to yawFactors.
    set self:roll to rollFactors.

    set self:pitchRate to pitchRateFactors.
    set self:yawRate to yawRateFactors.
    set self:rollRate to rollRateFactors.

    set self:serializedtype to "SteeringFactors".

    loadSteeringFactors(self).
    
    return self.
}

global function loadSteeringFactors {
    parameter self.
    initTypeFunctionOverrides(self, self:serializedtype).

    if self:getType() = "SteeringFactors" {
        set self:apply to {
            parameter pitch is pitchPID, yaw is yawPID, roll is rollPID, pitchRate is pitchRatePID, yawRate is yawRatePID, rollRate is rollRatePID.

            setFactors(pitch, self:pitch).
            setFactors(yaw, self:yaw).
            setFactors(roll, self:roll).

            setFactors(pitchRate, self:pitchRate).
            setFactors(yawRate, self:yawRate).
            setFactors(rollRate, self:rollRate).

            set activeSteeringFactors to self.
        }.
    } else if self:getType() = "SteeringRateFactors" {
        set self:apply to {
            parameter pitchRate is pitchRatePID, yawRate is yawRatePID, rollRate is rollRatePID.
            setFactors(pitchRate, self:pitchRate).
            setFactors(yawRate, self:yawRate).
            setFactors(rollRate, self:rollRate).

            set activeSteeringFactors to self.
        }.
    } else if self:getType() = "SteeringControlFactors" {
        set self:apply to {
            parameter pitch is pitchPID, yaw is yawPID, roll is rollPID.

            setFactors(pitch, self:pitch).
            setFactors(yaw, self:yaw).
            setFactors(roll, self:roll).

            set activeSteeringFactors to self.
        }.
    } else {
        printLog("Error: Failed to load SteeringFactors from Lexicon").
    }

    return self.
}

local function setFactors {
    //sets given pids terms based on a list of the following format: list(p, i, d, min, max)
    parameter pid, factors.
    set pid:KP to factors[0].
    set pid:KI to factors[1].
    set pid:KD to factors[2].
    if factors:length > 3 {
        set pid:minoutput to factors[3].
        set pid:maxoutput to factors[4].
    }
}

global function getSteeringMode {
    return steeringMode.
}

global function getSteeringOutputMode {
    //Get what mode the steering is in -> what factors are used and what output is generated
    if not activeSteeringFactors:hassuffix("getType") {
        return "none".
    } else if activeSteeringFactors:getType() = "SteeringFactors" {
        return "ratecontrol".
    } else if activeSteeringFactors:getType() = "SteeringRateFactors" {
        return "rate".
    } else if activeSteeringFactors:getType() = "SteeringControlFactors" {
        return "control".
    } else {
        return "none".
    }
}

global function getActiveSteeringFactors {
    return activeSteeringFactors.
}

global function getSteeringDirection {
    return steeringDirection.
}

printLog("Loaded " + ScriptPath():name + " in " + round(time:seconds - loadTime, 2) + "s").